package br.com.weeapp.enumeration;

import android.content.Context;

import br.com.weeapp.R;

/**
 * Created by ranipieper on 8/20/15.
 */
public enum LevelEnun {
    BASICO(1, R.string.level_basico),
    INTERMEDIARIO (2, R.string.level_intermediario),
    AVANCADO (3, R.string.level_avancado),
    CONVERSACAO (4, R.string.level_conversacao);

    public int id;
    public int resName;

    private LevelEnun(int id, int resName) {
        this.id = id;
        this.resName = resName;
    }

    public static LevelEnun fromInt(int code) {
        switch(code) {
            case 1:
                return BASICO;
            case 2:
                return INTERMEDIARIO;
            case 3:
                return AVANCADO;
            case 4:
                return CONVERSACAO;
        }

        return null;
    }

    public static LevelEnun fromName(Context context, String name) {
        if (context.getString(BASICO.resName).toLowerCase().indexOf(name.toLowerCase()) >= 0) {
            return BASICO;
        } else if (context.getString(INTERMEDIARIO.resName).toLowerCase().indexOf(name.toLowerCase()) >= 0) {
            return INTERMEDIARIO;
        } else if (context.getString(AVANCADO.resName).toLowerCase().indexOf(name.toLowerCase()) >= 0) {
            return AVANCADO;
        } else if (context.getString(CONVERSACAO.resName).toLowerCase().indexOf(name.toLowerCase()) >= 0) {
            return CONVERSACAO;
        }
        return null;
    }
}