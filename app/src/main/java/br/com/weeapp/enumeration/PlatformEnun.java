package br.com.weeapp.enumeration;

import android.content.Context;

import br.com.weeapp.R;

/**
 * Created by ranipieper on 8/20/15.
 */
public enum PlatformEnun {
    SKYPE(1, R.string.plataform_skype),
    HANGOUT (2, R.string.plataform_hangout);

    public int id;
    public int resName;

    private PlatformEnun(int id, int resName) {
        this.id = id;
        this.resName = resName;
    }

    public static PlatformEnun fromInt(int code) {
        switch(code) {
            case 1:
                return SKYPE;
            case 2:
                return HANGOUT;
        }

        return null;
    }

    public static PlatformEnun fromName(Context context, String name) {
        if (context.getString(SKYPE.resName).toLowerCase().indexOf(name.toLowerCase()) >= 0) {
            return SKYPE;
        } else if (context.getString(HANGOUT.resName).toLowerCase().indexOf(name.toLowerCase()) >= 0) {
            return HANGOUT;
        }
        return null;
    }
}