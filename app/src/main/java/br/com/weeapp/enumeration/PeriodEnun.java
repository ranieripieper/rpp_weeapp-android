package br.com.weeapp.enumeration;

import android.content.Context;

import br.com.weeapp.R;

/**
 * Created by ranipieper on 8/20/15.
 */
public enum PeriodEnun {
    MANHA(1, R.string.period_manha),
    TARDE (2, R.string.period_tarde),
    NOITE (2, R.string.period_noite),
    MADRUGADA (2, R.string.period_madrugada);

    public int id;
    public int resName;

    private PeriodEnun(int id, int resName) {
        this.id = id;
        this.resName = resName;
    }

    public static PeriodEnun fromInt(int code) {
        switch(code) {
            case 1:
                return MANHA;
            case 2:
                return TARDE;
            case 3:
                return NOITE;
            case 4:
                return MADRUGADA;
        }

        return null;
    }

    public static PeriodEnun fromName(Context context, String name) {
        if (context.getString(MANHA.resName).toLowerCase().indexOf(name.toLowerCase()) >= 0) {
            return MANHA;
        } else if (context.getString(TARDE.resName).toLowerCase().indexOf(name.toLowerCase()) >= 0) {
            return TARDE;
        } else if (context.getString(NOITE.resName).toLowerCase().indexOf(name.toLowerCase()) >= 0) {
            return NOITE;
        } else if (context.getString(MADRUGADA.resName).toLowerCase().indexOf(name.toLowerCase()) >= 0) {
            return MADRUGADA;
        }
        return null;
    }
}