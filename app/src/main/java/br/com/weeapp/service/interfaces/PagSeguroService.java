package br.com.weeapp.service.interfaces;

import br.com.weeapp.model.result.PagSeguroResult;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by ranipieper on 8/30/15.
 */
public interface PagSeguroService {
/*
    curl "https://ws.sandbox.pagseguro.uol.com.br/v2/checkout/" -d "email=roberta@inglessa.com.br&token=F5C3F2E1494B48C0B987FB322C65FAD1
    &currency=BRL&itemId1=0001
    &itemDescription1=Notebook Prata
    &itemAmount1=2400.00&itemQuantity1=1
    &itemWeight1=1000&reference=REF12341234&senderName=Jose Comprador
    &senderEmail=comprador@uol.com.br"
*/

    @FormUrlEncoded
    @POST("/v2/checkout/")
    void generateCode(@Field("email") String email,
                      @Field("token") String token,
                      @Field("currency") String currency,
                      @Field("itemId1") String itemId1,
                      @Field("itemDescription1") String itemDescription1,
                      @Field("itemAmount1") String itemAmount1,
                      @Field("itemQuantity1") int itemQuantity1,
                      @Field("reference") String reference,
                      @Field("senderName") String senderName,
                      @Field("senderEmail") String senderEmail, retrofit.Callback<PagSeguroResult> callback);
}
