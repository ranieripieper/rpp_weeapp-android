package br.com.weeapp.service.interfaces;

import br.com.weeapp.model.Reserve;
import br.com.weeapp.model.result.PayResult;
import br.com.weeapp.service.exception.ServiceException;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by ranipieper on 8/25/15.
 */
public interface OrderService {

    @FormUrlEncoded
    @POST("/users/{user_id}/lessons/{lesson_id}/reserves.json")
    void reserve(@Path("user_id") long userId, @Path("lesson_id")  long lessonId,
                 @Field("level_id") int levelId, @Field("language_id") long languageId,
                 retrofit.Callback<Reserve> callback);

    @FormUrlEncoded
    @POST("/users/{user_id}/lessons/remove_reserves.json")
    Response cancelReserves(@Path("user_id") long userId, @Field("reserves") String lessons) throws ServiceException;

//    @FormUrlEncoded
//    @POST("/order.json")
//    void order( @Field("reserves") String reserves, retrofit.Callback<Order> callback);

    @FormUrlEncoded
    @POST("/order.json")
    void order( @Field("reserves") String reserves, retrofit.Callback<PayResult> callback);

    @FormUrlEncoded
    @POST("/order/{order_id_path}/pay")
    void pay(@Path("order_id_path") int orderIdPath, @Field("order_id") int fieldOrderId, retrofit.Callback<PayResult> callback);

    @GET("/confirmation.json")
    void confirmation(@Query("id_pagseguro") String idPagseguro, retrofit.Callback<Response> callback);

    @GET("/confirmation.json")
    Response confirmation( @Query("id_pagseguro") String idPagseguro);


    @FormUrlEncoded
    @POST("/reservelessons/rate.json")
    void rate(@Field("reservelessons_id") String reserveId, @Field("rate") Integer rate, retrofit.Callback<Response> callback);

}
