package br.com.weeapp.service.background;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;

import java.util.List;

import br.com.weeapp.model.Payment;
import br.com.weeapp.model.dao.PaymentDao;

/**
 * Created by ranipieper on 8/28/15.
 */
@EIntentService
public class CallConfirmationPaymentService extends IntentService {

    private static final String TAG = "CallConfirmationPaymentService";

    public CallConfirmationPaymentService() {
        super("CallConfirmationPaymentService");
    }

    @Bean
    PaymentDao paymentDao;

    @Override
    protected void onHandleIntent(Intent intent) {
        synchronized (TAG) {

        }
    }

    @ServiceAction
    void callConfirmations() {
        List<Payment> payments = paymentDao.getAll();
        if (payments != null) {
            for(final Payment payment : payments) {
                //TODO
                /*
                try {
                    Response r = RetrofitManager.getInstance().getOrderService().confirmation(payment.getTransactionId());
                    if (r != null && (r.getStatus() == HttpURLConnection.HTTP_OK || r.getStatus() == HttpURLConnection.HTTP_ACCEPTED)) {
                        payment.delete();
                    }
                } catch(Exception e) {
                }
                */
            }
        }
    }

    public static void callService(Context context) {
        CallConfirmationPaymentService_.intent(context)
                .callConfirmations()
                .start();
    }

}
