package br.com.weeapp.service.interfaces;

import java.util.List;

import br.com.weeapp.model.Lesson;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by ranipieper on 8/24/15.
 */
public interface TeacherService {

    @GET("/teachers/{id}/lessons.json")
    void getLessons(@Path("id") long id, retrofit.Callback<List<Lesson>> callback);
}
