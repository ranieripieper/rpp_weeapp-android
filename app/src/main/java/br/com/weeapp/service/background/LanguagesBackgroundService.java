package br.com.weeapp.service.background;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;

import java.util.List;

import br.com.weeapp.model.Language;
import br.com.weeapp.model.dao.LanguageDao;
import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.service.RetrofitManager;

/**
 * Created by ranipieper on 8/20/15.
 */
@EIntentService
public class LanguagesBackgroundService extends IntentService {

    private static final String TAG = "LanguagesBackgroundService";

    @Bean
    LanguageDao languageDao;

    public LanguagesBackgroundService() {
        super("LanguagesBackgroundService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        synchronized (TAG) {
        }
    }

    @ServiceAction
    void refreshLanguages() {
        if (SharedPrefManager.getInstance().isUpdateLanguages()) {
            List<Language> lst = languageDao.getAll();
            languageDao.saveAll(lst);
            SharedPrefManager.getInstance().setUpdateLanguages(false);
        }
        try {
            List<Language> languages = RetrofitManager.getInstance().getLanguageService().getLanguages();
            if (languages != null) {
                languageDao.deleteAll();
                languageDao.saveAll(languages);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void callService(Context context) {
        LanguagesBackgroundService_.intent(context)
                .refreshLanguages()
                .start();
    }

}
