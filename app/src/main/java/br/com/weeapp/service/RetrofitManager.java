package br.com.weeapp.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.squareup.okhttp.OkHttpClient;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import br.com.weeapp.BuildConfig;
import br.com.weeapp.model.User;
import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.service.exception.ServiceException;
import br.com.weeapp.service.interfaces.LanguageService;
import br.com.weeapp.service.interfaces.OrderService;
import br.com.weeapp.service.interfaces.SearchService;
import br.com.weeapp.service.interfaces.TeacherService;
import br.com.weeapp.service.interfaces.TipService;
import br.com.weeapp.service.interfaces.UserService;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Created by test on 8/15/15.
 */
public class RetrofitManager {

    private static String API_HOST = BuildConfig.API_URL;
    public RestAdapter restAdapter;
    final ConcurrentHashMap<Class, Object> services;
    private static RetrofitManager instance;

    public static final String HEADER_AUTH_TOKEN = "auth_token";
    public static final String HEADER_ID = "id";

    private RetrofitManager() {
        services = new ConcurrentHashMap();
        initialize();
    }

    public static RetrofitManager getInstance() {
        if (instance == null) {
            instance = new RetrofitManager();
        }
        return instance;
    }

    public static Gson getGson() {
        Gson gson = new GsonBuilder()
                //.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                //.excludeFieldsWithoutExposeAnnotation()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .registerTypeAdapter(Date.class, dateDeserializer)
                .registerTypeAdapter(Date.class, dateSerializer)
                .create();
        return gson;
    }
    private void initialize() {




        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                User user = SharedPrefManager.getInstance().getUser();
                if (user != null) {
                    request.addHeader(HEADER_AUTH_TOKEN, user.getAuthToken());
                    request.addHeader(HEADER_ID, String.valueOf(user.getId()));
                }

            }
        };

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_HOST)
                .setConverter(new GsonConverter(getGson()))
                .setErrorHandler(new WeeAppErrorHandler())
                .setRequestInterceptor(requestInterceptor)
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();
    }

    public UserService getUserService() {
        return (UserService)this.getService(UserService.class);
    }

    //Services
    public SearchService getSearchService() {
        return (SearchService)this.getService(SearchService.class);
    }

    public TipService getTipService() {
        return (TipService)this.getService(TipService.class);
    }

    public LanguageService getLanguageService() {
        return (LanguageService)this.getService(LanguageService.class);
    }

    public OrderService getOrderService() {
        return (OrderService)this.getService(OrderService.class);
    }

    public TeacherService getTeacherService() {
        return (TeacherService)this.getService(TeacherService.class);
    }

    protected <T> Object getService(Class<T> cls) {
        if(!this.services.contains(cls)) {
            this.services.putIfAbsent(cls, this.restAdapter.create(cls));
        }

        return this.services.get(cls);
    }

    class WeeAppErrorHandler implements ErrorHandler {
        @Override
        public Throwable handleError(RetrofitError cause) {
            Response r = cause.getResponse();
            if (r != null) {
                return new ServiceException(cause.getMessage(), r.getStatus());
            }
            return cause;
        }
    }


    static JsonDeserializer<Date> dateDeserializer = new JsonDeserializer<Date>() {
        @Override
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            if (json != null) {
                try {
                    Date dt = df1.get().parse(json.getAsString());
                    if (dt == null) {
                        return tryOtherFormat(json);
                    }
                    return dt;
                } catch (ParseException e) {
                    return tryOtherFormat(json);
                }
            }

            return null;
        }

        private Date tryOtherFormat(JsonElement json) {
            try {
                return df2.get().parse(json.getAsString());
            } catch (ParseException e1) {
                return tryOtherFormat2(json);
            }
        }

        private Date tryOtherFormat2(JsonElement json) {
            try {
                return df3.get().parse(json.getAsString());
            } catch (ParseException e1) {
            }
            return null;
        }
    };


    static JsonSerializer<Date> dateSerializer = new JsonSerializer<Date>() {
        @Override
        public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext
                context) {
            return src == null ? null : new JsonPrimitive(src.getTime());
        }
    };

    private static final ThreadLocal<DateFormat> df1 = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ") {
                public StringBuffer format(Date date, StringBuffer toAppendTo, java.text.FieldPosition pos) {
                    StringBuffer toFix = super.format(date, toAppendTo, pos);
                    return toFix.insert(toFix.length()-2, ':');
                };

                public Date parse(String text, ParsePosition pos) {
                    int indexOf = text.indexOf(':', text.length() - 4);
                    if (indexOf > 0) {
                        text = text.substring(0, indexOf) + text.substring(indexOf+1, text.length());
                        return super.parse(text, pos);
                    } else {
                        return null;
                    }

                }

            };
        }
    };

    private static final ThreadLocal<DateFormat> df2 = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        }
    };

    private static final ThreadLocal<DateFormat> df3 = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };
}
