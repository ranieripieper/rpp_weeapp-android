package br.com.weeapp.service.background;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;

import br.com.weeapp.model.dao.LanguageDao;
import br.com.weeapp.service.RetrofitManager;

/**
 * Created by ranipieper on 8/20/15.
 */
@EIntentService
public class LogoutBackgroundService extends IntentService {

    private static final String TAG = "LogoutBackgroundService";

    public LogoutBackgroundService() {
        super("LogoutBackgroundService");
    }

    @Bean
    LanguageDao languageDao;

    @Override
    protected void onHandleIntent(Intent intent) {
        synchronized (TAG) {

        }
    }

    @ServiceAction
    void logout(String authToken, String userId) {
        try {
            RetrofitManager.getInstance().getUserService().logout(authToken, userId);
        } catch(RuntimeException e) {

        } catch(Exception e) {

        }
    }

    public static void callService(Context context, String authToken, String userId) {
        LogoutBackgroundService_.intent(context)
                .logout(authToken, userId)
                .start();
    }

}
