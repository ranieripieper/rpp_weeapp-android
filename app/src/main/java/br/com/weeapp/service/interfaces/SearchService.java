package br.com.weeapp.service.interfaces;

import br.com.weeapp.model.result.SearchResult;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by ranipieper on 8/21/15.
 */
public interface SearchService {

    @GET("/search.json")
    void search(@Query("level") Integer level,
                @Query("skype") Boolean skype,
                @Query("hangout") Boolean hangout,
                @Query("schedule") String schedule,
                @Query("language") Long language,
                @Query("page") int page,
                @Query("per_page") int perPage,
                retrofit.Callback<SearchResult> callback);

}
