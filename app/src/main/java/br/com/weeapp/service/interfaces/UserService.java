package br.com.weeapp.service.interfaces;

import java.util.List;

import br.com.weeapp.model.User;
import br.com.weeapp.model.result.MyReservesResult;
import br.com.weeapp.model.result.RecoveyPasswordResult;
import br.com.weeapp.service.RetrofitManager;
import br.com.weeapp.service.exception.ServiceException;
import retrofit.client.Response;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by ranipieper on 8/19/15.
 */
public interface UserService {

    @POST("/users.json")
    @Multipart
    void sign_up(@Part("avatar") TypedFile file, @Part("name") String name,
                 @Part("email") String email, @Part("password") String password,
                 @Part("password_confirmation") String passwordConfirmation,
                 @Part("device_token") String deviceToken,
                 retrofit.Callback<User> callback);

    @PUT("/users/{id}/edit.json")
    @Multipart
    void edit(@Path("id") long id, @Part("avatar") TypedFile file, @Part("name") String name,
                 @Part("email") String email, @Part("password") String password,
                 @Part("password_confirmation") String passwordConfirmation,
                 retrofit.Callback<User> callback);

    @PUT("/users/{id}/edit.json")
    @Multipart
    void edit(@Path("id") long id, @Part("avatar") String avatar, @Part("name") String name,
              @Part("email") String email, @Part("password") String password,
              @Part("password_confirmation") String passwordConfirmation,
              retrofit.Callback<User> callback);

    @FormUrlEncoded
    @POST("/users/sign_in.json")
    void loginFacebook(@Field("access_token") String accessToken, @Field("email") String email,
                       @Field("device_token") String deviceToken,
                 retrofit.Callback<User> callback);

    @FormUrlEncoded
    @POST("/users/sign_in.json")
    void login(@Field("email") String email, @Field("password") String password,
               @Field("device_token") String deviceToken,
               retrofit.Callback<User> callback);

    @DELETE("/users/sign_out.json")
    Response logout(@Query(RetrofitManager.HEADER_AUTH_TOKEN) String token, @Query(RetrofitManager.HEADER_ID) String userId) throws ServiceException;

    @FormUrlEncoded
    @POST("/users/password")
    void recoveyPassword(@Field("email") String email, retrofit.Callback<RecoveyPasswordResult> callback);


    @DELETE("/users/{id}.json")
    void remove(@Path("id") long id, retrofit.Callback<Response> callback);

    @GET("/users/{user_id}/reserves.json")
    void myReserves(@Path("user_id") Long UserId, @Query("filter") String type, retrofit.Callback<List<MyReservesResult>> callback);
}
