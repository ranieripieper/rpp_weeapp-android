package br.com.weeapp.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.ConcurrentHashMap;

import br.com.weeapp.BuildConfig;
import br.com.weeapp.service.interfaces.PagSeguroService;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.SimpleXMLConverter;

/**
 * Created by test on 8/15/15.
 */
public class RetrofitManagerPagSeguro {

    private static String API_HOST = "https://ws.sandbox.pagseguro.uol.com.br/";
    public RestAdapter restAdapter;
    final ConcurrentHashMap<Class, Object> services;
    private static RetrofitManagerPagSeguro instance;

    private RetrofitManagerPagSeguro() {
        services = new ConcurrentHashMap();
        initialize();
    }

    public static RetrofitManagerPagSeguro getInstance() {
        if (instance == null) {
            instance = new RetrofitManagerPagSeguro();
        }
        return instance;
    }

    private void initialize() {

        Gson gson = new GsonBuilder().create();

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_HOST)
                .setClient(new OkClient(new OkHttpClient()))
                .setConverter(new SimpleXMLConverter())
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();
    }

    public PagSeguroService getPagSeguroService() {
        return (PagSeguroService)this.getService(PagSeguroService.class);
    }

    protected <T> Object getService(Class<T> cls) {
        if(!this.services.contains(cls)) {
            this.services.putIfAbsent(cls, this.restAdapter.create(cls));
        }

        return this.services.get(cls);
    }
}
