package br.com.weeapp.service.interfaces;

import br.com.weeapp.model.result.TipResult;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by ranipieper on 8/24/15.
 */
public interface TipService {

    @GET("/tips.json")
    void getTips(@Query("page") int page,
                 @Query("per_page") int perPage, retrofit.Callback<TipResult> callback);
}
