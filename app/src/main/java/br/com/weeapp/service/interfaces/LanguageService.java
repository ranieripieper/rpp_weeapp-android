package br.com.weeapp.service.interfaces;

import java.util.List;

import br.com.weeapp.model.Language;
import retrofit.http.GET;

/**
 * Created by ranipieper on 8/19/15.
 */
public interface LanguageService {

    @GET("/languages.json")
    List<Language> getLanguages();

    @GET("/languages.json")
    void getLanguages(retrofit.Callback<List<Language>> callback);

}
