package br.com.weeapp.service.background;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;

import java.util.List;

import br.com.weeapp.model.Reserve;
import br.com.weeapp.model.User;
import br.com.weeapp.model.dao.LanguageDao;
import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.service.RetrofitManager;
import retrofit.client.Response;

/**
 * Created by ranipieper on 8/27/15.
 */
@EIntentService
public class CancelReservesService extends IntentService {

    private static final String TAG = "CancelReservesService";

    public CancelReservesService() {
        super("CancelReservesService");
    }

    @Bean
    LanguageDao languageDao;

    @Override
    protected void onHandleIntent(Intent intent) {
        synchronized (TAG) {

        }
    }

    @ServiceAction
    void cancelReserves(List<Reserve> reserves) {
        User user = SharedPrefManager.getInstance().getUser();
        try {
            String reservesStr = "";
            if (reserves != null && !reserves.isEmpty()) {
                for (Reserve reserve : reserves) {
                    reservesStr += reserve.getId() + ",";
                }
                reservesStr = reservesStr.substring(0, reservesStr.length()-1);
            }

            Response r = RetrofitManager.getInstance().getOrderService().cancelReserves(user.getId(), reservesStr);
        } catch(RuntimeException e) {

        } catch(Exception e) {

        }
    }

    public static void callService(Context context, List<Reserve> reserves) {
        CancelReservesService_.intent(context)
                .cancelReserves(reserves)
                .start();
    }

}
