package br.com.weeapp;

import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.SaveCallback;

import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.service.background.LanguagesBackgroundService;
import br.com.weeapp.util.Constants;
import io.fabric.sdk.android.Fabric;

/**
 * Created by ranipieper on 8/19/15.
 */
public class WeeAppApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initCrashlytics();

        // Init SharedPrefManager
        SharedPrefManager.init(this);

        // Init local database
        ActiveAndroid.initialize(this);

        LanguagesBackgroundService.callService(this);

        configImageLoader();

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        Parse.initialize(this, Constants.PARSE_APP_ID, Constants.PARSE_CLIENT_KEY);

        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                String deviceToken = (String) ParseInstallation.getCurrentInstallation().get("deviceToken");
                SharedPrefManager.getInstance().setDeviceToken(deviceToken);
            }
        });
    }

    private void configImageLoader() {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img_teacher_place_holder)
                .showImageForEmptyUri(R.drawable.img_teacher_place_holder)
                .showImageOnFail(R.drawable.img_teacher_place_holder)
                .resetViewBeforeLoading(false)
                .delayBeforeLoading(100).considerExifParams(true)
                .cacheInMemory(true) // default
                .cacheOnDisc(true).build(); // default

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this).defaultDisplayImageOptions(options) // default
                // .writeDebugLogs()
                //.imageDownloader(new AuthImageDownloader(this, 1000, 1000))
                .build();

        ImageLoader.getInstance().init(config);
    }

    private void initCrashlytics() {
        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder()
                        .disabled(BuildConfig.DEBUG)
                        .build())
                .build();
        // Initialize Fabric with the debug-disabled crashlytics.
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, crashlyticsKit, new Answers(), new Crashlytics());
        } else {
            Fabric.with(this, crashlyticsKit);
        }
    }
}