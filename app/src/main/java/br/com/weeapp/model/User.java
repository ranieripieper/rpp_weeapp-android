package br.com.weeapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ranipieper on 8/19/15.
 */
public class User {

    /*
    {
  "id": 2,
  "name": "Roberto Capelo",
  "facebook_uid": null,
  "created_at": "2015-08-19T21:34:47.219Z",
  "updated_at": "2015-08-19T21:34:47.319Z",
  "email": "teste@gmail.com",
  "auth_token": "Fkgz8AxWQunUy5AUk5W7",
  "avatar": "https://pbs.twimg.com/profile_images/378800000812130596/ea07672039a442bb98e35f22c162878b_400x400.png"
}

     */
    private Long id;

    private String name;

    @SerializedName("facebook_uid")
    private String facebookUid;

    @SerializedName("created_at")
    private Date createdAt;

    @SerializedName("updated_at")
    private Date updatedAt;

    private String email;

    @SerializedName("auth_token")
    private String authToken;

    private String avatar;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFacebookUid() {
        return facebookUid;
    }

    public void setFacebookUid(String facebookUid) {
        this.facebookUid = facebookUid;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }


}
