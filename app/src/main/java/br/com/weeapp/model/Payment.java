package br.com.weeapp.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by ranipieper on 8/27/15.
 */
@Table(name = "Payment")
public class Payment extends Model {

    @Column(name = "orderId")
    private Long orderId;

    @Column(name = "transactionId")
    private String transactionId;

    @Column(name = "paid")
    private boolean paid;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }
}
