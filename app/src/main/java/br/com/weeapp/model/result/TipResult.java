package br.com.weeapp.model.result;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import br.com.weeapp.model.Tip;

/**
 * Created by ranipieper on 8/24/15.
 */
public class TipResult {

    private ArrayList<Tip> tips;

    @SerializedName("TODO")
    private Pagination pagination;

    public ArrayList<Tip> getTips() {
        return tips;
    }

    public void setTips(ArrayList<Tip> tips) {
        this.tips = tips;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}
