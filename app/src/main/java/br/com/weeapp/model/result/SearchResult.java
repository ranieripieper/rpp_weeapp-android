package br.com.weeapp.model.result;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import br.com.weeapp.model.Teacher;

/**
 * Created by ranipieper on 8/24/15.
 */
public class SearchResult {

    private ArrayList<Teacher> teachers;

    @SerializedName("TODO")
    private Pagination pagination;

    public ArrayList<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(ArrayList<Teacher> teachers) {
        this.teachers = teachers;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}
