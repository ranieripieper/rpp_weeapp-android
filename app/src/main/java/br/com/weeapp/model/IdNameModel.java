package br.com.weeapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ranipieper on 8/24/15.
 */
public class IdNameModel implements Parcelable {

    private Integer id;

    private String name;

    public IdNameModel() {
    }

    public IdNameModel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
    }

    protected IdNameModel(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
    }

    public static final Parcelable.Creator<IdNameModel> CREATOR = new Parcelable.Creator<IdNameModel>() {
        public IdNameModel createFromParcel(Parcel source) {
            return new IdNameModel(source);
        }

        public IdNameModel[] newArray(int size) {
            return new IdNameModel[size];
        }
    };
}
