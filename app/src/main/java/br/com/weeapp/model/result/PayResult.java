package br.com.weeapp.model.result;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 8/31/15.
 */
public class PayResult {

    @SerializedName("redirect_url")
    private String redirectUrl;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
