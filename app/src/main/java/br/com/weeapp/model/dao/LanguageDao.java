package br.com.weeapp.model.dao;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import org.androidannotations.annotations.EBean;

import java.util.List;

import br.com.weeapp.model.Language;
import br.com.weeapp.util.StringUtil;

/**
 * Created by ranipieper on 8/20/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class LanguageDao extends BaseDao<Language> {

    @Override
    public Class<Language> getModelClass() {
        return Language.class;
    }

    public void saveAll(List<Language> lst) {
        if (lst != null) {
            for (Language model : lst) {
                model.setNameNormalize(StringUtil.stripAccents(model.getName()));
                model.save();
            }
        }
    }

    public void deleteAll() {
        new Delete().from(Language.class).execute();
    }

    public Language getByName(String name) {
        name = StringUtil.stripAccents(name);
        return new Select().from(Language.class).where("nameNormalize = ?", StringUtil.stripAccents(name)).executeSingle();

    }

    public List<Language> getAll() {
        return new Select().from(getModelClass()).orderBy("nameNormalize asc").execute();
    }


}
