package br.com.weeapp.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

import br.com.weeapp.model.Teacher;

/**
 * Created by ranipieper on 8/31/15.
 */
public class MyReservesResult implements Parcelable {

    private Date date;
    private Teacher teacher;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(date != null ? date.getTime() : -1);
        dest.writeParcelable(this.teacher, 0);
    }

    public MyReservesResult() {
    }

    protected MyReservesResult(Parcel in) {
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        this.teacher = in.readParcelable(Teacher.class.getClassLoader());
    }

    public static final Creator<MyReservesResult> CREATOR = new Creator<MyReservesResult>() {
        public MyReservesResult createFromParcel(Parcel source) {
            return new MyReservesResult(source);
        }

        public MyReservesResult[] newArray(int size) {
            return new MyReservesResult[size];
        }
    };

    public boolean showContacts( boolean nextLessons) {
        if (nextLessons) {
            return true;
        }
        return false;
    }
}
