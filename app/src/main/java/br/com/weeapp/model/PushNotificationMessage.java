package br.com.weeapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PushNotificationMessage implements Parcelable {

	public static final int SCREEN_TIP = 1;
	public static final int SCREEN_NEXT_LESSONS = 2;
	public static final int SCREEN_LAST_LESSONS = 3;

	@SerializedName("alert")
	private String message;

	@SerializedName("screen")
	private int screen;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getScreen() {
		return screen;
	}

	public void setScreen(int screen) {
		this.screen = screen;
	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.message);
		dest.writeInt(this.screen);
	}

	public PushNotificationMessage() {
	}

	protected PushNotificationMessage(Parcel in) {
		this.message = in.readString();
		this.screen = in.readInt();
	}

	public static final Parcelable.Creator<PushNotificationMessage> CREATOR = new Parcelable.Creator<PushNotificationMessage>() {
		public PushNotificationMessage createFromParcel(Parcel source) {
			return new PushNotificationMessage(source);
		}

		public PushNotificationMessage[] newArray(int size) {
			return new PushNotificationMessage[size];
		}
	};
}
