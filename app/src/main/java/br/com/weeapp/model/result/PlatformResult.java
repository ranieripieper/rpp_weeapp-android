package br.com.weeapp.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import br.com.weeapp.model.IdNameModel;

/**
 * Created by ranipieper on 8/31/15.
 */
public class PlatformResult implements Parcelable {
    private IdNameModel platform;
    private String contact;

    public IdNameModel getPlatform() {
        return platform;
    }

    public void setPlatform(IdNameModel platform) {
        this.platform = platform;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.platform, 0);
        dest.writeString(this.contact);
    }

    public PlatformResult() {
    }

    protected PlatformResult(Parcel in) {
        this.platform = in.readParcelable(IdNameModel.class.getClassLoader());
        this.contact = in.readString();
    }

    public static final Parcelable.Creator<PlatformResult> CREATOR = new Parcelable.Creator<PlatformResult>() {
        public PlatformResult createFromParcel(Parcel source) {
            return new PlatformResult(source);
        }

        public PlatformResult[] newArray(int size) {
            return new PlatformResult[size];
        }
    };
}
