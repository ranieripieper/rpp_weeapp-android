package br.com.weeapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 9/11/15.
 */
public class TeacherLanguages implements Parcelable {
    private Long id;
    @SerializedName("language_id")
    private Long languageId;
    private Language language;
    @SerializedName("language_levels")
    private List<LanguageLevel> languageLevels;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public List<LanguageLevel> getLanguageLevels() {
        return languageLevels;
    }

    public void setLanguageLevels(List<LanguageLevel> languageLevels) {
        this.languageLevels = languageLevels;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.languageId);
        dest.writeParcelable(this.language, 0);
        dest.writeTypedList(languageLevels);
    }

    public TeacherLanguages() {
    }

    protected TeacherLanguages(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.languageId = (Long) in.readValue(Long.class.getClassLoader());
        this.language = in.readParcelable(Language.class.getClassLoader());
        this.languageLevels = in.createTypedArrayList(LanguageLevel.CREATOR);
    }

    public static final Creator<TeacherLanguages> CREATOR = new Creator<TeacherLanguages>() {
        public TeacherLanguages createFromParcel(Parcel source) {
            return new TeacherLanguages(source);
        }

        public TeacherLanguages[] newArray(int size) {
            return new TeacherLanguages[size];
        }
    };
}
