package br.com.weeapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ranipieper on 8/5/15.
 */
public class Tip {

    private Long id;
    private String description;
    @SerializedName("created_at")
    private Date createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
