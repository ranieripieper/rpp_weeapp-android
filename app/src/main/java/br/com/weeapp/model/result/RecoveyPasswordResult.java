package br.com.weeapp.model.result;

/**
 * Created by ranipieper on 8/27/15.
 */
public class RecoveyPasswordResult {

    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
