package br.com.weeapp.model.dao;

import org.androidannotations.annotations.EBean;

import br.com.weeapp.model.Payment;

/**
 * Created by ranipieper on 8/20/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PaymentDao extends BaseDao<Payment> {

    @Override
    public Class<Payment> getModelClass() {
        return Payment.class;
    }

}
