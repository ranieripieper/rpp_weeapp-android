package br.com.weeapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.Date;

import br.com.weeapp.enumeration.PeriodEnun;
import br.com.weeapp.util.DateUtil;

/**
 * Created by ranipieper on 8/5/15.
 */
public class Lesson implements Parcelable {
    private Long id;
    private Date date;
    private Language language;
    private IdNameModel level;
    private Teacher teacher;
    @SerializedName("reservelesson")
    private Reserve reserve;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public String getDateToCompare() {
        return DateUtil.diaMesAno.get().format(this.getDate());
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public IdNameModel getLevel() {
        return level;
    }

    public void setLevel(IdNameModel level) {
        this.level = level;
    }

    public String getHour() {
        return DateUtil.horaMin.get().format(this.date);
    }

    public PeriodEnun getPeriodEnun() {
        //Periodo da manhã 6h até 11:59
        //Periodo da tarde 12h até 17:59
        //Periodo da noite 18h até 23:59
        //Periodo da madrugada 0h até 5:59
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.date);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        if (hour >= 6 && hour < 12) {
            return PeriodEnun.MANHA;
        } else if (hour >= 12 && hour < 18) {
            return PeriodEnun.TARDE;
        } else if (hour >= 18 && hour < 24) {
            return PeriodEnun.NOITE;
        } else if (hour >= 0 && hour < 6) {
            return PeriodEnun.MADRUGADA;
        }
        return null;
    }

    public Reserve getReserve() {
        return reserve;
    }

    public void setReserve(Reserve reserve) {
        this.reserve = reserve;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeLong(date != null ? date.getTime() : -1);
        dest.writeParcelable(this.language, flags);
        dest.writeParcelable(this.reserve, flags);
        dest.writeParcelable(this.level, 0);
        dest.writeParcelable(this.teacher, 0);
    }

    public Lesson() {
    }

    protected Lesson(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        this.language = in.readParcelable(Language.class.getClassLoader());
        this.reserve = in.readParcelable(Reserve.class.getClassLoader());
        this.level = in.readParcelable(IdNameModel.class.getClassLoader());
        this.teacher = in.readParcelable(Teacher.class.getClassLoader());
    }

    public static final Parcelable.Creator<Lesson> CREATOR = new Parcelable.Creator<Lesson>() {
        public Lesson createFromParcel(Parcel source) {
            return new Lesson(source);
        }

        public Lesson[] newArray(int size) {
            return new Lesson[size];
        }
    };
}
