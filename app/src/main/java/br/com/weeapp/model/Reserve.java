package br.com.weeapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ranipieper on 8/25/15.
 */
public class Reserve implements Parcelable {

    private Long id;
    @SerializedName("lesson_id")
    private Long lessonId;
    @SerializedName("date")
    private Date date;
    private Lesson lesson;
    private Float rate;
    @SerializedName("order_id")
    private Long orderId;
    private Language language;
    private IdNameModel level;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getLessonId() {
        return lessonId;
    }

    public void setLessonId(Long lessonId) {
        this.lessonId = lessonId;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public IdNameModel getLevel() {
        return level;
    }

    public void setLevel(IdNameModel level) {
        this.level = level;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.lessonId);
        dest.writeLong(date != null ? date.getTime() : -1);
        dest.writeParcelable(this.lesson, 0);
        dest.writeValue(this.rate);
        dest.writeValue(this.orderId);
        dest.writeParcelable(this.language, 0);
        dest.writeParcelable(this.level, 0);
    }

    public Reserve() {
    }

    protected Reserve(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.lessonId = (Long) in.readValue(Long.class.getClassLoader());
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        this.lesson = in.readParcelable(Lesson.class.getClassLoader());
        this.rate = (Float) in.readValue(Float.class.getClassLoader());
        this.orderId = (Long) in.readValue(Long.class.getClassLoader());
        this.language = in.readParcelable(Language.class.getClassLoader());
        this.level = in.readParcelable(IdNameModel.class.getClassLoader());
    }

    public static final Creator<Reserve> CREATOR = new Creator<Reserve>() {
        public Reserve createFromParcel(Parcel source) {
            return new Reserve(source);
        }

        public Reserve[] newArray(int size) {
            return new Reserve[size];
        }
    };
}
