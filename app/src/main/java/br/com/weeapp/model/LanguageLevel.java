package br.com.weeapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ranipieper on 9/11/15.
 */
public class LanguageLevel implements Parcelable {
    private Long id;
    private IdNameModel level;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IdNameModel getLevel() {
        return level;
    }

    public void setLevel(IdNameModel level) {
        this.level = level;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeParcelable(this.level, 0);
    }

    public LanguageLevel() {
    }

    protected LanguageLevel(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.level = in.readParcelable(IdNameModel.class.getClassLoader());
    }

    public static final Parcelable.Creator<LanguageLevel> CREATOR = new Parcelable.Creator<LanguageLevel>() {
        public LanguageLevel createFromParcel(Parcel source) {
            return new LanguageLevel(source);
        }

        public LanguageLevel[] newArray(int size) {
            return new LanguageLevel[size];
        }
    };
}
