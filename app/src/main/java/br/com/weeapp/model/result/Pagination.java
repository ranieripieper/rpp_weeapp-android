package br.com.weeapp.model.result;

/**
 * Created by ranipieper on 8/24/15.
 */
public class Pagination {

    private int page;
    private int total;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
