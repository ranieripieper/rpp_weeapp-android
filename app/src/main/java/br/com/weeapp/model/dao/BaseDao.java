package br.com.weeapp.model.dao;

import com.activeandroid.Model;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by ranipieper on 8/20/15.
 */
public abstract class BaseDao<T extends Model> {

    public int count() {
        return new Select().from(getModelClass()).count();
    }

    public void saveAll(List<T> lst) {
        if (lst != null) {
            for (Model model : lst) {
                model.save();
            }
        }
    }

    public void deleteAll(Class clazz) {
        new Delete().from(clazz).executeSingle();
    }

    public List<T> getAll() {
        return new Select().from(getModelClass()).execute();
    }

    public T getById(Long id) {
        return new Select().from(getModelClass()).where("id = ? ", id).executeSingle();
    }

    public abstract Class<T> getModelClass();
}
