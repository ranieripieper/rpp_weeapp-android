package br.com.weeapp.model.result;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by ranipieper on 8/30/15.
 */

//<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?><checkout><code>7AC6D3291F1F4F9AA447AFA97346F232</code><date>2015-08-30T19:10:34.000-03:00</date></checkout>

@Root(name = "checkout")
public class PagSeguroResult {

    @Element(name = "code", required=true)
    private String code;

    @Element(name = "date", required=false)
    private String date;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
