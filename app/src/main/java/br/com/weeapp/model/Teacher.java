package br.com.weeapp.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashSet;
import java.util.List;

import br.com.weeapp.util.StringUtil;

/**
 * Created by ranipieper on 8/6/15.
 */
public class Teacher implements Parcelable {

    private Long id;
    private String name;
    @SerializedName("avatarurl")
    private String avatar;
    private Float rate;
    private String skype;
    private String hangout;
    @SerializedName("teacher_languages")
    private List<TeacherLanguages> teacherLanguages;
    @SerializedName("teacher_lessons")
    private List<Lesson> lessons;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public String getRateStr() {
        return String.format("%.1f", rate);
    }

    public String getSkype() {
        return skype;
    }

    public String getHangout() {
        return hangout;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public boolean isSkypeAvaiable() {
        return !TextUtils.isEmpty(this.skype);
    }

    public boolean isHangoutAvaiable() {
        return !TextUtils.isEmpty(this.hangout);
    }

    public Teacher() {
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public void setHangout(String hangout) {
        this.hangout = hangout;
    }

    public List<TeacherLanguages> getTeacherLanguages() {
        return teacherLanguages;
    }

    public void setTeacherLanguages(List<TeacherLanguages> teacherLanguages) {
        this.teacherLanguages = teacherLanguages;
    }

    public String getLanguagesStr() {
        String result = "";
        if (this.getTeacherLanguages() != null) {
            for (TeacherLanguages language : this.getTeacherLanguages()) {
                result += language.getLanguage().getName() + StringUtil.SEPARATOR;
            }
            result = StringUtil.adjustString(result);
        }
        return result;
    }

    public String getAllLevelsStr() {
        LinkedHashSet<String> allLevels = new LinkedHashSet();
        String result = "";
        if (this.getTeacherLanguages() != null) {
            for (TeacherLanguages language : this.getTeacherLanguages()) {
                if (language.getLanguageLevels() != null) {
                    for (LanguageLevel languageLevel : language.getLanguageLevels()) {
                        if (!allLevels.contains(language.getLanguageLevels())) {
                            allLevels.add(languageLevel.getLevel().getName());
                        }
                    }
                }
            }
        }
        for (String level : allLevels) {
            result += level + StringUtil.SEPARATOR;
        }
        result = StringUtil.adjustString(result);
        return result;
    }

    public String getLevelsStr(Long languageId) {
        LinkedHashSet<String> allLevels = new LinkedHashSet();
        String result = "";
        if (this.getTeacherLanguages() != null) {
            for (TeacherLanguages language : this.getTeacherLanguages()) {
                if (language.getLanguageId().equals(languageId)) {
                    if (language.getLanguageLevels() != null) {
                        for (LanguageLevel languageLevel : language.getLanguageLevels()) {
                            if (!allLevels.contains(language.getLanguageLevels())) {
                                allLevels.add(languageLevel.getLevel().getName());
                            }
                        }
                    }
                    break;
                }
            }
        }
        for (String level : allLevels) {
            result += level + StringUtil.SEPARATOR;
        }
        result = StringUtil.adjustString(result);
        return result;
    }

    public boolean showBtAvaliarProf(boolean nextLessons) {
        if (!nextLessons && (getLessons() != null)) {
            for (Lesson lesson : getLessons()) {
                if (lesson.getReserve() != null && (lesson.getReserve().getRate() == null || lesson.getReserve().getRate() == 0)) {
                    return true;
                }
            }

        }
        return false;

    }

    public Language getLanguage(Long languageId) {
        if (getTeacherLanguages() != null) {
            for(TeacherLanguages tmp : getTeacherLanguages()) {
                if (languageId.equals(tmp.getLanguageId())) {
                    return tmp.getLanguage();
                }
            }
        }
        return null;
    }
    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.avatar);
        dest.writeValue(this.rate);
        dest.writeString(this.skype);
        dest.writeString(this.hangout);
        dest.writeTypedList(teacherLanguages);
        dest.writeTypedList(lessons);
    }

    protected Teacher(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.name = in.readString();
        this.avatar = in.readString();
        this.rate = (Float) in.readValue(Float.class.getClassLoader());
        this.skype = in.readString();
        this.hangout = in.readString();
        this.teacherLanguages = in.createTypedArrayList(TeacherLanguages.CREATOR);
        this.lessons = in.createTypedArrayList(Lesson.CREATOR);
    }

    public static final Creator<Teacher> CREATOR = new Creator<Teacher>() {
        public Teacher createFromParcel(Parcel source) {
            return new Teacher(source);
        }

        public Teacher[] newArray(int size) {
            return new Teacher[size];
        }
    };
}