package br.com.weeapp.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.List;

import br.com.weeapp.util.StringUtil;

/**
 * Created by ranipieper on 8/20/15.
 */
@Table(name = "Languages", id = BaseColumns._ID)
public class Language extends Model implements Parcelable {

    @Column( index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE )
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "nameNormalize")
    private String nameNormalize;

    private List<IdNameModel> levels;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMyId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameNormalize() {
        return nameNormalize;
    }

    public void setNameNormalize(String nameNormalize) {
        this.nameNormalize = nameNormalize;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.nameNormalize);
        dest.writeTypedList(levels);
    }

    public Language() {
    }

    public Language(long id, String name) {
        this.id = id;
        this.name = name;
        this.nameNormalize = StringUtil.stripAccents(this.name);
    }

    protected Language(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.name = in.readString();
        this.nameNormalize = in.readString();
        this.levels = in.createTypedArrayList(IdNameModel.CREATOR);
    }

    public static final Parcelable.Creator<Language> CREATOR = new Parcelable.Creator<Language>() {
        public Language createFromParcel(Parcel source) {
            return new Language(source);
        }

        public Language[] newArray(int size) {
            return new Language[size];
        }
    };

    public List<IdNameModel> getLevels() {
        return levels;
    }

}
