package br.com.weeapp.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import br.com.weeapp.R;
import me.ranieripieper.androidutils.viewlib.font.FontUtilCache;

/**
 * Created by ranipieper on 8/3/15.
 */
public class EditText extends com.rey.material.widget.EditText {

    private boolean required = false;
    private String regexValidation = "";
    private int errorMessage = -1;
    private int minLength = -1;

    private OnOkDoneClick mOnOkDoneClick;

    public EditText(Context context) {
        super(context);
    }

    public EditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public EditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private boolean validateField() {
        if (required || !TextUtils.isEmpty(regexValidation)) {
            return true;
        }
        return false;
    }

    private void init(Context context, AttributeSet attrs) {
        loadAttributes(context, attrs);

        if (validateField()) {
            setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        isValid();
                    }
                }
            });
        }

    }

    private void loadAttributes(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.EditTextPlus);
        a.recycle();
        setDefaultAttrs(ctx, attrs);
    }

    private void setDefaultAttrs(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs,
                R.styleable.TextViewPlus);
        TypedArray defaultAttr = ctx.obtainStyledAttributes(attrs,
                R.styleable.DDSDefaultAttributes);

        String customFont = defaultAttr
                .getString(R.styleable.DDSDefaultAttributes_text_font);
        required = defaultAttr.getBoolean(
                R.styleable.DDSDefaultAttributes_required, false);
        regexValidation = defaultAttr
                .getString(R.styleable.DDSDefaultAttributes_regex_validation);

        defaultAttr.recycle();

        TypedArray weEditTextAttributes_error_message = ctx.obtainStyledAttributes(attrs, R.styleable.WeeEditTextAttributes);

        errorMessage = weEditTextAttributes_error_message
                .getResourceId(R.styleable.WeeEditTextAttributes_error_message, -1);

        minLength = weEditTextAttributes_error_message
                .getInteger(R.styleable.WeeEditTextAttributes_min_length, -1);


        defaultAttr.recycle();

        FontUtilCache.setCustomFont(ctx, mLabelView, customFont);
        FontUtilCache.setCustomFont(ctx, mInputView, customFont);
        a.recycle();
    }

    public String getTextStr() {
        return getText().toString();
    }
    public boolean isValid() {
        if (required && TextUtils.isEmpty(this.getText().toString())) {
            showErrorMsg();
            return false;
        }
        if (!TextUtils.isEmpty(regexValidation)) {
            if (!this.getText().toString().matches(regexValidation)) {
                showErrorMsg();
                return false;
            }
        }
        if (minLength > 0 && (required || (getText().length() > 0))) {
            if (minLength > getText().length()) {
                showErrorMsg(true);
                return false;
            }
        }

        this.setError(null);
        return true;
    }

    private void showErrorMsg() {
        showErrorMsg(false);
    }

    private void showErrorMsg(boolean length) {
        if (errorMessage == -1) {
            if (length) {
                this.setError(getContext().getString(R.string.msg_campo_invalido_length, this.getHint(), minLength));
            } else {
                this.setError(getContext().getString(R.string.msg_campo_invalido, this.getHint()));
            }

        } else {
            this.setError(getContext().getString(errorMessage));
        }
    }

    public boolean isNotValid() {
        return !isValid();
    }

    public void setonOkDoneClick(OnOkDoneClick onOkDoneClick) {
        this.mOnOkDoneClick = onOkDoneClick;
        this.setOnEditorActionListener(new TextView.OnEditorActionListener(){
            public boolean onEditorAction(TextView exampleView, int actionId, KeyEvent event){
                if(actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_NULL
                        || event.getKeyCode() == KeyEvent.KEYCODE_ENTER){

                    mOnOkDoneClick.onOkDoneClick(EditText.this);
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    public static abstract class OnOkDoneClick {
        public abstract void onOkDoneClick(EditText editText);
    }

    public static final void hiddenKeyboard(Context mContext, EditText edt) {
        if (edt != null) {
            InputMethodManager imm = (InputMethodManager) mContext
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
        }
    }

    public void hiddenKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindowToken(), 0);
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
