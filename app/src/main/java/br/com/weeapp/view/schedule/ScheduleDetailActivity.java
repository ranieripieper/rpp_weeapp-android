package br.com.weeapp.view.schedule;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import br.com.weeapp.R;
import br.com.weeapp.model.Lesson;
import br.com.weeapp.model.result.MyReservesResult;
import br.com.weeapp.service.RetrofitManager;
import br.com.weeapp.view.WeeDialogUtil;
import br.com.weeapp.view.base.BaseActivity;
import br.com.weeapp.view.custom.Button;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 8/31/15.
 */
@EActivity(R.layout.schedule_detail_activity)
public class ScheduleDetailActivity extends BaseActivity {

    public static final String EXTRA_SCHEDULE = "EXTRA_SCHEDULE";
    public static final String EXTRA_NEXT_LESSON = "EXTRA_NEXT_LESSON";

    @ViewById(R.id.txt_name)
    TextViewPlus txtName;
    @ViewById(R.id.skype_contact)
    View layoutSkype;
    @ViewById(R.id.hangout_contact)
    View layoutHangout;
    @ViewById(R.id.txt_skype)
    TextViewPlus txtSkype;
    @ViewById(R.id.txt_hangout)
    TextViewPlus txtHangout;
    @ViewById(R.id.img_photo)
    ImageView imgPhoto;
    @ViewById(R.id.txt_idioma)
    TextViewPlus txtIdioma;
    @ViewById(R.id.txt_nivel)
    TextViewPlus txtNivel;
    @ViewById(R.id.txt_avaliacao)
    TextViewPlus txtAvaliacao;
    @ViewById(R.id.txt_nr_aulas)
    TextViewPlus txNrAulas;
    @ViewById(R.id.bt_avaliar_professor)
    Button btAvaliar;

    @ViewById(R.id.txt_data_lesson)
    TextViewPlus txtDataLesson;
    @ViewById(R.id.txt_hour_lesson)
    TextViewPlus txtHourLesson;
    @ViewById(R.id.txt_language_level)
    TextViewPlus txtLanguageLevel;
    @ViewById(R.id.txt_duration)
    TextViewPlus txtDuration;

    @Extra(EXTRA_SCHEDULE)
    MyReservesResult obj;

    @Extra(EXTRA_NEXT_LESSON)
    boolean mNextLesson;

    @ViewById(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @ViewById(R.id.bt_avaliar_screen)
    Button btAvaliarscreen;
    @ViewById(R.id.txt_nr_aulas_screen)
    TextViewPlus txNrAulasScreen;

    private LessonDetailAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @AfterViews
    public void afterViews() {
        hideLogo();
        enableBackButton();

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setVisibility(View.VISIBLE);
        mAdapter = new LessonDetailAdapter(this, obj.getTeacher().getLessons());
        mRecyclerView.setAdapter(mAdapter);

        //StickyHeaderDecoration decor = new StickyHeaderDecoration((StickyHeaderAdapter)mAdapter);
        //mRecyclerView.addItemDecoration(decor);

        txtAvaliacao.setVisibility(View.VISIBLE);
        btAvaliar.setVisibility(View.GONE);

        if (obj.showContacts(mNextLesson)) {
            String skypeContact = obj.getTeacher().getSkype();
            String hangoutContact = obj.getTeacher().getHangout();

            if (!TextUtils.isEmpty(skypeContact)) {
                layoutSkype.setVisibility(View.VISIBLE);
                txtSkype.setText(skypeContact);
            } else {
                layoutSkype.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(hangoutContact)) {
                layoutHangout.setVisibility(View.VISIBLE);
                txtHangout.setText(hangoutContact);
            } else {
                layoutHangout.setVisibility(View.GONE);
            }
        } else {
            layoutHangout.setVisibility(View.GONE);
            layoutSkype.setVisibility(View.GONE);
        }

        final WeeDialogUtil.RatingListener ratingListener = new WeeDialogUtil.RatingListener() {
            @Override
            public void ratingClick(final MyReservesResult myReserve, final Integer rate) {
                showLoading();
                String ids = "";
                for (Lesson lesson : myReserve.getTeacher().getLessons()) {
                    if (lesson.getReserve().getRate() == null || lesson.getReserve().getRate().intValue() == 0) {
                        ids += lesson.getReserve().getId() + ",";
                    }
                    if (!TextUtils.isEmpty(ids)) {
                        ids = ids.substring(0,ids.length()-1);
                    }
                }
                RetrofitManager.getInstance().getOrderService().rate(ids, rate, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        hideLoading();
                        WeeDialogUtil.showDialog(ScheduleDetailActivity.this, R.string.avaliacao_enviada, R.string.obrigado);
                        for (Lesson lesson : myReserve.getTeacher().getLessons()) {
                            if (lesson.getReserve().getRate() == null || lesson.getReserve().getRate().intValue() == 0) {
                                lesson.getReserve().setRate(Float.valueOf(rate));
                            }
                        }
                        btAvaliar.setVisibility(View.GONE);
                        btAvaliarscreen.setVisibility(View.GONE);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showError(error, null);
                    }
                });
                hideLoading();
            }
        };

        btAvaliarscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WeeDialogUtil.showRatingDialog(ScheduleDetailActivity.this, obj, ratingListener);
            }
        });

        txtName.setText(obj.getTeacher().getName());
        ImageLoader.getInstance().displayImage(obj.getTeacher().getAvatar(), imgPhoto);

        if (obj.getTeacher() != null && obj.getTeacher().getTeacherLanguages() != null && !obj.getTeacher().getTeacherLanguages().isEmpty()) {
            txtIdioma.setText(obj.getTeacher().getLanguagesStr());
            txtIdioma.setVisibility(View.VISIBLE);
        } else {
            txtIdioma.setVisibility(View.GONE);
        }

        if (obj.getTeacher() != null && obj.getTeacher().getTeacherLanguages() != null && !obj.getTeacher().getTeacherLanguages().isEmpty()) {
            txtNivel.setText(obj.getTeacher().getAllLevelsStr());
            txtNivel.setVisibility(View.VISIBLE);
        } else {
            txtNivel.setVisibility(View.GONE);
        }

        if (obj.getTeacher() != null && obj.getTeacher().getRate() > 0) {
            txtAvaliacao.setText(getString(R.string.avaliacao, obj.getTeacher().getRateStr()));
        } else {
            txtAvaliacao.setText(getString(R.string.avaliacao, "-"));;
        }

        if (obj.getTeacher().getLessons() == null || obj.getTeacher().getLessons().size() == 1) {
            txNrAulasScreen.setText(getString(R.string.uma_aula));
        } else {
            txNrAulasScreen.setText(getString(R.string.x_aulas, obj.getTeacher().getLessons().size()));
        }
        txNrAulas.setVisibility(View.GONE);

        if (obj.getTeacher().showBtAvaliarProf(mNextLesson)) {
            btAvaliarscreen.setVisibility(View.VISIBLE);
        } else {
            btAvaliarscreen.setVisibility(View.GONE);
        }
    }

    public static void showActivity(Context ctx, MyReservesResult obj, boolean nextLesson) {
        Intent it = new Intent(ctx, ScheduleDetailActivity_.class);
        it.putExtra(EXTRA_NEXT_LESSON, nextLesson);
        it.putExtra(EXTRA_SCHEDULE, obj);
        ctx.startActivity(it);
    }
}
