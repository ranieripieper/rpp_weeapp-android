package br.com.weeapp.view.schedule;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import br.com.weeapp.R;
import br.com.weeapp.model.Lesson;
import br.com.weeapp.util.DateUtil;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;

/**
 * Created by yefeng on 8/5/15.
 * github:yefengfreedom
 */
public class LessonDetailAdapter extends RecyclerView.Adapter<LessonDetailAdapter.MyReserveViewHolder> implements StickyHeaderAdapter<LessonDetailAdapter.HeaderHolder> {
    private List<Lesson> mDatalist;
    private Context mContext;

    public LessonDetailAdapter(Context context, List<Lesson> list) {
        this.mContext = context;
        this.mDatalist = list;
    }

    @Override
    public MyReserveViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewtype) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_lesson_row, viewGroup, false);
        return new MyReserveViewHolder(v);
    }


    @Override
    public void onBindViewHolder(MyReserveViewHolder viewHolder, int position) {
        final Lesson obj = mDatalist.get(position);
        viewHolder.txtDataLesson.setText(DateUtil.diaMesAno.get().format(obj.getDate()));
        viewHolder.txtHourLesson.setText(DateUtil.horaMin.get().format(obj.getDate()));
        viewHolder.txtLanguageLevel.setText(obj.getLanguage().getName() + " " + obj.getLevel().getName());
    }

    @Override
    public int getItemCount() {
        return (null != mDatalist ? mDatalist.size() : 0);
    }

    //header
    @Override
    public long getHeaderId(int position) {
        final Lesson obj = mDatalist.get(position);
        try {
            Date dt = DateUtil.diaMesAno.get().parse(obj.getDateToCompare());
            return dt.getTime();
        } catch(ParseException e) {
            return 0l;
        }
    }

    @Override
    public HeaderHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.date_row, viewGroup, false);
        return new HeaderHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderHolder headerHolder, int position) {
        final Lesson obj = mDatalist.get(position);
        headerHolder.tvHeader.setText(DateUtil.diaMesAno.get().format(obj.getDate()));
    }

    /**
     * Header View Holder
     */
    protected class HeaderHolder extends RecyclerView.ViewHolder {
        public TextView tvHeader;

        public HeaderHolder(View itemView) {
            super(itemView);
            tvHeader = (TextView)itemView.findViewById(R.id.txt_header);
        }
    }

    /**
     * Row View Holder
     */
    public class MyReserveViewHolder extends RecyclerView.ViewHolder {
        public TextViewPlus txtDataLesson;
        public TextViewPlus txtHourLesson;
        public TextViewPlus txtLanguageLevel;

        public MyReserveViewHolder(View v) {
            super(v);
            txtDataLesson = (TextViewPlus) v.findViewById(R.id.txt_data_lesson);
            txtHourLesson = (TextViewPlus) v.findViewById(R.id.txt_hour_lesson);
            txtLanguageLevel = (TextViewPlus) v.findViewById(R.id.txt_language_level);
        }

    }

}