package br.com.weeapp.view.home;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.transition.TransitionInflater;
import android.view.Menu;
import android.view.MenuItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import br.com.weeapp.R;
import br.com.weeapp.model.PushNotificationMessage;
import br.com.weeapp.view.base.BaseActivity;
import br.com.weeapp.view.base.BaseFragment;
import br.com.weeapp.view.schedule.ScheduleFragment_;
import br.com.weeapp.view.search.SearchFragment_;
import br.com.weeapp.view.settings.SettingsFragment_;
import br.com.weeapp.view.tip.TipFragment_;

/**
 * Created by ranipieper on 7/30/15.
 */
@EActivity(R.layout.home_activity)
public class HomeActivity extends BaseActivity {

    private BaseFragment actualFragment;

    @Extra(EXTRA_SCREEN_NOTIFICATION)
    int screenNotification;

    private int hideMenu = R.id.action_search;

    @AfterViews
    void afterViews() {
        if (screenNotification == PushNotificationMessage.SCREEN_TIP) {
            showFragment(new TipFragment_(), false);
            hideMenu = R.id.action_tip;
        } else if (screenNotification == PushNotificationMessage.SCREEN_NEXT_LESSONS || screenNotification == PushNotificationMessage.SCREEN_LAST_LESSONS) {
            ScheduleFragment_ frag = new ScheduleFragment_();
            Bundle bunlde = new Bundle();
            bunlde.putInt(EXTRA_SCREEN_NOTIFICATION, screenNotification);
            frag.setArguments(bunlde);
            showFragment(frag, false);
            hideMenu = R.id.action_schedule;
        } else {
            showFragment(new SearchFragment_(), false);
            hideMenu = R.id.action_search;
        }

        //showFragment(new ScheduleFragment_(), false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        boolean result;
        if (id == R.id.action_settings) {
            hideMenu = R.id.action_settings;
            showFragment(new SettingsFragment_());
            result = true;
        } else if (id == R.id.action_tip) {
            hideMenu = R.id.action_tip;
            showFragment(new TipFragment_());
            result = true;
        } else if (id == R.id.action_schedule) {
            hideMenu = R.id.action_schedule;
            showFragment(new ScheduleFragment_());
            result = true;
        } else if (id == R.id.action_search) {
            hideMenu = R.id.action_search;
            showFragment(new SearchFragment_());
            result = true;
        } else {
            result = super.onOptionsItemSelected(item);
        }
        invalidateOptionsMenu();
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        if (hideMenu != -1) {
            menu.findItem(hideMenu).setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }


    public void showFragment(BaseFragment fragment) {
        showFragment(fragment, false);
    }

    public void showFragment(BaseFragment fragment, boolean addBackStack) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setSharedElementReturnTransition(TransitionInflater.from(this).inflateTransition(R.transition.change_image_transform));
            fragment.setEnterTransition(TransitionInflater.from(this).inflateTransition(android.R.transition.explode));

            if (actualFragment != null) {
                actualFragment.setSharedElementReturnTransition(TransitionInflater.from(this).inflateTransition(R.transition.change_image_transform));
                actualFragment.setExitTransition(TransitionInflater.from(this).inflateTransition(android.R.transition.explode));
            }
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        if (addBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
        }
        transaction.commit();
    }

}
