package br.com.weeapp.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import me.ranieripieper.androidutils.viewlib.font.FontUtilCache;

/**
 * Created by ranipieper on 8/3/15.
 */
public class Button extends com.rey.material.widget.Button {

    public Button(Context context) {
        super(context);
    }

    public Button(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public Button(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray defaultAttr = ctx.obtainStyledAttributes(attrs, me.ranieripieper.androidutils.viewlib.R.styleable.DDSDefaultAttributes);
        String customFont = defaultAttr.getString(me.ranieripieper.androidutils.viewlib.R.styleable.DDSDefaultAttributes_text_font);
        defaultAttr.recycle();
        FontUtilCache.setCustomFont(ctx, this, customFont);
    }
}
