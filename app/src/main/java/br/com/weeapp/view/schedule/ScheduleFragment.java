package br.com.weeapp.view.schedule;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import br.com.weeapp.R;
import br.com.weeapp.model.PushNotificationMessage;
import br.com.weeapp.view.base.BaseActivity;
import br.com.weeapp.view.base.BaseFragment;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;

/**
 * Created by ranipieper on 8/5/15.
 */
@EFragment(R.layout.schedule_fragment)
public class ScheduleFragment extends BaseFragment {

    @ViewById(R.id.viewpager)
    ViewPager viewPager;
    @ViewById(R.id.sliding_tabs)
    TabLayout tabLayout;

    @FragmentArg(BaseActivity.EXTRA_SCREEN_NOTIFICATION)
    int screenNotification;

    @AfterViews
    void afterViews() {
        SchedulePagerAdapter schedulePagerAdapter = new SchedulePagerAdapter(getChildFragmentManager(), getActivity());
        viewPager.setAdapter(schedulePagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(schedulePagerAdapter.getTabView(i));
        }

        if (screenNotification > 0 && screenNotification == PushNotificationMessage.SCREEN_NEXT_LESSONS) {
            viewPager.setCurrentItem(1);
        }
    }

    public class SchedulePagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;
        private String tabTitles[] = new String[] { getString(R.string.aulas_passadas), getString(R.string.proximas_aulas) };
        private Context context;

        public SchedulePagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            return SchedulePageFragment.newInstance(position == 1);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        public View getTabView(int position) {
            View v = LayoutInflater.from(context).inflate(R.layout.simple_textview, null);
            TextViewPlus tv = (TextViewPlus) v.findViewById(R.id.text);
            tv.setText(tabTitles[position]);
            return v;
        }
    }
}
