package br.com.weeapp.view.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.com.weeapp.R;

/**
 * Created by ranipieper on 8/10/15.
 */
public class RatingComponent extends LinearLayout implements View.OnClickListener {

    private RelativeLayout layoutNota1;
    private NotaButton btNota1;
    private NotaButton btNota2;
    private NotaButton btNota3;
    private NotaButton btNota4;
    private NotaButton btNota5;
    private NotaButton btNota6;
    private NotaButton btNota7;
    private NotaButton btNota8;
    private NotaButton btNota9;
    private NotaButton btNota10;

    private Integer nota = null;
    private NotaButton btSelected;

    private static int RES_ICON_SELECTED = R.drawable.circular_nota_selected_background;
    private static int RES_ICON_UNSELECTED = R.drawable.circular_nota_background;

    public RatingComponent(Context context) {
        super(context, null);
        init(context);
    }

    public RatingComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RatingComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v1 = inflater.inflate(R.layout.include_notas, this, true);
        View v2 = inflater.inflate(R.layout.include_notas_6_10, this, true);

        layoutNota1 = (RelativeLayout)v1.findViewById(R.id.layout_nota_1);
        btNota1 = (NotaButton)v1.findViewById(R.id.bt_nota_1);
        btNota2 = (NotaButton)v1.findViewById(R.id.bt_nota_2);
        btNota3 = (NotaButton)v1.findViewById(R.id.bt_nota_3);
        btNota4 = (NotaButton)v1.findViewById(R.id.bt_nota_4);
        btNota5 = (NotaButton)v1.findViewById(R.id.bt_nota_5);
        btNota6 = (NotaButton)v2.findViewById(R.id.bt_nota_6);
        btNota7 = (NotaButton)v2.findViewById(R.id.bt_nota_7);
        btNota8 = (NotaButton)v2.findViewById(R.id.bt_nota_8);
        btNota9 = (NotaButton)v2.findViewById(R.id.bt_nota_9);
        btNota10 = (NotaButton)v2.findViewById(R.id.bt_nota_10);

        postDelayed(new Runnable() {
            @Override
            public void run() {
                setVisibility(View.VISIBLE);
                configButtons();
            }
        }, 10);
    }

    private void configButtons() {
        int width = layoutNota1.getWidth();
        configButton(btNota1, width);
        configButton(btNota2, width);
        configButton(btNota3, width);
        configButton(btNota4, width);
        configButton(btNota5, width);
        configButton(btNota6, width);
        configButton(btNota7, width);
        configButton(btNota8, width);
        configButton(btNota9, width);
        configButton(btNota10, width);
    }

    private void configButton(NotaButton bt, int width) {
        bt.setRadius(width / 2);
        bt.setmIconSize(width - 2);
        bt.invalidate();
        bt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v instanceof NotaButton) {
            NotaButton bt = (NotaButton)v;

            if (btSelected == null || !bt.equals(btSelected)) {
                bt.setIcon(getResources().getDrawable(RES_ICON_SELECTED), true);
                if (btSelected != null) {
                    btSelected.setIcon(getResources().getDrawable(RES_ICON_UNSELECTED), true);
                }
            }

            if (bt.equals(btNota1)) {
                nota = 1;
                btSelected = btNota1;
            } else if (bt.equals(btNota2)) {
                nota = 2;
                btSelected = btNota2;
            } else if (bt.equals(btNota3)) {
                nota = 3;
                btSelected = btNota3;
            } else if (bt.equals(btNota4)) {
                nota = 4;
                btSelected = btNota4;
            } else if (bt.equals(btNota5)) {
                nota = 5;
                btSelected = btNota5;
            } else if (bt.equals(btNota6)) {
                nota = 6;
                btSelected = btNota6;
            } else if (bt.equals(btNota7)) {
                nota = 7;
                btSelected = btNota7;
            } else if (bt.equals(btNota8)) {
                nota = 8;
                btSelected = btNota8;
            } else if (bt.equals(btNota9)) {
                nota = 9;
                btSelected = btNota9;
            } else if (bt.equals(btNota10)) {
                nota = 10;
                btSelected = btNota10;
            }
        }
    }

    public Integer getNota() {
        return nota;
    }
}
