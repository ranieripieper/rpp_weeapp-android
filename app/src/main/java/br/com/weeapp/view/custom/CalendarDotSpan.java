package br.com.weeapp.view.custom;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.style.LineBackgroundSpan;

/**
 * Created by ranipieper on 8/6/15.
 */
public class CalendarDotSpan implements LineBackgroundSpan {
    public static final float DEFAULT_RADIUS = 3.0F;
    private final float radius;
    private final int color;

    public CalendarDotSpan() {
        this.radius = 3.0F;
        this.color = 0;
    }

    public CalendarDotSpan(int color) {
        this.radius = 3.0F;
        this.color = color;
    }

    public CalendarDotSpan(float radius) {
        this.radius = radius;
        this.color = 0;
    }

    public CalendarDotSpan(float radius, int color) {
        this.radius = radius;
        this.color = color;
    }

    public void drawBackground(Canvas canvas, Paint paint, int left, int right, int top, int baseline, int bottom, CharSequence charSequence, int start, int end, int lineNum) {
        int oldColor = paint.getColor();
        if(this.color != 0) {
            paint.setColor(this.color);
        }

        canvas.drawCircle(right - 2*this.radius, (float)top - 2*this.radius, this.radius, paint);
        paint.setColor(oldColor);
    }
}
