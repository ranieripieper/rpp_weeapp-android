package br.com.weeapp.view;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

import br.com.weeapp.R;
import br.com.weeapp.model.result.MyReservesResult;
import br.com.weeapp.util.StringUtil;
import br.com.weeapp.view.custom.Button;
import br.com.weeapp.view.custom.RatingComponent;

/**
 * Created by ranipieper on 7/31/15.
 */
public class WeeDialogUtil {

    public static void showError(Context context, int title, int msg, int textButton, View.OnClickListener buttonClick, boolean cancelOnTouchOutside) {
        showDialog(context, context.getString(title), context.getString(msg), context.getString(textButton), false, buttonClick, cancelOnTouchOutside, R.style.WeeAppTheme_ErrorDialog);
    }

    public static void showDialog(Context context, int title, int msg) {
        showDialog(context, context.getString(title), context.getString(msg), context.getString(R.string.fechar), false, null, true);
    }

    public static void showDialog(Context context, int title, int msg, int textButton, final boolean textButtonUnderline, View.OnClickListener buttonClick, boolean cancelOnTouchOutside) {
        showDialog(context, context.getString(title), context.getString(msg), context.getString(textButton), textButtonUnderline, buttonClick, cancelOnTouchOutside);
    }

    public static void showDialog(Context context, String title, String msg, String textButton, View.OnClickListener buttonClick, boolean cancelOnTouchOutside) {
        showDialog(context, title, msg, textButton, false, buttonClick, cancelOnTouchOutside, R.style.WeeAppTheme_DefaultDialog);
    }

    public static void showDialog(Context context, String title, String msg, String textButton, final boolean textButtonUnderline, View.OnClickListener buttonClick, boolean cancelOnTouchOutside) {
        showDialog(context, title, msg, textButton, textButtonUnderline, buttonClick, cancelOnTouchOutside, R.style.WeeAppTheme_DefaultDialog);
    }

    public static void showDialog(Context context, int title, int msg, int textButton, final boolean textButtonUnderline, View.OnClickListener buttonClick, boolean cancelOnTouchOutside, int style) {
        showDialog(context, context.getString(title), context.getString(msg), context.getString(textButton), textButtonUnderline, buttonClick, cancelOnTouchOutside, style);
    }

    public static void showDialog(final Context context, final String title, final String msg, final String textButton, final boolean textButtonUnderline, final View.OnClickListener buttonClick, final boolean cancelOnTouchOutside, final int style) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, style);
        View v = LayoutInflater.from(context).inflate(R.layout.wee_dialog, null);
        builder.setView(v);

        TextView txtTitle = (TextView)v.findViewById(R.id.txt_title);
        TextView txtMessage = (TextView)v.findViewById(R.id.txt_message);
        TextView txtButton = (TextView)v.findViewById(R.id.txt_button);

        txtTitle.setText(title);
        txtMessage.setText(msg);
        txtButton.setText(textButton);
        final AlertDialog dialog = builder.create();

        if (textButtonUnderline) {
            txtButton.setPaintFlags(txtButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }

        txtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (buttonClick != null) {
                    buttonClick.onClick(v);
                }
            }
        });

        dialog.setCanceledOnTouchOutside(cancelOnTouchOutside);

        dialog.show();
    }

    public static void showConfirmDialog(final Context context, final int title, final int msg, final int textButtonLeft, final int textButtonRight, final View.OnClickListener buttonClickLeft, final View.OnClickListener buttonClickRight) {
        showConfirmDialog(context, context.getString(title), context.getString(msg), context.getString(textButtonLeft), context.getString(textButtonRight), buttonClickLeft, buttonClickRight);
    }

    public static void showConfirmDialog(final Context context, final String title, final String msg, final String textButtonLeft, final String textButtonRight, final View.OnClickListener buttonClickLeft, final View.OnClickListener buttonClickRight) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.WeeAppTheme_ConfirmDialog);
        View v = LayoutInflater.from(context).inflate(R.layout.wee_confirm_dialog, null);
        builder.setView(v);

        TextView txtTitle = (TextView)v.findViewById(R.id.txt_title);
        TextView txtMessage = (TextView)v.findViewById(R.id.txt_message);
        TextView btLeft = (TextView)v.findViewById(R.id.bt_left);
        TextView btRight = (TextView)v.findViewById(R.id.bt_right);

        txtTitle.setText(title);
        txtMessage.setText(msg);
        btLeft.setText(textButtonLeft);
        btRight.setText(textButtonRight);

        final AlertDialog dialog = builder.create();

        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (buttonClickLeft != null) {
                    buttonClickLeft.onClick(v);
                }
            }
        });

        btRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (buttonClickRight != null) {
                    buttonClickRight.onClick(v);
                }
            }
        });

        dialog.setCanceledOnTouchOutside(false);

        dialog.show();
    }

    public static void showErrorDialog(final Context context, final String title, final String msg, final String textButtonLeft) {
        showErrorDialog(context, title, msg, textButtonLeft, null, null, null);
    }

    public static void showErrorDialog(final Context context, final String title, final String msg, final String textButtonLeft, final String textButtonRight, final View.OnClickListener buttonClickLeft, final View.OnClickListener buttonClickRight) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.WeeAppTheme_ConfirmDialog);
        View v = LayoutInflater.from(context).inflate(R.layout.wee_error_dialog, null);
        builder.setView(v);

        TextView txtTitle = (TextView)v.findViewById(R.id.txt_title);
        TextView txtMessage = (TextView)v.findViewById(R.id.txt_message);
        TextView btLeft = (TextView)v.findViewById(R.id.bt_left);
        TextView btRight = (TextView)v.findViewById(R.id.bt_right);

        txtTitle.setText(title);
        txtMessage.setText(msg);
        btLeft.setText(textButtonLeft);
        if (TextUtils.isEmpty(textButtonRight)) {
            btRight.setVisibility(View.GONE);
            ((LinearLayout.LayoutParams)btLeft.getLayoutParams()).weight = 2;
            btLeft.setBackgroundResource(R.color.bg_error_dialog);
        } else {
            btRight.setText(textButtonRight);
        }

        final AlertDialog dialog = builder.create();

        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (buttonClickLeft != null) {
                    buttonClickLeft.onClick(v);
                }
            }
        });

        btRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (buttonClickRight != null) {
                    buttonClickRight.onClick(v);
                }
            }
        });

        dialog.setCanceledOnTouchOutside(false);

        dialog.show();
    }


    public static void showRatingDialog(final Context context, final MyReservesResult reserve, final RatingListener ratingListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.WeeAppTheme_ConfirmDialog);
        View v = LayoutInflater.from(context).inflate(R.layout.wee_rating_dialog, null);
        builder.setView(v);

        TextView txtTeacher = (TextView)v.findViewById(R.id.txt_teacher);
        TextView btConfirm = (TextView)v.findViewById(R.id.bt_confirm);
        final RatingComponent ratingComponent = (RatingComponent)v.findViewById(R.id.rating_component);

        if (reserve.getTeacher() != null && !TextUtils.isEmpty(reserve.getTeacher().getName())) {
            txtTeacher.setText(reserve.getTeacher().getName());
        }

        final AlertDialog dialog = builder.create();

        btConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ratingComponent.getNota() == null || ratingComponent.getNota().intValue() == 0) {
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                    if (ratingListener != null) {
                        ratingListener.ratingClick(reserve, ratingComponent.getNota());
                    }
                }
            }
        });

        dialog.setCanceledOnTouchOutside(false);

        dialog.show();
    }

    public static void setValue(Context context, Button bt, ImageView img, int resImgOff, int resImgOn, int resDefault, int resDefaultEmpty, LinkedHashSet<String> values) {
        bt.setText(resDefault);
        String text = "";
        if (values != null && values.size() > 0) {
            List<String> sortValues = new ArrayList(values);
            Collections.sort(sortValues);
            for (String value : sortValues) {
                text += value + StringUtil.SEPARATOR;
            }
            text = StringUtil.adjustString(text);
            img.setBackgroundColor(context.getResources().getColor(R.color.filter_img_selected));
            img.setImageResource(resImgOn);
        } else {
            text += context.getString(resDefaultEmpty);
            img.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
            img.setImageResource(resImgOff);
        }

        if (!TextUtils.isEmpty(text) || resDefaultEmpty != R.string.empty) {
            text = " - " + text;
            SpannableString content = new SpannableString(text);
            content.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.filter_text_selected)), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            content.setSpan(new RelativeSizeSpan(0.75f), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            bt.append(content);
        }
    }

    public interface RatingListener {
        void ratingClick(MyReservesResult myReserve, Integer rate);
    }


}
