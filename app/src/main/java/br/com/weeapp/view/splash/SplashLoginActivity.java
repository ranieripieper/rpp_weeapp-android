package br.com.weeapp.view.splash;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.Arrays;

import br.com.weeapp.BuildConfig;
import br.com.weeapp.R;
import br.com.weeapp.model.User;
import br.com.weeapp.model.result.RecoveyPasswordResult;
import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.service.RetrofitManager;
import br.com.weeapp.view.WeeDialogUtil;
import br.com.weeapp.view.base.BaseActivity;
import br.com.weeapp.view.custom.EditText;
import br.com.weeapp.view.home.HomeActivity_;
import br.com.weeapp.view.login.SignUpActivity;
import br.com.weeapp.view.login.TermosUsoActivity;
import me.ranieripieper.androidutils.viewlib.util.ScreenUtil;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@EActivity(R.layout.splash_login_activity)
public class SplashLoginActivity extends BaseActivity {

    private final int TIME_SPLASH_LOGGED = BuildConfig.DEBUG ? 100 : 1500;
    private final int ANIMATION_DURATION = BuildConfig.DEBUG ? 500 : 2500;
    private final float DEFAULT_SCALE_LOGO = 0.9F;

    @ViewById(R.id.img_logo)
    ImageView imgLogo;

    @ViewById(R.id.txt_wee)
    ImageView txtWee;

    @ViewById(R.id.edt_login)
    EditText edtLogin;

    @ViewById(R.id.edt_password)
    EditText edtPassword;

    @ViewById(R.id.bt_ainda_nao_cadastrado)
    Button btCadastrese;

    @ViewById(R.id.bt_entrar)
    Button btEntrar;

    @ViewById(R.id.layout_login)
    RelativeLayout layoutLogin;

    @ViewById(R.id.bt_connectar_facebook)
    LoginButton btFacebook;

    @Extra(EXTRA_SCREEN_NOTIFICATION)
    int screenNotification;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    public void afterViews() {
        //underline
        String cadastreseAqui = getString(R.string.cadastra_se_aqui);
        SpannableString content = new SpannableString(cadastreseAqui);
        content.setSpan(new UnderlineSpan(), 0, cadastreseAqui.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        btCadastrese.append(" ");
        btCadastrese.append(content);

        if (SharedPrefManager.getInstance().isLogged()) {
            layoutLogin.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    showHome();
                }
            }, TIME_SPLASH_LOGGED);
        } else {
            //config facebook
            configFacebook();
            showLogin();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    private void showLogin() {
        layoutLogin.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        imgLogo.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int screenSize = ScreenUtil.getDisplayHeight(this);
        int logoHeight = imgLogo.getMeasuredHeight();
        int layoutLoginHeight = layoutLogin.getMeasuredHeight();
        float scaleLogo = DEFAULT_SCALE_LOGO;

        //actual position logo
        int acutalPositionLogo = screenSize / 2 - logoHeight / 2;
        // tamanho da tela em que o logo deve se encaixar
        int screenSizeLogo = screenSize - layoutLoginHeight;
         //default new position

        if (screenSizeLogo < logoHeight) {
            //calcula scale
            scaleLogo = Float.valueOf(screenSizeLogo) / logoHeight;
        }

        int newPositionLogo = screenSize/4;

        if (screenSize - layoutLoginHeight < screenSize/4) {
            newPositionLogo = screenSize - layoutLoginHeight;
            newPositionLogo = screenSize /2 - newPositionLogo;
        }
        if (newPositionLogo > 0) {
            newPositionLogo *= -1;
        }
        imgLogo.animate().setDuration(ANIMATION_DURATION).translationYBy(newPositionLogo).scaleY(scaleLogo).scaleX(scaleLogo);
        txtWee.animate().setDuration(ANIMATION_DURATION / 2).alpha(0.0f);

        TranslateAnimation layoutLoginAnimation = new TranslateAnimation(0, 0, screenSize, 0);
        layoutLoginAnimation.setDuration(ANIMATION_DURATION);
        layoutLoginAnimation.setFillAfter(true);
        layoutLogin.startAnimation(layoutLoginAnimation);

        if (BuildConfig.DEBUG) {
            edtLogin.setText("ranieripieper@gmail.com");
            edtPassword.setText("12345678");
        }
    }

    @Click(R.id.bt_ainda_nao_cadastrado)
    void signUpClick() {
        SignUpActivity.showActivity(this);
    }

    @Click(R.id.bt_entrar)
    void btEntrarClick() {
        if (isFieldsValid()) {
            showLoading();
            RetrofitManager.getInstance().getUserService().login(edtLogin.getTextStr(), edtPassword.getTextStr(), SharedPrefManager.getInstance().getDeviceToken(), new Callback<User>() {
                @Override
                public void success(User user, Response response) {
                    SharedPrefManager.getInstance().setUser(user);
                    showHome();
                }

                @Override
                public void failure(RetrofitError error) {
                    hideLoading();
                    showLoginError();
                }
            });
        }
    }

    private boolean isFieldsValid() {
        if (edtLogin.isNotValid() | edtPassword.isNotValid()) {
            return false;
        }
        return true;
    }

    private void showLoginError() {
        View.OnClickListener btEsqueciSenhaClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading();
                RetrofitManager.getInstance().getUserService().recoveyPassword(edtLogin.getTextStr(), new Callback<RecoveyPasswordResult>() {
                    @Override
                    public void success(RecoveyPasswordResult recoveyPasswordResult, Response response) {
                        hideLoading();
                        WeeDialogUtil.showDialog(SplashLoginActivity.this, R.string.recuperacao_de_senha, R.string.msg_senha_enviada, R.string.ok, false, null, true);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideLoading();
                        showError(SplashLoginActivity.this, getString(R.string.msg_error_recovey_password), null);
                    }
                });
            }
        };

        WeeDialogUtil.showDialog(this, R.string.erro_ao_entrar, R.string.msg_error_login, R.string.esqueci_minha_senha, true, btEsqueciSenhaClick, true);
    }



    private void showHome() {
        if (SharedPrefManager.getInstance().isTermosAceito()) {
            Intent intent = new Intent(this, HomeActivity_.class);
            intent.putExtra(EXTRA_SCREEN_NOTIFICATION, screenNotification);
            startActivity(intent);
        } else {
            TermosUsoActivity.showActivity(this, true);
        }

        hideLoading();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //Facebook
    private CallbackManager callbackManager;

    private void configFacebook() {
        LoginManager.getInstance().logOut();
        callbackManager = CallbackManager.Factory.create();
        btFacebook.setReadPermissions(Arrays.asList("public_profile, email"));
        btFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading();
            }
        });

        FacebookCallback<LoginResult> fbCallback = new FacebookCallback<com.facebook.login.LoginResult>() {
            @Override
            public void onSuccess(final com.facebook.login.LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject me, GraphResponse response) {
                                if (response.getError() != null) {
                                    facebookError();
                                } else {
                                    final String email = me.optString("email");
                                    // para dar tempo
                                    new Handler().postDelayed(new Runnable() {

                                        @Override
                                        public void run() {
                                            facebookSuccess(email, loginResult.getAccessToken().getToken());
                                        }
                                    }, 300);

                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException exception) {
                facebookError();
            }
        };

        btFacebook.registerCallback(callbackManager, fbCallback);
    }

    private void facebookSuccess(final String email, final String accessToken) {
        if (TextUtils.isEmpty(accessToken)) {
            facebookError();
        } else {
            if (TextUtils.isEmpty(email)) {
                hideLoading();
                showDialogInputEmail(accessToken);
            } else {
                showLoading();
                RetrofitManager.getInstance().getUserService().loginFacebook(accessToken, email, SharedPrefManager.getInstance().getDeviceToken(),
                        new Callback<User>() {
                            @Override
                            public void success(User user, Response response) {
                                SharedPrefManager.getInstance().setUser(user);
                                showHome();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                showError(error, null);
                                facebookError();
                            }
                        });
            }
        }
    }

    private void showDialogInputEmail(final String accessToken) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.WeeAppTheme_ConfirmDialog);
        View v = LayoutInflater.from(this).inflate(R.layout.wee_dialog_input_email, null);
        builder.setView(v);

        final EditText edtEmailDialog = (EditText)v.findViewById(R.id.edt_email);
        TextView btLeft = (TextView)v.findViewById(R.id.bt_left);
        TextView btRight = (TextView)v.findViewById(R.id.bt_right);

        final AlertDialog dialog = builder.create();

        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                facebookError();
            }
        });

        btRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtEmailDialog.hiddenKeyboard();
                dialog.dismiss();
                showLoading();
                if (edtEmailDialog.isValid()) {
                    facebookSuccess(edtEmailDialog.getTextStr(), accessToken);
                }
            }
        });

        dialog.setCanceledOnTouchOutside(false);

        dialog.show();
    }

    private void facebookError() {
        LoginManager.getInstance().logOut();
        hideLoading();
    }

    public static final void showActivity(Context ctx) {
        ctx.startActivity(new Intent(ctx, SplashLoginActivity_.class));
    }
}
