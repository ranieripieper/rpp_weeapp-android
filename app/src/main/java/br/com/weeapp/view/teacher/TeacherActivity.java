package br.com.weeapp.view.teacher;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.app.DialogFragment;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateChangedListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import br.com.weeapp.R;
import br.com.weeapp.enumeration.PeriodEnun;
import br.com.weeapp.model.Language;
import br.com.weeapp.model.Lesson;
import br.com.weeapp.model.Reserve;
import br.com.weeapp.model.Teacher;
import br.com.weeapp.model.User;
import br.com.weeapp.model.dao.LanguageDao;
import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.service.RetrofitManager;
import br.com.weeapp.service.background.CancelReservesService;
import br.com.weeapp.service.exception.ServiceException;
import br.com.weeapp.util.DateUtil;
import br.com.weeapp.view.WeeDialogUtil;
import br.com.weeapp.view.base.BaseActivity;
import br.com.weeapp.view.base.error.ErrorListener;
import br.com.weeapp.view.custom.Button;
import br.com.weeapp.view.custom.CalendarDotSpan;
import br.com.weeapp.view.dialog.LessonsDialogFragment;
import br.com.weeapp.view.dialog.OptionsDialogFragment;
import br.com.weeapp.view.order.OrderLessonsActivity;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 8/6/15.
 */
@EActivity(R.layout.teacher_activity)
public class TeacherActivity extends BaseActivity {

    public static final String EXTRA_TEACHER = "EXTRA_TEACHER";
    public static final String EXTRA_LANGUAGE = "EXTRA_LANGUAGE";
    public static final String EXTRA_LEVEL = "EXTRA_LEVEL";

    private static final String HORARIO_FRAGMENT_TAG = "HORARIO_FRAGMENT_TAG";
    private static final String LESSONS_FRAGMENT_TAG = "LESSONS_FRAGMENT_TAG";

    private LinkedHashSet<String> mValuesPeriod = new LinkedHashSet();

    @ViewById(R.id.scroll_view)
    ScrollView scrollView;

    @ViewById(R.id.bt_horario)
    Button btHorario;

    @ViewById(R.id.img_horario)
    ImageView imgHorario;

    @ViewById(R.id.calendar)
    MaterialCalendarView calendarView;

    @ViewById(R.id.img_photo)
    ImageView imgPhoto;

    @ViewById(R.id.img_skype)
    ImageView imgSkype;

    @ViewById(R.id.img_hangout)
    ImageView imgHangout;

    @ViewById(R.id.txt_name)
    TextViewPlus txtName;

    @ViewById(R.id.txt_idioma)
    TextViewPlus txtIdioma;

    @ViewById(R.id.txt_nivel)
    TextViewPlus txtNivel;

    @ViewById(R.id.txt_avaliacao)
    TextViewPlus txtAvaliacao;

    @Extra(EXTRA_TEACHER)
    Teacher teacher;

    @Extra(EXTRA_LANGUAGE)
    Long languageId;

    @Extra(EXTRA_LEVEL)
    Integer levelId;

    @Bean
    LanguageDao languageDao;

    private Language language;

    private User user;
    private List<Lesson> mLessons = new ArrayList();
    private List<Lesson> mLessonsFilter = new ArrayList();
    private List<Lesson> mLessonsSelected = new ArrayList();
    private HashMap<String, List<Lesson>> mLessonsDay = new HashMap();
    private HashMap<PeriodEnun, List<Lesson>> mLessonsSchedules = new HashMap();
    private ArrayList<Reserve> mReserves = new ArrayList<>();


    private BroadcastReceiver broadcastReceiver;
    public static final String BROADCAST_FINISH_TEACHER_ACTIVITY = "finish_teacher_activity";

    @AfterViews
    void afterViews() {
        user = SharedPrefManager.getInstance().getUser();
        hideLogo();
        enableBackButton();

        configBaseInfo();
        callGetLessons();
    }

    private void callGetLessons() {
        showLoading();
        RetrofitManager.getInstance().getTeacherService().getLessons(teacher.getId(), new Callback<List<Lesson>>() {
            @Override
            public void success(List<Lesson> lessons, Response response) {
                mLessons = lessons;
                mLessonsFilter = lessons;
                for (Lesson lesson : lessons) {
                    List<Lesson> lst = mLessonsDay.get(lesson.getDateToCompare());
                    if (lst == null) {
                        lst = new ArrayList<Lesson>();
                    }
                    lst.add(lesson);
                    mLessonsDay.put(lesson.getDateToCompare(), lst);
                }
                mLessonsSchedules.put(PeriodEnun.MANHA, new ArrayList<Lesson>());
                mLessonsSchedules.put(PeriodEnun.TARDE, new ArrayList<Lesson>());
                mLessonsSchedules.put(PeriodEnun.NOITE, new ArrayList<Lesson>());
                mLessonsSchedules.put(PeriodEnun.MADRUGADA, new ArrayList<Lesson>());

                for (Lesson lesson : lessons) {
                    PeriodEnun period = lesson.getPeriodEnun();
                    List<Lesson> lst = mLessonsSchedules.get(period);
                    if (lst == null) {
                        lst = new ArrayList<Lesson>();
                    }
                    lst.add(lesson);
                    mLessonsSchedules.put(period, lst);
                }
                configCalendar();
                hideLoading();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, new ErrorListener() {
                    @Override
                    public void refresh() {
                        callGetLessons();
                    }

                    @Override
                    public void close() {
                        finish();
                    }
                });
            }
        });
    }

    private void configBaseInfo() {

        txtName.setText(teacher.getName());

        language = teacher.getLanguage(languageId);

        if (language != null) {
            txtNivel.setText(teacher.getLevelsStr(languageId));
            txtIdioma.setText(language.getName());
        }

        txtAvaliacao.setText(getString(R.string.avaliacao, teacher.getRateStr()));
        ImageLoader.getInstance().displayImage(teacher.getAvatar(), imgPhoto);
        if (teacher.isSkypeAvaiable()) {
            imgSkype.setVisibility(View.VISIBLE);
        } else {
            imgSkype.setVisibility(View.GONE);
        }
        if (teacher.isHangoutAvaiable()) {
            imgHangout.setVisibility(View.VISIBLE);
        } else {
            imgHangout.setVisibility(View.GONE);
        }
    }

    @Click(R.id.bt_horario)
    void selectPeriodClick() {

        OptionsDialogFragment.OptionsDialogListener listener = new OptionsDialogFragment.OptionsDialogListener() {
            @Override
            public void onSelectedValues(DialogFragment dialog, LinkedHashSet<String> values) {
                mValuesPeriod = values;
                //faz tratamento para tirar o horário
                LinkedHashSet<String> newValues = new LinkedHashSet();
                mLessonsFilter = new ArrayList<>();
                if (values != null && !values.isEmpty()) {
                    for (String value : values) {
                        if (value.indexOf(getString(R.string.period_manha)) >= 0) {
                            newValues.add(getString(R.string.period_manha));
                            mLessonsFilter.addAll(mLessonsSchedules.get(PeriodEnun.MANHA));
                        } else if (value.indexOf(getString(R.string.period_tarde)) >= 0) {
                            newValues.add(getString(R.string.period_tarde));
                            mLessonsFilter.addAll(mLessonsSchedules.get(PeriodEnun.TARDE));
                        } else if (value.indexOf(getString(R.string.period_noite)) >= 0) {
                            newValues.add(getString(R.string.period_noite));
                            mLessonsFilter.addAll(mLessonsSchedules.get(PeriodEnun.NOITE));
                        } else if (value.indexOf(getString(R.string.period_madrugada)) >= 0) {
                            newValues.add(getString(R.string.period_madrugada));
                            mLessonsFilter.addAll(mLessonsSchedules.get(PeriodEnun.MADRUGADA));
                        }
                    }
                } else {
                    mLessonsFilter = mLessons;
                }

                mValuesPeriod = values;
                WeeDialogUtil.setValue(TeacherActivity.this, btHorario, imgHorario, R.drawable.icn_horario, R.drawable.icn_horario_on, R.string.horario_da_aula, R.string.todos, newValues);
                calendarView.invalidateDecorators();
            }
        };
        OptionsDialogFragment dialog = new OptionsDialogFragment(getString(R.string.horario_da_aula), getResources().getStringArray(R.array.options_horarios), listener, mValuesPeriod);
        dialog.show(getSupportFragmentManager(), HORARIO_FRAGMENT_TAG);
    }

    @Click(R.id.bt_confirmar_agendamento)
    void confirmarAgendamentoClick() {
        if (mLessonsSelected == null || mLessonsSelected.isEmpty()) {
            WeeDialogUtil.showErrorDialog(this, getString(R.string.error), getString(R.string.msg_nenhum_agendamento), getString(R.string.fechar));
        } else {

            broadcastReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context arg0, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals(BROADCAST_FINISH_TEACHER_ACTIVITY)) {
                        unregisterReceiver();
                        finish();
                    }
                }
            };
            registerReceiver(broadcastReceiver, new IntentFilter(BROADCAST_FINISH_TEACHER_ACTIVITY));
            OrderLessonsActivity.showActivity(this, teacher, mReserves);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        unregisterReceiver();
    }

    private void unregisterReceiver() {
        if (broadcastReceiver != null) {
            try {
                unregisterReceiver(broadcastReceiver);
            } catch (IllegalArgumentException e) {
            }
        }
    }

    private void dateSelected(final CalendarDay calendarDay) {
        if (isDateSelected(calendarDay.getCalendar())) {
            WeeDialogUtil.showConfirmDialog(this, R.string.dialog_cancelar, R.string.msg_cancelar_agendamento, R.string.sim, R.string.nao, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Lesson lessonRemove = null;
                    for (Lesson lesson : mLessonsSelected) {
                        if (lesson.getDateToCompare().equals(getDateToCompare(calendarDay.getDate()))) {
                            lessonRemove = lesson;
                            break;
                        }
                    }
                    if (lessonRemove != null) {
                        mLessonsSelected.remove(lessonRemove);
                        calendarView.invalidateDecorators();

                        Reserve reserve = null;
                        for (Reserve r : mReserves) {
                            if (r.getLessonId().equals(lessonRemove.getId())) {
                                reserve = r;
                                break;
                            }
                        }
                        if (reserve != null) {
                            CancelReservesService.callService(TeacherActivity.this, Arrays.asList(reserve));
                        }
                    }

                }
            }, null);
        } else {
            openSelectHour(calendarDay.getCalendar());
        }
    }

    private void openSelectHour(Calendar cal) {
        LessonsDialogFragment.LessonsDialogListener listener = new LessonsDialogFragment.LessonsDialogListener() {
            @Override
            public void onSelectedValues(DialogFragment dialog, Lesson value) {
                reserveLesson(value);
            }
        };
        List<Lesson> dayLessons = new ArrayList<>(mLessonsDay.get(getDateToCompare(cal)));
        //remove as lessons selecionadas
        for (Lesson lessonSelected : mLessonsSelected) {
            if (dayLessons.contains(lessonSelected)) {
                dayLessons.remove(lessonSelected);
            }
        }
        LessonsDialogFragment dialog = LessonsDialogFragment.getLessonsDialogFragment(getString(R.string.horario_da_aula), getString(R.string.lessons_subtitle_dialog), dayLessons, listener);
        dialog.show(getSupportFragmentManager(), LESSONS_FRAGMENT_TAG);
    }

    private void reserveLesson(final Lesson lesson) {
        showLoading(true);
        RetrofitManager.getInstance().getOrderService().reserve(user.getId(), lesson.getId(), levelId, languageId, new Callback<Reserve>() {
            @Override
            public void success(Reserve reserve, Response response) {
                reserve.setLesson(lesson);
                mReserves.add(reserve);
                mLessonsSelected.add(lesson);
                calendarView.invalidateDecorators();
                scrollView.fullScroll(View.FOCUS_DOWN);
                hideLoading();
            }

            @Override
            public void failure(RetrofitError error) {
                hideLoading();
                if (error.getCause() instanceof ServiceException) {
                    ServiceException serviceException = (ServiceException) error.getCause();
                    if (serviceException.getHttpStatus() != null &&
                            serviceException.getHttpStatus().intValue() == HttpURLConnection.HTTP_BAD_REQUEST) {

                        removeLesson(lesson);
                    }
                }

                showError(error, null);

            }
        });
    }

    private void removeLesson(Lesson lesson) {
        mLessonsFilter.remove(lesson);
        List<Lesson> lessons = mLessonsDay.get(lesson.getDateToCompare());
        if (lessons != null) {
            lessons.remove(lesson);
        }
        lessons = mLessonsSchedules.get(lesson.getDateToCompare());
        if (lessons != null) {
            lessons.remove(lesson);
        }
    }

    private boolean isDateSelected(Calendar cal) {
        for (Lesson lesson : mLessonsSelected) {
            if (lesson.getDateToCompare().equals(getDateToCompare(cal))) {
                return true;
            }
        }
        return false;
    }
    
    private String getDateToCompare(Calendar cal) {
        return getDateToCompare(cal.getTime());
    }

    private String getDateToCompare(Date dt) {
        return DateUtil.diaMesAno.get().format(dt);
    }

    private void configCalendar() {
        Calendar maxDate = Calendar.getInstance();
        maxDate.add(Calendar.MONTH, 3);
        calendarView.setMaximumDate(maxDate);
        calendarView.setMinimumDate(Calendar.getInstance());
        calendarView.setTitleMonths(R.array.months);
        calendarView.setOnDateChangedListener(new OnDateChangedListener() {
            @Override
            public void onDateChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {
                if (calendarDay != null && calendarDay.getCalendar() != null) {
                    dateSelected(calendarDay);
                }
                calendarView.clearSelection();
            }
        });

        DayViewDecorator dayViewDecoratorLessonAvailabe = new DayViewDecorator() {
            @Override
            public boolean shouldDecorate(CalendarDay calendarDay) {
                return lessonAvailable(calendarDay.getCalendar()) && !isDateSelected(calendarDay.getCalendar());
            }

            @Override
            public void decorate(DayViewFacade dayViewFacade) {
                dayViewFacade.setBackgroundDrawable(getResources().getDrawable(R.drawable.calendar_default_decorator));
                dayViewFacade.addSpan(new CalendarDotSpan(5f));
                dayViewFacade.setDaysDisabled(false);
            }
        };

        DayViewDecorator dayViewDecoratorDisable = new DayViewDecorator() {
            @Override
            public boolean shouldDecorate(CalendarDay calendarDay) {
                return !lessonAvailable(calendarDay.getCalendar());
            }

            @Override
            public void decorate(DayViewFacade dayViewFacade) {
                dayViewFacade.setBackgroundDrawable(getResources().getDrawable(R.drawable.calendar_default_decorator));
                dayViewFacade.setDaysDisabled(true);
            }
        };
        DayViewDecorator dayViewDecoratorSelected = new DayViewDecorator() {
            @Override
            public boolean shouldDecorate(CalendarDay calendarDay) {
                return isDateSelected(calendarDay.getCalendar());
            }

            @Override
            public void decorate(DayViewFacade dayViewFacade) {
                dayViewFacade.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.calendar_background_selected)));
                dayViewFacade.setDaysDisabled(false);
            }
        };

        calendarView.addDecorator(dayViewDecoratorSelected);
        calendarView.addDecorator(dayViewDecoratorDisable);
        calendarView.addDecorator(dayViewDecoratorLessonAvailabe);
    }

    private boolean lessonAvailable(Calendar cal) {
        for (Lesson lesson : mLessonsFilter) {
            if (lesson.getDateToCompare().equals(getDateToCompare(cal))) {
                return true;
            }
        }
        return false;
    }

    public static void showActivity(Activity activity, Teacher teacher, Long languageId, Integer levelId, RelativeLayout layoutTransition) {
        Intent intent = new Intent(activity, TeacherActivity_.class);
        intent.putExtra(EXTRA_TEACHER, teacher);
        intent.putExtra(EXTRA_LANGUAGE, languageId);
        intent.putExtra(EXTRA_LEVEL, levelId);

        if (layoutTransition != null && Build.VERSION.SDK_INT >= 21) {
            View sharedView = layoutTransition;
            String transitionName = activity.getString(R.string.transition_detail_teacher);

            ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(activity, sharedView, transitionName);
            activity.startActivity(intent, transitionActivityOptions.toBundle());
        } else {
            activity.startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if (!showDialogDesistir()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (android.R.id.home == item.getItemId()) {
            if (!showDialogDesistir()) {
               finish();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean showDialogDesistir() {
        if (mLessonsSelected != null && !mLessonsSelected.isEmpty()) {
            WeeDialogUtil.showConfirmDialog(this, R.string.dialog_desistiu, R.string.msg_desistir_agendamento, R.string.sim, R.string.nao, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CancelReservesService.callService(TeacherActivity.this, mReserves);
                    finish();
                }
            }, null);
            return true;
        } else {
            return false;
        }
    }
}
