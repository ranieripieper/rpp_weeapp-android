package br.com.weeapp.view.custom;

/**
 * Created by ranipieper on 8/5/15.
 */
public interface ViewHolderClick {

    void onItemSelected(int position);
    void onItemUnselected(int position);
}
