package br.com.weeapp.view.base;

import android.support.v4.app.Fragment;
import android.view.View;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import br.com.weeapp.R;
import br.com.weeapp.view.base.error.ErrorListener;
import br.com.weeapp.view.home.HomeActivity;
import retrofit.RetrofitError;

/**
 * Created by ranipieper on 8/5/15.
 */
@EFragment
public class BaseFragment extends Fragment {

    @ViewById(R.id.layout_loading)
    protected View layoutLoading;

    @ViewById(R.id.wee_content)
    protected View weeContent;

    protected HomeActivity getHomeActivity() {
        return (HomeActivity)getActivity();
    }

    protected void showFragment(BaseFragment fragment) {
        getHomeActivity().showFragment(fragment);
    }

    protected void showFragment(BaseFragment fragment, boolean history) {
        getHomeActivity().showFragment(fragment, history);
    }

    protected void showLoading() {
        showLoading(false);
    }

    protected void showLoading(boolean showContent) {
        if (layoutLoading != null) {
            layoutLoading.setVisibility(View.VISIBLE);
        }
        if (!showContent && weeContent != null) {
            weeContent.setVisibility(View.GONE);
        }
    }

    protected void hideLoading(boolean showContent) {
        if (layoutLoading != null) {
            layoutLoading.setVisibility(View.GONE);
        }
        if (showContent && weeContent != null) {
            weeContent.setVisibility(View.VISIBLE);
        }
    }

    protected void hideLoading() {
        hideLoading(true);
    }

    protected void showError(RetrofitError error, ErrorListener listener) {
        showError(error, listener, true);
    }

    protected void showError(RetrofitError error, ErrorListener listener, boolean showContent) {
        hideLoading(showContent);
        getHomeActivity().showError(error, listener);
    }
}
