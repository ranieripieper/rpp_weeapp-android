package br.com.weeapp.view.search;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.freedom.yefeng.yfrecyclerview.YfListAdapter;
import com.freedom.yefeng.yfrecyclerview.YfSimpleViewHolder;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import br.com.weeapp.R;
import br.com.weeapp.model.Language;
import br.com.weeapp.model.Teacher;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;

/**
 * Created by ranipieper on 8/5/15.
 */
public class TeacherAdapter extends YfListAdapter<Teacher> {

    private Activity mActivity;
    private Language mLanguage;

    public TeacherAdapter(Activity activity, List<Teacher> data, Language language) {
        super(new ArrayList<Teacher>(data));
        this.mActivity = activity;
        this.mLanguage = language;
    }

    @Override
    public RecyclerView.ViewHolder onCreateDataViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public RecyclerView.ViewHolder onCreateEmptyViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_row, parent, false);
        return new YfSimpleViewHolder(view);
    }

    @Override
    public RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.include_loading_footer, parent, false);
        return new FooterViewHolder(view);
    }

    @Override
    public void onBindDataViewHolder(RecyclerView.ViewHolder pViewHolder, int position) {
        ViewHolder viewHolder = ((ViewHolder) pViewHolder);
        viewHolder.itemView.setTag(mData.get(position));
        Teacher teacher = mData.get(position);
        viewHolder.txtName.setText(teacher.getName());

        viewHolder.txtIdioma.setText("");
        if (mLanguage != null) {
            viewHolder.txtIdioma.setText(mLanguage.getName());
        }

        viewHolder.txtAvaliacao.setText(mActivity.getString(R.string.avaliacao, teacher.getRateStr()));
        ImageLoader.getInstance().displayImage(teacher.getAvatar(), viewHolder.imgPhoto);
        if (teacher.isSkypeAvaiable()) {
            viewHolder.imgSkype.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imgSkype.setVisibility(View.GONE);
        }
        if (teacher.isHangoutAvaiable()) {
            viewHolder.imgHangout.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imgHangout.setVisibility(View.GONE);
        }

        //nivel
        viewHolder.txtNivel.setText(teacher.getLevelsStr(mLanguage.getMyId()));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextViewPlus txtName;
        public RelativeLayout layoutTransition;
        public ImageView imgSkype;
        public ImageView imgHangout;
        public ImageView imgPhoto;
        public TextViewPlus txtIdioma;
        public TextViewPlus txtNivel;
        public TextViewPlus txtAvaliacao;

        public ViewHolder(View v) {
            super(v);
            txtName = (TextViewPlus) v.findViewById(R.id.txt_name);
            txtIdioma = (TextViewPlus) v.findViewById(R.id.txt_idioma);
            txtNivel = (TextViewPlus) v.findViewById(R.id.txt_nivel);
            txtAvaliacao = (TextViewPlus) v.findViewById(R.id.txt_avaliacao);
            imgSkype = (ImageView) v.findViewById(R.id.img_skype);
            imgHangout = (ImageView) v.findViewById(R.id.img_hangout);
            imgPhoto = (ImageView) v.findViewById(R.id.img_photo);
            layoutTransition = (RelativeLayout) v.findViewById(R.id.layout_teacher_info);
        }
    }

    private static final class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }
}
