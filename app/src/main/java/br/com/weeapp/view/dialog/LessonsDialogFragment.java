package br.com.weeapp.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.solovyev.android.views.llm.DividerItemDecoration;

import java.util.List;

import br.com.weeapp.R;
import br.com.weeapp.model.Lesson;
import br.com.weeapp.view.custom.ViewHolderClick;

/**
 * Created by ranipieper on 8/4/15.
 */
public class LessonsDialogFragment extends DialogFragment {

    private RecyclerView mRecyclerView;
    private LessonsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    LessonsDialogListener mListener;

    public String mTitle;
    public String mSubtitle;
    public List<Lesson> mOptions;

    public LessonsDialogFragment() {
    }


    public static LessonsDialogFragment getLessonsDialogFragment(String title, String subtitle, List<Lesson> options, LessonsDialogListener listener) {
        LessonsDialogFragment lessonsDialogFragment = new LessonsDialogFragment();
        lessonsDialogFragment.mTitle = title;
        lessonsDialogFragment.mSubtitle = subtitle;
        lessonsDialogFragment.mOptions = options;
        lessonsDialogFragment.mListener = listener;
        return lessonsDialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.WeeAppTheme_OptionsDialog);
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.wee_lessons_dialog, null);
        builder.setView(v);

        TextView txtTitle = (TextView) v.findViewById(R.id.txt_title);
        TextView txtSubtitle = (TextView) v.findViewById(R.id.txt_subtitle);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.lst_options);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new org.solovyev.android.views.llm.LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getResources().getDrawable(R.drawable.line), true, false);

        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        ViewHolderClick viewHolderClick = new ViewHolderClick() {
            @Override
            public void onItemSelected(int position) {
                mListener.onSelectedValues(LessonsDialogFragment.this, mOptions.get(position));
                dismiss();
            }

            @Override
            public void onItemUnselected(int position) {
            }
        };

        mAdapter = new LessonsAdapter(getActivity(), this.mOptions, viewHolderClick);
        mRecyclerView.setAdapter(mAdapter);

        txtTitle.setText(mTitle);
        txtSubtitle.setText(mSubtitle);

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(true);

        return dialog;
    }

    public interface LessonsDialogListener {
        public void onSelectedValues(DialogFragment dialog, Lesson lesson);
    }

}
