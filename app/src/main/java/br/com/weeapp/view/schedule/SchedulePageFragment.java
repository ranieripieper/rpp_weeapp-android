package br.com.weeapp.view.schedule;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.freedom.yefeng.yfrecyclerview.YfLoadMoreListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.weeapp.R;
import br.com.weeapp.model.Lesson;
import br.com.weeapp.model.User;
import br.com.weeapp.model.result.MyReservesResult;
import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.service.RetrofitManager;
import br.com.weeapp.util.Constants;
import br.com.weeapp.view.WeeDialogUtil;
import br.com.weeapp.view.base.BaseFragment;
import br.com.weeapp.view.base.error.ErrorListener;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 8/5/15.
 */
@EFragment(R.layout.schedule_page_fragment)
public class SchedulePageFragment extends BaseFragment implements YfLoadMoreListener {

    private boolean mNextLessons = false;
    private boolean mLoadingLock = false;
    private int page = 1;
    private boolean loadMore = true;

    @ViewById(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @ViewById(R.id.txt_msg_empty)
    TextViewPlus txtMsgEmpty;

    private MyReserveAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @AfterViews
    void afterViews() {
        callService();

        if (mNextLessons) {
            txtMsgEmpty.setText(R.string.voce_ainda_nao_tem_aula_agendada);
        } else {
            txtMsgEmpty.setText(R.string.voce_ainda_nao_teve_nenhum_aula);
        }

    }

    private void callService() {
        if (page == 1) {
            mRecyclerView.setVisibility(View.GONE);
            txtMsgEmpty.setVisibility(View.GONE);
            showLoading(false);
        }

        User user = SharedPrefManager.getInstance().getUser();
        String type = mNextLessons ? Constants.RESERVE_TYPE_NEW : Constants.RESERVE_TYPE_OLD;
        RetrofitManager.getInstance().getUserService().myReserves(user.getId(), type, new Callback<List<MyReservesResult>>() {
            @Override
            public void success(List<MyReservesResult> myReservesResults, Response response) {
                populateView(myReservesResults);
                hideLoading();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, new ErrorListener() {
                    @Override
                    public void refresh() {
                        callService();
                    }

                    @Override
                    public void close() {

                    }
                });
            }
        });
    }

    private void populateView(List<MyReservesResult> lstReserves) {
        if (lstReserves == null || lstReserves.isEmpty()) {
            if (page == 1) {
                txtMsgEmpty.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        } else {
            if (page == 1) {
                mRecyclerView.setHasFixedSize(true);
                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);

                txtMsgEmpty.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);

                WeeDialogUtil.RatingListener ratingListener = new WeeDialogUtil.RatingListener() {
                    @Override
                    public void ratingClick(final MyReservesResult myReserve, final Integer rate) {
                        showLoading();
                        String ids = "";
                        for (Lesson lesson : myReserve.getTeacher().getLessons()) {
                            if (lesson.getReserve().getRate() == null || lesson.getReserve().getRate().intValue() == 0) {
                                ids += lesson.getReserve().getId() + ",";
                            }
                            if (!TextUtils.isEmpty(ids)) {
                                ids = ids.substring(0,ids.length()-1);
                            }
                        }
                        RetrofitManager.getInstance().getOrderService().rate(ids, rate, new Callback<Response>() {
                            @Override
                            public void success(Response response, Response response2) {
                                hideLoading();
                                WeeDialogUtil.showDialog(getActivity(), R.string.avaliacao_enviada, R.string.obrigado);
                                for (Lesson lesson : myReserve.getTeacher().getLessons()) {
                                    if (lesson.getReserve().getRate() == null || lesson.getReserve().getRate().intValue() == 0) {
                                        lesson.getReserve().setRate(Float.valueOf(rate));
                                    }
                                }
                                mAdapter.notifyDataSetChanged();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                showError(error, null);
                            }
                        });
                        hideLoading();
                    }
                };

                mAdapter = new MyReserveAdapter(getActivity(), lstReserves, new MyReserveAdapter.MyReserveListener() {
                    @Override
                    public void onClick(MyReservesResult result) {
                        ScheduleDetailActivity.showActivity(getActivity(), result, mNextLessons);
                    }
                }, mNextLessons, ratingListener);
                mRecyclerView.setAdapter(mAdapter);

                StickyHeaderDecoration decor = new StickyHeaderDecoration((StickyHeaderAdapter)mAdapter);
                mRecyclerView.addItemDecoration(decor);

                mRecyclerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            } else {
                //mAdapter.addData(new ArrayList(lstReserves));
            }

        }
        //TODO
        if (lstReserves.size() < 100) {
            loadMore = false;
        } else {
            loadMore = true;
        }

        mLoadingLock = false;
    }

    @Override
    public void loadMore() {
        /*
        if (mLoadingLock) {
            return;
        }

        if (loadMore) {
            // has more
            page++;
            mLoadingLock = true;
            if (!mAdapter.getFooters().contains("loading...")) {
                mAdapter.addFooter("loading...");
            }
            callService();
        } else {
            // no more
            if (mAdapter.getFooters().contains("loading...")) {
                mAdapter.removeFooter("loading...");
            }
        }
        */
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void setNextLessons(boolean nextLessons) {
        this.mNextLessons = nextLessons;
    }

    public static SchedulePageFragment_ newInstance(boolean nextLessons) {
        SchedulePageFragment_ fragment = new SchedulePageFragment_();
        fragment.setNextLessons(nextLessons);
        return fragment;
    }
}
