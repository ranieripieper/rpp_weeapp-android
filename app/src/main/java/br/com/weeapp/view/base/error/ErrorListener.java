package br.com.weeapp.view.base.error;

/**
 * Created by ranipieper on 8/21/15.
 */
public interface ErrorListener {
    void refresh();
    void close();
}
