package br.com.weeapp.view.base.error;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import br.com.weeapp.R;
import br.com.weeapp.view.WeeDialogUtil;
import retrofit.RetrofitError;

/**
 * Created by ranipieper on 8/21/15.
 */
public class ErrorUtil {


    /*
        ErrorUtil.showError(this, "mensagem de erro", new ErrorListener() {
                @Override
                public void refresh() {
                    System.out.print("refresh");
                }

                @Override
                public void close() {
                    System.out.print("refresh");
                }
            });

     */

    public static void showError(Context context, RetrofitError error, ErrorListener listener) {
        if (error != null && !TextUtils.isEmpty(error.getMessage())) {
            showError(context, error.getMessage(), listener);
        } else {
            showError(context, context.getString(R.string.msg_generic_error), listener);
        }

    }

    public static void showError(Context context, String error, final ErrorListener listener) {

        View.OnClickListener leftClick = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.close();
                }
            }
        };

        View.OnClickListener rightClick = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.refresh();
                }
            }
        };

        if (listener != null) {
            WeeDialogUtil.showErrorDialog(context, context.getString(R.string.error), error, context.getString(R.string.fechar), context.getString(R.string.tentar_novamente), leftClick, rightClick);
        } else {
            WeeDialogUtil.showErrorDialog(context, context.getString(R.string.error), error, context.getString(R.string.fechar));
        }

    }
}
