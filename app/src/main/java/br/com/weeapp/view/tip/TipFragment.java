package br.com.weeapp.view.tip;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.freedom.yefeng.yfrecyclerview.YfListAdapter;
import com.freedom.yefeng.yfrecyclerview.YfListRecyclerView;
import com.freedom.yefeng.yfrecyclerview.YfLoadMoreListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Date;

import br.com.weeapp.R;
import br.com.weeapp.model.Tip;
import br.com.weeapp.model.result.TipResult;
import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.service.RetrofitManager;
import br.com.weeapp.util.Constants;
import br.com.weeapp.view.base.BaseFragment;
import br.com.weeapp.view.base.error.ErrorListener;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 8/5/15.
 */
@EFragment(R.layout.tip_fragment)
public class TipFragment extends BaseFragment implements YfLoadMoreListener {

    private boolean mLoadingLock = false;
    private int page = 1;
    private boolean loadMore = true;

    @ViewById(R.id.recycler_view)
    YfListRecyclerView mRecyclerView;

    @ViewById(R.id.txt_msg_empty)
    TextViewPlus txtMsgEmpty;

    private YfListAdapter<Tip> mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @AfterViews
    void afterViews() {
        callService();
    }

    private void populateView(TipResult tipResult) {
        ArrayList<Tip> lstTips = null;
        if (tipResult != null) {
            lstTips = tipResult.getTips();
        }
        if (lstTips == null || lstTips.isEmpty()) {
            if (page == 1) {
                txtMsgEmpty.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        } else {
            if (page == 1) {
                mRecyclerView.setHasFixedSize(true);
                mLayoutManager = new org.solovyev.android.views.llm.LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

                mRecyclerView.setLayoutManager(mLayoutManager);

                mAdapter = new TipAdapter(getHomeActivity(), lstTips);

                mRecyclerView.setAdapter(mAdapter);
                mRecyclerView.setVisibility(View.VISIBLE);
                txtMsgEmpty.setVisibility(View.GONE);

                mRecyclerView.enableAutoLoadMore(this);
            } else {
                mAdapter.addData(lstTips);
            }

        }
        if (lstTips.size() < Constants.PER_PAGE) {
            loadMore = false;
        } else {
            loadMore = true;
        }

        mLoadingLock = false;
    }

    @Override
    public void loadMore() {
        if (mLoadingLock) {
            return;
        }

        if (loadMore) {
            // has more
            page++;
            mLoadingLock = true;
            if (!mAdapter.getFooters().contains("loading...")) {
                mAdapter.addFooter("loading...");
            }
            callService();
        } else {
            // no more
            if (mAdapter.getFooters().contains("loading...")) {
                mAdapter.removeFooter("loading...");
            }
        }
    }

    private void callService() {
        if (page == 1) {
            mRecyclerView.setVisibility(View.GONE);
            txtMsgEmpty.setVisibility(View.GONE);
            showLoading(false);
        }

        RetrofitManager.getInstance().getTipService().getTips(page, Constants.PER_PAGE, new Callback<TipResult>() {
            @Override
            public void success(TipResult tipResult, Response response) {
                hideLoading();
                populateView(tipResult);
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, new ErrorListener() {
                    @Override
                    public void refresh() {
                        callService();
                    }

                    @Override
                    public void close() {
                        getFragmentManager().popBackStackImmediate();
                    }
                });
            }
        });
    }

    @Override
    public void onDestroyView() {
        SharedPrefManager.getInstance().setDateLastTip(new Date());
        super.onDestroyView();
    }
}
