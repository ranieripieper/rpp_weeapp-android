package br.com.weeapp.view.custom;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;

import com.rey.material.app.ThemeManager;
import com.rey.material.drawable.LineMorphingDrawable;
import com.rey.material.drawable.OvalShadowDrawable;
import com.rey.material.drawable.RippleDrawable;
import com.rey.material.util.ThemeUtil;
import com.rey.material.util.ViewUtil;
import com.rey.material.widget.RippleManager;

/**
 * Created by ranipieper on 8/11/15.
 */
public class NotaButton  extends View implements ThemeManager.OnThemeChangedListener {

    private OvalShadowDrawable mBackground;
    private Drawable mIcon;
    private Drawable mPrevIcon;
    private int mAnimDuration = -1;
    private Interpolator mInterpolator;
    private NotaButton.SwitchIconAnimator mSwitchIconAnimator;
    private int mIconSize = -1;
    private RippleManager mRippleManager;
    protected int mStyleId;
    protected int mCurrentStyle = -2147483648;

    public static NotaButton make(Context context, int resId) {
        return new NotaButton(context, (AttributeSet)null, resId);
    }

    public NotaButton(Context context) {
        super(context);
        this.init(context, (AttributeSet)null, 0, 0);
    }

    public NotaButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context, attrs, 0, 0);
    }

    public NotaButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context, attrs, defStyleAttr, 0);
    }

    public NotaButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        this.init(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        this.setClickable(true);
        this.mSwitchIconAnimator = new NotaButton.SwitchIconAnimator();
        this.applyStyle(context, attrs, defStyleAttr, defStyleRes);
        this.mStyleId = ThemeManager.getStyleId(context, attrs, defStyleAttr, defStyleRes);
    }

    public void applyStyle(int resId) {
        ViewUtil.applyStyle(this, resId);
        this.applyStyle(this.getContext(), (AttributeSet)null, 0, resId);
    }

    protected void applyStyle(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray a = context.obtainStyledAttributes(attrs, com.rey.material.R.styleable.FloatingActionButton, defStyleAttr, defStyleRes);
        int radius = -1;
        int elevation = -1;
        ColorStateList bgColor = null;
        int bgAnimDuration = -1;
        int iconSrc = 0;
        int iconLineMorphing = 0;
        int background = 0;

        for(int drawable = a.getIndexCount(); background < drawable; ++background) {
            int attr = a.getIndex(background);
            if(attr == com.rey.material.R.styleable.FloatingActionButton_fab_radius) {
                radius = a.getDimensionPixelSize(attr, 0);
            } else if(attr == com.rey.material.R.styleable.FloatingActionButton_fab_elevation) {
                elevation = a.getDimensionPixelSize(attr, 0);
            } else if(attr == com.rey.material.R.styleable.FloatingActionButton_fab_backgroundColor) {
                bgColor = a.getColorStateList(attr);
            } else if(attr == com.rey.material.R.styleable.FloatingActionButton_fab_backgroundAnimDuration) {
                bgAnimDuration = a.getInteger(attr, 0);
            } else if(attr == com.rey.material.R.styleable.FloatingActionButton_fab_iconSrc) {
                iconSrc = a.getResourceId(attr, 0);
            } else if(attr == com.rey.material.R.styleable.FloatingActionButton_fab_iconLineMorphing) {
                iconLineMorphing = a.getResourceId(attr, 0);
            } else if(attr == com.rey.material.R.styleable.FloatingActionButton_fab_iconSize) {
                this.mIconSize = a.getDimensionPixelSize(attr, 0);
            } else if(attr == com.rey.material.R.styleable.FloatingActionButton_fab_animDuration) {
                this.mAnimDuration = a.getInteger(attr, 0);
            } else if(attr == com.rey.material.R.styleable.FloatingActionButton_fab_interpolator) {
                int resId = a.getResourceId(com.rey.material.R.styleable.FloatingActionButton_fab_interpolator, 0);
                if(resId != 0) {
                    this.mInterpolator = AnimationUtils.loadInterpolator(context, resId);
                }
            }
        }

        a.recycle();
        if(this.mIconSize < 0) {
            this.mIconSize = ThemeUtil.dpToPx(context, 24);
        }

        if(this.mAnimDuration < 0) {
            this.mAnimDuration = context.getResources().getInteger(17694721);
        }

        if(this.mInterpolator == null) {
            this.mInterpolator = new DecelerateInterpolator();
        }

        if(this.mBackground == null) {
            if(radius < 0) {
                radius = ThemeUtil.dpToPx(context, 28);
            }

            if(elevation < 0) {
                elevation = ThemeUtil.dpToPx(context, 4);
            }

            if(bgColor == null) {
                bgColor = ColorStateList.valueOf(ThemeUtil.colorAccent(context, 0));
            }

            if(bgAnimDuration < 0) {
                bgAnimDuration = 0;
            }

            this.mBackground = new OvalShadowDrawable(radius, bgColor, (float)elevation, (float)elevation, bgAnimDuration);
            this.mBackground.setInEditMode(this.isInEditMode());
            this.mBackground.setBounds(0, 0, this.getWidth(), this.getHeight());
            this.mBackground.setCallback(this);
        } else {
            if(radius >= 0) {
                this.mBackground.setRadius(radius);
            }

            if(bgColor != null) {
                this.mBackground.setColor(bgColor);
            }

            if(elevation >= 0) {
                this.mBackground.setShadow((float)elevation, (float)elevation);
            }

            if(bgAnimDuration >= 0) {
                this.mBackground.setAnimationDuration(bgAnimDuration);
            }
        }

        if(iconLineMorphing != 0) {
            this.setIcon((new LineMorphingDrawable.Builder(context, iconLineMorphing)).build(), false);
        } else if(iconSrc != 0) {
            this.setIcon(context.getResources().getDrawable(iconSrc), false);
        }

        this.getRippleManager().onCreate(this, context, attrs, defStyleAttr, defStyleRes);
        Drawable var16 = this.getBackground();
        if(var16 != null && var16 instanceof RippleDrawable) {
            RippleDrawable var17 = (RippleDrawable)var16;
            var17.setBackgroundDrawable((Drawable)null);
            var17.setMask(1, 0, 0, 0, 0, (int)this.mBackground.getPaddingLeft(), (int)this.mBackground.getPaddingTop(), (int)this.mBackground.getPaddingRight(), (int)this.mBackground.getPaddingBottom());
        }

    }

    public void onThemeChanged(ThemeManager.OnThemeChangedEvent event) {
        int style = ThemeManager.getInstance().getCurrentStyle(this.mStyleId);
        if(this.mCurrentStyle != style) {
            this.mCurrentStyle = style;
            this.applyStyle(this.mCurrentStyle);
        }

    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if(this.mStyleId != 0) {
            ThemeManager.getInstance().registerOnThemeChangedListener(this);
            this.onThemeChanged((ThemeManager.OnThemeChangedEvent)null);
        }

    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        RippleManager var10000 = this.mRippleManager;
        RippleManager.cancelRipple(this);
        if(this.mStyleId != 0) {
            ThemeManager.getInstance().unregisterOnThemeChangedListener(this);
        }

    }

    public int getRadius() {
        return this.mBackground.getRadius();
    }

    public void setRadius(int radius) {
        if(this.mBackground.setRadius(radius)) {
            this.requestLayout();
        }

    }

    @TargetApi(21)
    public float getElevation() {
        return Build.VERSION.SDK_INT >= 21?super.getElevation():this.mBackground.getShadowSize();
    }

    @TargetApi(21)
    public void setElevation(float elevation) {
        if(Build.VERSION.SDK_INT >= 21) {
            super.setElevation(elevation);
        } else if(this.mBackground.setShadow(elevation, elevation)) {
            this.requestLayout();
        }

    }

    public int getLineMorphingState() {
        return this.mIcon != null && this.mIcon instanceof LineMorphingDrawable?((LineMorphingDrawable)this.mIcon).getLineState():-1;
    }

    public void setLineMorphingState(int state, boolean animation) {
        if(this.mIcon != null && this.mIcon instanceof LineMorphingDrawable) {
            ((LineMorphingDrawable)this.mIcon).switchLineState(state, animation);
        }

    }

    public ColorStateList getBackgroundColor() {
        return this.mBackground.getColor();
    }

    public Drawable getIcon() {
        return this.mIcon;
    }

    public void setIcon(Drawable icon, boolean animation) {
        if(icon != null) {
            if(animation) {
                this.mSwitchIconAnimator.startAnimation(icon);
                this.invalidate();
            } else {
                if(this.mIcon != null) {
                    this.mIcon.setCallback((Drawable.Callback)null);
                    this.unscheduleDrawable(this.mIcon);
                }

                this.mIcon = icon;
                float half = (float)this.mIconSize / 2.0F;
                this.mIcon.setBounds((int)(this.mBackground.getCenterX() - half), (int)(this.mBackground.getCenterY() - half), (int)(this.mBackground.getCenterX() + half), (int)(this.mBackground.getCenterY() + half));
                this.mIcon.setCallback(this);
                this.invalidate();
            }

        }
    }

    public void setBackgroundColor(ColorStateList color) {
        this.mBackground.setColor(color);
        this.invalidate();
    }

    public void setBackgroundColor(int color) {
        this.mBackground.setColor(color);
        this.invalidate();
    }

    public void show(Activity activity, int x, int y, int gravity) {
        if(this.getParent() == null) {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(this.mBackground.getIntrinsicWidth(), this.mBackground.getIntrinsicHeight());
            this.updateParams(x, y, gravity, params);
            activity.getWindow().addContentView(this, params);
        } else {
            this.updateLocation(x, y, gravity);
        }

    }

    public void show(ViewGroup parent, int x, int y, int gravity) {
        if(this.getParent() == null) {
            android.view.ViewGroup.LayoutParams params = parent.generateLayoutParams((AttributeSet)null);
            params.width = this.mBackground.getIntrinsicWidth();
            params.height = this.mBackground.getIntrinsicHeight();
            this.updateParams(x, y, gravity, params);
            parent.addView(this, params);
        } else {
            this.updateLocation(x, y, gravity);
        }

    }

    public void updateLocation(int x, int y, int gravity) {
        if(this.getParent() != null) {
            this.updateParams(x, y, gravity, this.getLayoutParams());
        } else {
            Log.v(NotaButton.class.getSimpleName(), "updateLocation() is called without parent");
        }

    }

    private void updateParams(int x, int y, int gravity, android.view.ViewGroup.LayoutParams params) {
        int horizontalGravity = gravity & 7;
        switch(horizontalGravity) {
            case 1:
                this.setLeftMargin(params, (int)((float)x - this.mBackground.getCenterX()));
                break;
            case 2:
            case 4:
            default:
                this.setLeftMargin(params, (int)((float)x - this.mBackground.getPaddingLeft()));
                break;
            case 3:
                this.setLeftMargin(params, (int)((float)x - this.mBackground.getPaddingLeft()));
                break;
            case 5:
                this.setLeftMargin(params, (int)((float)x - this.mBackground.getPaddingLeft() - (float)(this.mBackground.getRadius() * 2)));
        }

        int verticalGravity = gravity & 112;
        switch(verticalGravity) {
            case 16:
                this.setTopMargin(params, (int)((float)y - this.mBackground.getCenterY()));
                break;
            case 48:
                this.setTopMargin(params, (int)((float)y - this.mBackground.getPaddingTop()));
                break;
            case 80:
                this.setTopMargin(params, (int)((float)y - this.mBackground.getPaddingTop() - (float)(this.mBackground.getRadius() * 2)));
                break;
            default:
                this.setTopMargin(params, (int)((float)y - this.mBackground.getPaddingTop()));
        }

        this.setLayoutParams(params);
    }

    private void setLeftMargin(android.view.ViewGroup.LayoutParams params, int value) {
        if(params instanceof FrameLayout.LayoutParams) {
            ((FrameLayout.LayoutParams)params).leftMargin = value;
        } else if(params instanceof android.widget.RelativeLayout.LayoutParams) {
            ((android.widget.RelativeLayout.LayoutParams)params).leftMargin = value;
        } else {
            Log.v(NotaButton.class.getSimpleName(), "cannot recognize LayoutParams: " + params);
        }

    }

    private void setTopMargin(android.view.ViewGroup.LayoutParams params, int value) {
        if(params instanceof FrameLayout.LayoutParams) {
            ((FrameLayout.LayoutParams)params).topMargin = value;
        } else if(params instanceof android.widget.RelativeLayout.LayoutParams) {
            ((android.widget.RelativeLayout.LayoutParams)params).topMargin = value;
        } else {
            Log.v(NotaButton.class.getSimpleName(), "cannot recognize LayoutParams: " + params);
        }

    }

    public void dismiss() {
        if(this.getParent() != null) {
            ((ViewGroup)this.getParent()).removeView(this);
        }

    }

    protected boolean verifyDrawable(Drawable who) {
        return super.verifyDrawable(who) || this.mBackground == who || this.mIcon == who || this.mPrevIcon == who;
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if(this.mBackground != null) {
            this.mBackground.setState(this.getDrawableState());
        }

        if(this.mIcon != null) {
            this.mIcon.setState(this.getDrawableState());
        }

        if(this.mPrevIcon != null) {
            this.mPrevIcon.setState(this.getDrawableState());
        }

    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        this.setMeasuredDimension(this.mBackground.getIntrinsicWidth(), this.mBackground.getIntrinsicHeight());
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.mBackground.setBounds(0, 0, w, h);
        float half;
        if(this.mIcon != null) {
            half = (float)this.mIconSize / 2.0F;
            this.mIcon.setBounds((int)(this.mBackground.getCenterX() - half), (int)(this.mBackground.getCenterY() - half), (int)(this.mBackground.getCenterX() + half), (int)(this.mBackground.getCenterY() + half));
        }

        if(this.mPrevIcon != null) {
            half = (float)this.mIconSize / 2.0F;
            this.mPrevIcon.setBounds((int)(this.mBackground.getCenterX() - half), (int)(this.mBackground.getCenterY() - half), (int)(this.mBackground.getCenterX() + half), (int)(this.mBackground.getCenterY() + half));
        }

    }

    public void draw(@NonNull Canvas canvas) {
        this.mBackground.draw(canvas);
        super.draw(canvas);
        if(this.mPrevIcon != null) {
            this.mPrevIcon.draw(canvas);
        }

        if(this.mIcon != null) {
            this.mIcon.draw(canvas);
        }

    }

    protected RippleManager getRippleManager() {
        if(this.mRippleManager == null) {
            Class var1 = RippleManager.class;
            synchronized(RippleManager.class) {
                if(this.mRippleManager == null) {
                    this.mRippleManager = new RippleManager();
                }
            }
        }

        return this.mRippleManager;
    }

    public void setOnClickListener(OnClickListener l) {
        RippleManager rippleManager = this.getRippleManager();
        if(l == rippleManager) {
            super.setOnClickListener(l);
        } else {
            rippleManager.setOnClickListener(l);
            this.setOnClickListener(rippleManager);
        }

    }

    public boolean onTouchEvent(@NonNull MotionEvent event) {
        int action = event.getActionMasked();
        if(action == 0 && !this.mBackground.isPointerOver(event.getX(), event.getY())) {
            return false;
        } else {
            boolean result = super.onTouchEvent(event);
            return this.getRippleManager().onTouchEvent(event) || result;
        }
    }

    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        NotaButton.SavedState ss = new NotaButton.SavedState(superState);
        ss.state = this.getLineMorphingState();
        return ss;
    }

    protected void onRestoreInstanceState(Parcelable state) {
        NotaButton.SavedState ss = (NotaButton.SavedState)state;
        super.onRestoreInstanceState(ss.getSuperState());
        if(ss.state >= 0) {
            this.setLineMorphingState(ss.state, false);
        }

        this.requestLayout();
    }

    class SwitchIconAnimator implements Runnable {
        boolean mRunning = false;
        long mStartTime;

        SwitchIconAnimator() {
        }

        public void resetAnimation() {
            this.mStartTime = SystemClock.uptimeMillis();
            NotaButton.this.mIcon.setAlpha(0);
            NotaButton.this.mPrevIcon.setAlpha(255);
        }

        public boolean startAnimation(Drawable icon) {
            if(NotaButton.this.mIcon == icon) {
                return false;
            } else {
                NotaButton.this.mPrevIcon = NotaButton.this.mIcon;
                NotaButton.this.mIcon = icon;
                float half = (float)NotaButton.this.mIconSize / 2.0F;
                NotaButton.this.mIcon.setBounds((int)(NotaButton.this.mBackground.getCenterX() - half), (int)(NotaButton.this.mBackground.getCenterY() - half), (int)(NotaButton.this.mBackground.getCenterX() + half), (int)(NotaButton.this.mBackground.getCenterY() + half));
                NotaButton.this.mIcon.setCallback(NotaButton.this);
                if(NotaButton.this.getHandler() != null) {
                    this.resetAnimation();
                    this.mRunning = true;
                    NotaButton.this.getHandler().postAtTime(this, SystemClock.uptimeMillis() + 16L);
                } else {
                    NotaButton.this.mPrevIcon.setCallback((Drawable.Callback)null);
                    NotaButton.this.unscheduleDrawable(NotaButton.this.mPrevIcon);
                    NotaButton.this.mPrevIcon = null;
                }

                NotaButton.this.invalidate();
                return true;
            }
        }

        public void stopAnimation() {
            this.mRunning = false;
            NotaButton.this.mPrevIcon.setCallback((Drawable.Callback)null);
            NotaButton.this.unscheduleDrawable(NotaButton.this.mPrevIcon);
            NotaButton.this.mPrevIcon = null;
            NotaButton.this.mIcon.setAlpha(255);
            if(NotaButton.this.getHandler() != null) {
                NotaButton.this.getHandler().removeCallbacks(this);
            }

            NotaButton.this.invalidate();
        }

        public void run() {
            long curTime = SystemClock.uptimeMillis();
            float progress = Math.min(1.0F, (float)(curTime - this.mStartTime) / (float)NotaButton.this.mAnimDuration);
            float value = NotaButton.this.mInterpolator.getInterpolation(progress);
            NotaButton.this.mIcon.setAlpha(Math.round(255.0F * value));
            NotaButton.this.mPrevIcon.setAlpha(Math.round(255.0F * (1.0F - value)));
            if(progress == 1.0F) {
                this.stopAnimation();
            }

            if(this.mRunning) {
                if(NotaButton.this.getHandler() != null) {
                    NotaButton.this.getHandler().postAtTime(this, SystemClock.uptimeMillis() + 16L);
                } else {
                    this.stopAnimation();
                }
            }

            NotaButton.this.invalidate();
        }
    }

    static class SavedState extends BaseSavedState {
        int state;
        public static final Creator<NotaButton.SavedState> CREATOR = new Creator() {
            public NotaButton.SavedState createFromParcel(Parcel in) {
                return new NotaButton.SavedState(in);
            }

            public NotaButton.SavedState[] newArray(int size) {
                return new NotaButton.SavedState[size];
            }
        };

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            this.state = in.readInt();
        }

        public void writeToParcel(@NonNull Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(this.state);
        }

        public String toString() {
            return "NotaButton.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " state=" + this.state + "}";
        }
    }

    public int getmIconSize() {
        return mIconSize;
    }

    public void setmIconSize(int mIconSize) {
        this.mIconSize = mIconSize;
        setIcon(getIcon(), false);
    }
}
