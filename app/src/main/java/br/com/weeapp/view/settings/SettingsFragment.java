package br.com.weeapp.view.settings;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import br.com.weeapp.R;
import br.com.weeapp.model.User;
import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.service.RetrofitManager;
import br.com.weeapp.service.background.LogoutBackgroundService;
import br.com.weeapp.view.WeeDialogUtil;
import br.com.weeapp.view.base.BaseFragment;
import br.com.weeapp.view.custom.Button;
import br.com.weeapp.view.login.SignUpActivity;
import br.com.weeapp.view.login.TermosUsoActivity;
import br.com.weeapp.view.splash.SplashLoginActivity_;
import de.hdodenhof.circleimageview.CircleImageView;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 8/5/15.
 */
@EFragment(R.layout.settings_fragment)
public class SettingsFragment extends BaseFragment {

    @ViewById(R.id.img_profile_image)
    CircleImageView imgPhoto;

    @ViewById(R.id.txt_name)
    TextViewPlus txtName;

    @ViewById(R.id.bt_editar_perfil)
    Button btEditarPerfil;

    @AfterViews
    void afterViews() {
        User user = SharedPrefManager.getInstance().getUser();
        txtName.setText(user.getName());
        ImageLoader.getInstance().displayImage(user.getAvatar(), imgPhoto);

        if (!TextUtils.isEmpty(user.getFacebookUid())) {
            btEditarPerfil.setVisibility(View.GONE);
        }
    }

    @Click(R.id.bt_editar_perfil)
    void btEditarPerfilClick() {
        SignUpActivity.showActivity(getActivity(), true);
    }

    @Click(R.id.bt_termos_de_uso)
    void btTermosDeUsoClick() {
        TermosUsoActivity.showActivity(getActivity(), false);
    }

    @Click(R.id.bt_sobre_wee)
    void btSobreWeeClick() {
        SobreActivity.showActivity(getActivity());
    }

    @Click(R.id.bt_sair)
    void btSairClick() {
        final User user = SharedPrefManager.getInstance().getUser();

        WeeDialogUtil.showConfirmDialog(getActivity(), R.string.atencao, R.string.msg_logout, R.string.nao, R.string.sim, null, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogoutBackgroundService.callService(getActivity(), user.getAuthToken(), String.valueOf(user.getId()));
                SharedPrefManager.getInstance().logout();
                startActivity(new Intent(getActivity(), SplashLoginActivity_.class));
                getActivity().finishAffinity();
            }
        });
    }

    @Click(R.id.bt_deletar_minha_conta)
    void btDeletarContaClick() {
        WeeDialogUtil.showConfirmDialog(getActivity(), R.string.atencao, R.string.msg_remove_account, R.string.nao, R.string.sim, null, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callRemoveAccount();
            }
        });
    }

    private void callRemoveAccount() {
        showLoading();
        final User user = SharedPrefManager.getInstance().getUser();
        RetrofitManager.getInstance().getUserService().remove(user.getId(), new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                SharedPrefManager.getInstance().logout();
                startActivity(new Intent(getActivity(), SplashLoginActivity_.class));
                getActivity().finishAffinity();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, null);
            }
        });
    }
}
