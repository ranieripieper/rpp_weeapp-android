package br.com.weeapp.view.tip;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.freedom.yefeng.yfrecyclerview.YfListAdapter;
import com.freedom.yefeng.yfrecyclerview.YfSimpleViewHolder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.weeapp.R;
import br.com.weeapp.model.Tip;
import br.com.weeapp.preferences.SharedPrefManager;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;

/**
 * Created by ranipieper on 8/5/15.
 */
public class TipAdapter extends YfListAdapter<Tip> {

    private Activity mActivity;
    private Date mLastTipDate;

    public TipAdapter(Activity activity, List<Tip> data) {
        super(new ArrayList(data));
        this.mActivity = activity;
        mLastTipDate = SharedPrefManager.getInstance().getDateLastTip();
    }

    @Override
    public RecyclerView.ViewHolder onCreateDataViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tip_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public RecyclerView.ViewHolder onCreateEmptyViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tip_row, parent, false);
        return new YfSimpleViewHolder(view);
    }

    @Override
    public RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.include_loading_footer, parent, false);
        return new FooterViewHolder(view);
    }

    @Override
    public void onBindDataViewHolder(RecyclerView.ViewHolder pViewHolder, int position) {
        ViewHolder viewHolder = ((ViewHolder) pViewHolder);
        Tip tip = mData.get(position);
        viewHolder.txtTip.setText(tip.getDescription());
        if (position % 2 == 0) {
            viewHolder.layoutRow.setBackgroundResource(R.color.row_sep_1);
        } else {
            viewHolder.layoutRow.setBackgroundResource(R.color.row_sep_2);
        }
        if (mLastTipDate != null && mLastTipDate.after(tip.getCreatedAt())) {
            viewHolder.imgNewTip.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.imgNewTip.setVisibility(View.VISIBLE);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RelativeLayout layoutRow;
        public TextViewPlus txtTip;
        public ImageView imgNewTip;

        public ViewHolder(View v) {
            super(v);
            txtTip = (TextViewPlus) v.findViewById(R.id.txt_tip);
            imgNewTip = (ImageView) v.findViewById(R.id.img_new_tip);
            layoutRow = (RelativeLayout) v.findViewById(R.id.layout_row);
        }
    }

    private static final class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }
}
