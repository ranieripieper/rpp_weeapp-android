package br.com.weeapp.view.base;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.net.HttpURLConnection;

import br.com.weeapp.R;
import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.service.exception.ServiceException;
import br.com.weeapp.view.base.error.ErrorListener;
import br.com.weeapp.view.base.error.ErrorUtil;
import br.com.weeapp.view.splash.SplashLoginActivity;
import retrofit.RetrofitError;

/**
 * Created by ranipieper on 7/30/15.
 */
@EActivity
public abstract class BaseActivity extends AppCompatActivity {

    public static final String EXTRA_SCREEN_NOTIFICATION = "EXTRA_SCREEN_NOTIFICATION";

    @ViewById(R.id.wee_toolbar)
    protected Toolbar toolbar;

    @ViewById(R.id.layout_loading)
    protected View layoutLoading;

    @ViewById(R.id.wee_content)
    protected View weeContent;

    @AfterViews
    protected void baseAfterViews() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    protected void hideLogo() {
        if (toolbar != null) {
            toolbar.findViewById(R.id.img_logo).setVisibility(View.GONE);
        }
    }

    protected void enableBackButton() {
        if (toolbar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            toolbar.setNavigationIcon(R.drawable.icn_back);
        }
    }

    protected void showLoading() {
        showLoading(false);
    }

    protected void showLoading(boolean showContent) {
        if (layoutLoading != null) {
            layoutLoading.setVisibility(View.VISIBLE);
        }
        if (!showContent && weeContent != null) {
            weeContent.setVisibility(View.GONE);
        }
    }

    protected void hideLoading(boolean showContent) {
        if (layoutLoading != null) {
            layoutLoading.setVisibility(View.GONE);
        }
        if (showContent && weeContent != null) {
            weeContent.setVisibility(View.VISIBLE);
        }
    }

    protected void hideLoading() {
        hideLoading(true);
    }


    public static void showError(Context ctx, String error, ErrorListener listener) {
        ErrorUtil.showError(ctx, error, listener);
    }

    public static void showError(Context ctx, RetrofitError error, ErrorListener listener) {
        ErrorUtil.showError(ctx, error, listener);
    }

    protected void showError(RetrofitError error, ErrorListener listener) {
        hideLoading();
        boolean showUnauthorizedError = false;
        if (error.getCause() instanceof ServiceException) {
            ServiceException serviceException = (ServiceException)error.getCause();
            if (serviceException.getHttpStatus() != null &&
                    serviceException.getHttpStatus().intValue() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                showUnauthorizedError = true;
                SharedPrefManager.getInstance().logout();
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    finishAffinity();
                } else {
                    finish();
                }

                SplashLoginActivity.showActivity(this);
            }
        }

        if (!showUnauthorizedError) {
            ErrorUtil.showError(this, error, listener);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (android.R.id.home == item.getItemId()) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    @Override
    public void setTitle(CharSequence title) {
        if (txtTitle != null) {
            txtTitle.setText(title);
        } else {
            super.setTitle(title);
        }
    }
    */
}
