package br.com.weeapp.view.order;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.freedom.yefeng.yfrecyclerview.YfListAdapter;
import com.freedom.yefeng.yfrecyclerview.YfSimpleViewHolder;

import java.util.ArrayList;

import br.com.weeapp.R;
import br.com.weeapp.model.Reserve;
import br.com.weeapp.util.DateUtil;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;

/**
 * Created by ranipieper on 8/5/15.
 */
public class ReserveAdapter extends YfListAdapter<Reserve> {

    private Activity mActivity;

    public ReserveAdapter(Activity activity, ArrayList<Reserve> data) {
        super(data);
        this.mActivity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateDataViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_lesson_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public RecyclerView.ViewHolder onCreateEmptyViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_lesson_row, parent, false);
        return new YfSimpleViewHolder(view);
    }

    @Override
    public RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.include_loading_footer, parent, false);
        return new FooterViewHolder(view);
    }

    @Override
    public void onBindDataViewHolder(RecyclerView.ViewHolder pViewHolder, int position) {
        ViewHolder viewHolder = ((ViewHolder) pViewHolder);
        viewHolder.itemView.setTag(mData.get(position));
        Reserve reserve = mData.get(position);
        viewHolder.txtDataLesson.setText(DateUtil.diaMesAno.get().format(reserve.getDate()));
        viewHolder.txtHourLesson.setText(DateUtil.horaMin.get().format(reserve.getDate()));
        viewHolder.txtLanguageLevel.setText(reserve.getLanguage().getName() + " " + reserve.getLevel().getName());

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextViewPlus txtDataLesson;
        public TextViewPlus txtHourLesson;
        public TextViewPlus txtLanguageLevel;
        public TextViewPlus txtDuration;

        public ViewHolder(View v) {
            super(v);
            txtDataLesson = (TextViewPlus) v.findViewById(R.id.txt_data_lesson);
            txtHourLesson = (TextViewPlus) v.findViewById(R.id.txt_hour_lesson);
            txtLanguageLevel = (TextViewPlus) v.findViewById(R.id.txt_language_level);
            txtDuration = (TextViewPlus) v.findViewById(R.id.txt_duration);
        }
    }

    private static final class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }
}
