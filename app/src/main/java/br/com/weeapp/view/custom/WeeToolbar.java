package br.com.weeapp.view.custom;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.widget.TextView;

import br.com.weeapp.R;

/**
 * Created by ranipieper on 7/30/15.
 */
public class WeeToolbar extends Toolbar {

    private TextView txtTitle;

    public WeeToolbar(Context context) {
        super(context);
    }

    public WeeToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WeeToolbar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setTitle(CharSequence title) {
        if (txtTitle == null) {
            txtTitle = (TextView)findViewById(R.id.txt_toolbar_title);
        }
        if (txtTitle != null) {
            txtTitle.setText(title);
            super.setTitle(null);
        } else {
            super.setTitle(title);
        }
    }

    @Override
    public void setTitle(int resId) {
        this.setTitle(getResources().getString(resId));
    }

}
