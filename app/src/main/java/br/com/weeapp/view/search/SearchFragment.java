package br.com.weeapp.view.search;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import br.com.weeapp.R;
import br.com.weeapp.enumeration.LevelEnun;
import br.com.weeapp.model.Language;
import br.com.weeapp.model.dao.LanguageDao;
import br.com.weeapp.service.RetrofitManager;
import br.com.weeapp.view.WeeDialogUtil;
import br.com.weeapp.view.base.BaseFragment;
import br.com.weeapp.view.base.error.ErrorListener;
import br.com.weeapp.view.custom.Button;
import br.com.weeapp.view.dialog.OptionsDialogFragment;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 8/5/15.
 */
@EFragment(R.layout.search_fragment)
public class SearchFragment extends BaseFragment implements OptionsDialogFragment.OptionsDialogListener {

    private static int QTDE_LANGUAGE_ANIMATION_DURATION = 1000;

    @ViewById(R.id.bt_idioma)
    Button btIdioma;

    @ViewById(R.id.bt_nivel_aula)
    Button btNivelAula;

    @ViewById(R.id.bt_aula_via)
    Button btAulaVia;

    @ViewById(R.id.bt_horario)
    Button btHorario;

    @ViewById(R.id.img_idioma)
    ImageView imgIdioma;

    @ViewById(R.id.img_nivel_aula)
    ImageView imgNivelAula;

    @ViewById(R.id.img_aula_via)
    ImageView imgAulaVia;

    @ViewById(R.id.img_horario)
    ImageView imgHorario;

    @ViewById(R.id.txt_nr_languages)
    TextViewPlus txtNrLanguage;

    @ViewById(R.id.txt_msg_nr_languages)
    TextViewPlus txtMsgNrLanguages;

    @Bean
    LanguageDao languageDao;

    private LinkedHashSet<String> mValuesLanguage = new LinkedHashSet();
    private LinkedHashSet<String> mValuesLevel = new LinkedHashSet();
    private LinkedHashSet<String> mValuesPlataform = new LinkedHashSet();
    private LinkedHashSet<String> mValuesPeriod = new LinkedHashSet();

    private static String IDIOMA_FRAGMENT_TAG = "OptionsDialogFragment_IDIOMA_TAG";
    private static String AULA_VIA_FRAGMENT_TAG = "OptionsDialogFragment_AULA_VIA_TAG";
    private static String NIVEL_AULA_FRAGMENT_TAG = "OptionsDialogFragment_NIVEL_AULA_TAG";
    private static String HORARIO_FRAGMENT_TAG = "OptionsDialogFragment_HORARIO_TAG";


    @AfterViews
    void afterViews() {
        int qtdeLanguage = languageDao.count();
        if (qtdeLanguage > 0) {
            txtNrLanguage.setText(String.valueOf(qtdeLanguage));
            if (qtdeLanguage == 1) {
                txtMsgNrLanguages.setText(R.string.idioma_para_voce_praticar_de_qualquer_lugar_a_qualquer_hora);
            }

            configLanguages();
        } else {
            loadLanguages();
        }
        configButtons();
    }

    private void configButtons() {
        WeeDialogUtil.setValue(getActivity(), btIdioma, imgIdioma, R.drawable.icn_idioma, R.drawable.icn_idioma_on, R.string.idioma_req, R.string.empty, mValuesLanguage);
        WeeDialogUtil.setValue(getActivity(), btNivelAula, imgNivelAula, R.drawable.icn_nivel, R.drawable.icn_nivel_on, R.string.nivel_da_aula_req, R.string.empty, mValuesLevel);
        WeeDialogUtil.setValue(getActivity(), btAulaVia, imgAulaVia, R.drawable.icn_aula_via, R.drawable.icn_aula_via_on, R.string.aula_via, R.string.plataform_both, mValuesPlataform);
        WeeDialogUtil.setValue(getActivity(), btHorario, imgHorario, R.drawable.icn_horario, R.drawable.icn_horario_on, R.string.horario_da_aula, R.string.todos, mValuesPeriod);
    }

    private void loadLanguages() {
        txtNrLanguage.setAlpha(0.0f);
        showLoading();

        RetrofitManager.getInstance().getLanguageService().getLanguages(new Callback<List<Language>>() {
            @Override
            public void success(List<Language> languages, Response response) {
                if (languages != null) {
                    languageDao.saveAll(languages);
                    txtNrLanguage.setText(String.valueOf(languages.size()));
                    configLanguages();
                }
                hideLoading();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, new ErrorListener() {
                    @Override
                    public void refresh() {
                        loadLanguages();
                    }

                    @Override
                    public void close() {
                        configLanguages();
                    }
                }, false);
            }
        });
    }

    private void configLanguages() {
        txtNrLanguage.animate().setDuration(QTDE_LANGUAGE_ANIMATION_DURATION).alpha(1.0f);
    }

    @Click(R.id.bt_idioma)
    void btIdiomaClick() {
        List<Language> languages = languageDao.getAll();

        String[] options = new String[languages.size()];
        for (int i = 0; i < languages.size(); i++) {
            options[i] = languages.get(i).getName();
        }
        OptionsDialogFragment dialog = new OptionsDialogFragment(getString(R.string.idioma), options, this, null, false);
        dialog.show(getChildFragmentManager(), IDIOMA_FRAGMENT_TAG);
    }

    @Click(R.id.bt_nivel_aula)
    void btNivelAulaClick() {
        OptionsDialogFragment dialog = new OptionsDialogFragment(getString(R.string.nivel_da_aula), getResources().getStringArray(R.array.options_nivel_da_aula), this, null, false);
        dialog.show(getChildFragmentManager(), NIVEL_AULA_FRAGMENT_TAG);
    }

    @Click(R.id.bt_aula_via)
    void btAulaViaClick() {
        OptionsDialogFragment dialog = new OptionsDialogFragment(getString(R.string.aula_via), getResources().getStringArray(R.array.options_aula_via), this, mValuesPlataform);
        dialog.show(getChildFragmentManager(), AULA_VIA_FRAGMENT_TAG);
    }

    @Click(R.id.bt_horario)
    void btHorarioClick() {
        OptionsDialogFragment dialog = new OptionsDialogFragment(getString(R.string.horario_da_aula), getResources().getStringArray(R.array.options_horarios), this, mValuesPeriod);
        dialog.show(getChildFragmentManager(), HORARIO_FRAGMENT_TAG);
    }

    @Click(R.id.bt_search)
    void btSearchClick() {
        //verifica obrigatoriedade do filtro
        if (mValuesLevel == null || mValuesLevel.isEmpty() || mValuesLanguage == null || mValuesLanguage.isEmpty()) {
            WeeDialogUtil.showErrorDialog(getActivity(), getString(R.string.error), getString(R.string.msg_select_language_level), getString(R.string.fechar));
        } else {
            SearchResultFragment_ fragment = new SearchResultFragment_();
            fragment.setArguments(new Bundle());
            fragment.getArguments().putInt(SearchResultFragment.EXTRA_LEVEL, LevelEnun.fromName(getActivity(), getFirst(mValuesLevel)).id);
            fragment.getArguments().putStringArrayList(SearchResultFragment.EXTRA_PLATFORM, new ArrayList<String>(mValuesPlataform));
            fragment.getArguments().putStringArrayList(SearchResultFragment.EXTRA_PERIOD, new ArrayList<String>(mValuesPeriod));
            fragment.getArguments().putLong(SearchResultFragment.EXTRA_LANGUAGE, getLanguageId());
            showFragment(fragment, true);
        }
    }

    private Long getLanguageId() {
        String languageStr = getFirst(mValuesLanguage);
        if (languageStr != null) {
            Language language = languageDao.getByName(languageStr);
            if (language != null) {
                return  language.getMyId();
            }
        }
        return null;
    }

    private String getFirst(LinkedHashSet<String> lst) {
        if (lst != null && !lst.isEmpty()) {
            return lst.iterator().next();
        }
        return "";
    }

    @Override
    public void onSelectedValues(DialogFragment dialog, LinkedHashSet<String> values) {
        if (IDIOMA_FRAGMENT_TAG.equals(dialog.getTag())) {
            mValuesLanguage = values;
            WeeDialogUtil.setValue(getActivity(), btIdioma, imgIdioma, R.drawable.icn_idioma, R.drawable.icn_idioma_on, R.string.idioma_req, R.string.empty, values);
        } else if (NIVEL_AULA_FRAGMENT_TAG.equals(dialog.getTag())) {
            mValuesLevel = values;
            WeeDialogUtil.setValue(getActivity(), btNivelAula, imgNivelAula, R.drawable.icn_nivel, R.drawable.icn_nivel_on, R.string.nivel_da_aula_req, R.string.empty, values);
        } else if (AULA_VIA_FRAGMENT_TAG.equals(dialog.getTag())) {
            mValuesPlataform = values;
            WeeDialogUtil.setValue(getActivity(), btAulaVia, imgAulaVia, R.drawable.icn_aula_via, R.drawable.icn_aula_via_on, R.string.aula_via, R.string.plataform_both, values);
        } else if (HORARIO_FRAGMENT_TAG.equals(dialog.getTag())) {
            mValuesPeriod = values;
            //faz tratamento para tirar o horário
            LinkedHashSet<String> newValues = new LinkedHashSet();
            if (values != null) {
                for (String value : values) {
                    if (value.indexOf(getString(R.string.period_manha)) >= 0) {
                        newValues.add(getString(R.string.period_manha));
                    } else if (value.indexOf(getString(R.string.period_tarde)) >= 0) {
                        newValues.add(getString(R.string.period_tarde));
                    } else if (value.indexOf(getString(R.string.period_noite)) >= 0) {
                        newValues.add(getString(R.string.period_noite));
                    } else if (value.indexOf(getString(R.string.period_madrugada)) >= 0) {
                        newValues.add(getString(R.string.period_madrugada));
                    }
                }
            }
            WeeDialogUtil.setValue(getActivity(), btHorario, imgHorario, R.drawable.icn_horario, R.drawable.icn_horario_on, R.string.horario_da_aula, R.string.todos, newValues);
        }
    }
}
