package br.com.weeapp.view.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.io.File;

import br.com.weeapp.BuildConfig;
import br.com.weeapp.R;
import br.com.weeapp.model.User;
import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.service.RetrofitManager;
import br.com.weeapp.view.WeeDialogUtil;
import br.com.weeapp.view.base.BaseActivity;
import br.com.weeapp.view.base.error.ErrorListener;
import br.com.weeapp.view.custom.Button;
import br.com.weeapp.view.custom.EditText;
import de.hdodenhof.circleimageview.CircleImageView;
import me.ranieripieper.androidutils.viewlib.activity.cameragallery.SelectImageActivity;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by ranipieper on 8/3/15.
 */
@EActivity(R.layout.signup_activity)
public class SignUpActivity extends SelectImageActivity {

    @ViewById(R.id.wee_toolbar)
    protected Toolbar toolbar;

    @ViewById(R.id.img_profile_image)
    CircleImageView imgPhoto;

    @ViewById(R.id.img_add_photo)
    ImageView imgAddPhoto;

    @ViewById(R.id.edt_nome)
    EditText edtNome;

    @ViewById(R.id.edt_email)
    EditText edtEmail;

    @ViewById(R.id.edt_senha)
    EditText edtSenha;

    @ViewById(R.id.edt_conf_senha)
    EditText edtConfSenha;

    @ViewById(R.id.layout_loading)
    protected View layoutLoading;

    @ViewById(R.id.bt_cadastrar)
    Button btCadastrar;

    private static final String EXTRA_EDIT_PERFIL = "EXTRA_EDIT_PERFIL";

    @Extra(EXTRA_EDIT_PERFIL)
    boolean editPerfil;

    @AfterViews
    void afterViews() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        hideLogo();
        enableBackButton();

        edtConfSenha.setonOkDoneClick(new EditText.OnOkDoneClick() {
            @Override
            public void onOkDoneClick(EditText editText) {
                btCadastrarClick();
            }
        });

        if (editPerfil) {
            User user = SharedPrefManager.getInstance().getUser();
            if (user != null) {
                setTitle("");
                btCadastrar.setText(R.string.salvar_alteracoes);
                edtNome.setText(user.getName());
                edtEmail.setText(user.getEmail());

                edtSenha.setHint(R.string.senha);
                edtSenha.setRequired(false);
                edtConfSenha.setHint(R.string.repetir_senha);
                edtConfSenha.setRequired(false);

                if (!TextUtils.isEmpty(user.getAvatar())) {
                    ImageLoader.getInstance().displayImage(user.getAvatar(), imgPhoto);
                    imgPhoto.setVisibility(View.VISIBLE);
                    imgAddPhoto.setVisibility(View.GONE);
                }
            } else {
                finish();
            }

        }
        if (BuildConfig.DEBUG && !editPerfil) {
            edtNome.setText("nome teste");
            edtEmail.setText("teste2@teste.com");
            edtSenha.setText("qwertyui");
            edtConfSenha.setText("qwertyui");
        }
    }

    @Override
    protected void setImageSelect(Bitmap bitmap) {
        imgPhoto.setImageBitmap(bitmap);
        imgPhoto.setVisibility(View.VISIBLE);
        imgAddPhoto.setVisibility(View.INVISIBLE);
    }

    @Click(R.id.bt_cadastrar)
    void btCadastrarClick() {
        if (formIsValid()) {
            TypedFile image = null;
            if (!TextUtils.isEmpty(filePath)) {
                image = new TypedFile("image/jpeg", new File(filePath));
            }
            showLoading();
            final ErrorListener errorListener = new ErrorListener() {
                @Override
                public void refresh() {
                    btCadastrarClick();
                }

                @Override
                public void close() {
                    hideLoading();
                }
            };

            if (editPerfil) {
                User user = SharedPrefManager.getInstance().getUser();
                if (image != null) {
                    RetrofitManager.getInstance().getUserService().edit(user.getId(), image, edtNome.getTextStr(), edtEmail.getTextStr(),
                            edtSenha.getTextStr(), edtConfSenha.getTextStr(), new Callback<User>() {
                                @Override
                                public void success(User user, Response response) {
                                    successEdit(user);
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    BaseActivity.showError(SignUpActivity.this, error, errorListener);
                                }
                            });
                } else {
                    RetrofitManager.getInstance().getUserService().edit(user.getId(), user.getAvatar(), edtNome.getTextStr(), edtEmail.getTextStr(),
                            edtSenha.getTextStr(), edtConfSenha.getTextStr(), new Callback<User>() {
                                @Override
                                public void success(User user, Response response) {
                                    successEdit(user);
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    BaseActivity.showError(SignUpActivity.this, error, errorListener);
                                }
                            });
                }

            } else {
                RetrofitManager.getInstance().getUserService().sign_up(image, edtNome.getTextStr(), edtEmail.getTextStr(),
                        edtSenha.getTextStr(), edtConfSenha.getTextStr(), SharedPrefManager.getInstance().getDeviceToken(),new Callback<User>() {
                            @Override
                            public void success(User user, Response response) {
                                SharedPrefManager.getInstance().setUser(user);
                                showTermosUso();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                BaseActivity.showError(SignUpActivity.this, error, errorListener);
                            }
                        });
            }
        }
    }

    private void successEdit(User user) {
        hideLoading();
        SharedPrefManager.getInstance().setUser(user);
        WeeDialogUtil.showDialog(this, "", getString(R.string.msg_alteracao_fetuada), getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        }, false);
    }

    private void showTermosUso() {
        TermosUsoActivity.showActivity(this, true);
        hideLoading();
        finish();
    }

    private boolean formIsValid() {
        edtNome.hiddenKeyboard();
        edtEmail.hiddenKeyboard();
        edtSenha.hiddenKeyboard();
        edtConfSenha.hiddenKeyboard();

        if (edtNome.isNotValid() |
                edtEmail.isNotValid() |
                edtSenha.isNotValid() |
                edtConfSenha.isNotValid()) {
            return false;
        }

        if (!edtSenha.getText().toString().equals(edtConfSenha.getText().toString())) {
            edtConfSenha.setError(getString(R.string.msg_conf_senha_diferente));
        }
        return true;
    }

    protected void hideLogo() {
        if (toolbar != null) {
            toolbar.findViewById(R.id.img_logo).setVisibility(View.GONE);
        }
    }

    protected void enableBackButton() {
        if (toolbar != null &&  getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            toolbar.setNavigationIcon(R.drawable.icn_back);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (android.R.id.home == item.getItemId()) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void showLoading() {
        if (layoutLoading != null) {
            layoutLoading.setVisibility(View.VISIBLE);
        }
    }

    protected void hideLoading() {
        if (layoutLoading != null) {
            layoutLoading.setVisibility(View.GONE);
        }
    }

    public static void showActivity(Context ctx) {
        Intent it = new Intent(ctx, SignUpActivity_.class);
        ctx.startActivity(it);
    }

    public static void showActivity(Context ctx, boolean editPerfil) {
        Intent it = new Intent(ctx, SignUpActivity_.class);
        it.putExtra(EXTRA_EDIT_PERFIL, editPerfil);
        ctx.startActivity(it);
    }
}
