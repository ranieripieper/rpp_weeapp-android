package br.com.weeapp.view.schedule;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import br.com.weeapp.R;
import br.com.weeapp.model.result.MyReservesResult;
import br.com.weeapp.util.DateUtil;
import br.com.weeapp.view.WeeDialogUtil;
import br.com.weeapp.view.custom.Button;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;

/**
 * Created by ranipieper on 9/11/15.
 */
public class MyReserveAdapter extends RecyclerView.Adapter<MyReserveAdapter.MyReserveViewHolder> implements StickyHeaderAdapter<MyReserveAdapter.HeaderHolder> {
    private List<MyReservesResult> mDatalist;
    private MyReserveListener mMyReserveListener;
    private Context mContext;
    private boolean mNextLessons = false;
    private WeeDialogUtil.RatingListener mRatingListener;

    public MyReserveAdapter(Context context, List<MyReservesResult> list, MyReserveListener myReserveListener, boolean nextLessons, WeeDialogUtil.RatingListener ratingListener) {
        this.mContext = context;
        this.mDatalist = list;
        this.mMyReserveListener = myReserveListener;
        this.mNextLessons = nextLessons;
        this.mRatingListener = ratingListener;
    }

    @Override
    public MyReserveViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewtype) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.teacher_row_schedule, viewGroup, false);
        return new MyReserveViewHolder(v);
    }


    @Override
    public void onBindViewHolder(MyReserveViewHolder viewHolder, int position) {
        final MyReservesResult obj = mDatalist.get(position);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMyReserveListener != null) {
                    mMyReserveListener.onClick(obj);
                }
            }
        });
        if (obj.getTeacher().showBtAvaliarProf(mNextLessons)) {
            viewHolder.btAvaliar.setVisibility(View.VISIBLE);
            viewHolder.txtAvaliacao.setVisibility(View.GONE);
        } else {
            viewHolder.btAvaliar.setVisibility(View.GONE);
            viewHolder.txtAvaliacao.setVisibility(View.VISIBLE);
        }

        if (obj.showContacts(mNextLessons)) {
            String skypeContact = obj.getTeacher().getSkype();
            String hangoutContact = obj.getTeacher().getHangout();

            if (!TextUtils.isEmpty(skypeContact)) {
                viewHolder.layoutSkype.setVisibility(View.VISIBLE);
                viewHolder.txtSkype.setText(skypeContact);
            } else {
                viewHolder.layoutSkype.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(hangoutContact)) {
                viewHolder.layoutHangout.setVisibility(View.VISIBLE);
                viewHolder.txtHangout.setText(hangoutContact);
            } else {
                viewHolder.layoutHangout.setVisibility(View.GONE);
            }
        } else {
            viewHolder.layoutHangout.setVisibility(View.GONE);
            viewHolder.layoutSkype.setVisibility(View.GONE);
        }

        viewHolder.btAvaliar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WeeDialogUtil.showRatingDialog(mContext, obj, mRatingListener);
            }
        });

        viewHolder.txtName.setText(obj.getTeacher().getName());
        ImageLoader.getInstance().displayImage(obj.getTeacher().getAvatar(), viewHolder.imgPhoto);

        if (obj.getTeacher() != null && obj.getTeacher().getTeacherLanguages() != null && !obj.getTeacher().getTeacherLanguages().isEmpty()) {
            viewHolder.txtIdioma.setText(obj.getTeacher().getLanguagesStr());
            viewHolder.txtIdioma.setVisibility(View.VISIBLE);
        } else {
            viewHolder.txtIdioma.setVisibility(View.GONE);
        }

        if (obj.getTeacher() != null && obj.getTeacher().getTeacherLanguages() != null && !obj.getTeacher().getTeacherLanguages().isEmpty()) {
            viewHolder.txtNivel.setText(obj.getTeacher().getAllLevelsStr());
            viewHolder.txtNivel.setVisibility(View.VISIBLE);
        } else {
            viewHolder.txtNivel.setVisibility(View.GONE);
        }

        if (obj.getTeacher() != null && obj.getTeacher().getRate() > 0) {
            viewHolder.txtAvaliacao.setText(mContext.getString(R.string.avaliacao, obj.getTeacher().getRateStr()));
        } else {
            viewHolder.txtAvaliacao.setText(mContext.getString(R.string.avaliacao, "-"));;
        }

        if (obj.getTeacher().getLessons() == null || obj.getTeacher().getLessons().size() == 1) {
            viewHolder.txNrAulas.setText(mContext.getString(R.string.uma_aula));
        } else {
            viewHolder.txNrAulas.setText(mContext.getString(R.string.x_aulas, obj.getTeacher().getLessons().size()));
        }

    }

    @Override
    public int getItemCount() {
        return (null != mDatalist ? mDatalist.size() : 0);
    }

    //header
    @Override
    public long getHeaderId(int position) {
        final MyReservesResult data = mDatalist.get(position);
        return data.getDate().getTime();
    }

    @Override
    public HeaderHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.date_row, viewGroup, false);
        return new HeaderHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderHolder headerHolder, int position) {
        final MyReservesResult data = mDatalist.get(position);
        headerHolder.tvHeader.setText(DateUtil.diaMesAno.get().format(data.getDate()));
    }

    /**
     * Header View Holder
     */
    protected class HeaderHolder extends RecyclerView.ViewHolder {
        public TextView tvHeader;

        public HeaderHolder(View itemView) {
            super(itemView);
            tvHeader = (TextView)itemView.findViewById(R.id.txt_header);
        }
    }

    /**
     * Row View Holder
     */
    public class MyReserveViewHolder extends RecyclerView.ViewHolder {
        public TextViewPlus txtName;
        public View layoutSkype;
        public View layoutHangout;
        public TextViewPlus txtSkype;
        public TextViewPlus txtHangout;
        public ImageView imgPhoto;
        public TextViewPlus txtIdioma;
        public TextViewPlus txtNivel;
        public TextViewPlus txtAvaliacao;
        public TextViewPlus txNrAulas;
        public Button btAvaliar;

        public MyReserveViewHolder(View v) {
            super(v);
            txtName = (TextViewPlus) v.findViewById(R.id.txt_name);
            txtIdioma = (TextViewPlus) v.findViewById(R.id.txt_idioma);
            txtNivel = (TextViewPlus) v.findViewById(R.id.txt_nivel);
            txtAvaliacao = (TextViewPlus) v.findViewById(R.id.txt_avaliacao);
            layoutSkype = v.findViewById(R.id.skype_contact);
            layoutHangout = v.findViewById(R.id.hangout_contact);
            imgPhoto = (ImageView) v.findViewById(R.id.img_photo);
            btAvaliar = (Button) v.findViewById(R.id.bt_avaliar_professor);
            txtSkype = (TextViewPlus)v.findViewById(R.id.txt_skype);
            txtHangout = (TextViewPlus)v.findViewById(R.id.txt_hangout);
            txNrAulas = (TextViewPlus)v.findViewById(R.id.txt_nr_aulas);
        }

    }

    public interface MyReserveListener {
        void onClick(MyReservesResult result);
    }
}