package br.com.weeapp.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import me.ranieripieper.androidutils.viewlib.font.FontUtilCache;

/**
 * Created by ranipieper on 8/8/15.
 */
public class RadioButton extends com.rey.material.widget.RadioButton {

    public RadioButton(Context context) {
        super(context);
    }

    public RadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public RadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    public void toggle() {
        setChecked(!isChecked());
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray defaultAttr = ctx.obtainStyledAttributes(attrs, me.ranieripieper.androidutils.viewlib.R.styleable.DDSDefaultAttributes);
        String customFont = defaultAttr.getString(me.ranieripieper.androidutils.viewlib.R.styleable.DDSDefaultAttributes_text_font);
        defaultAttr.recycle();
        FontUtilCache.setCustomFont(ctx, this, customFont);
    }

}
