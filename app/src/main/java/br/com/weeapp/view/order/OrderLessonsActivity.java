package br.com.weeapp.view.order;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.freedom.yefeng.yfrecyclerview.YfListAdapter;
import com.freedom.yefeng.yfrecyclerview.YfListRecyclerView;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import br.com.weeapp.R;
import br.com.weeapp.model.Language;
import br.com.weeapp.model.Order;
import br.com.weeapp.model.Reserve;
import br.com.weeapp.model.Teacher;
import br.com.weeapp.model.User;
import br.com.weeapp.model.result.PayResult;
import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.service.RetrofitManager;
import br.com.weeapp.util.Constants;
import br.com.weeapp.view.WeeDialogUtil;
import br.com.weeapp.view.base.BaseActivity;
import br.com.weeapp.view.custom.Button;
import br.com.weeapp.view.teacher.TeacherActivity;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 8/25/15.
 */
@EActivity(R.layout.order_lessons_activity)
public class OrderLessonsActivity extends BaseActivity {

    public static final String EXTRA_TEACHER = "EXTRA_TEACHER";
    public static final String EXTRA_RESERVES = "EXTRA_RESERVES";

    @ViewById(R.id.recycler_view)
    YfListRecyclerView mRecyclerView;

    @ViewById(R.id.txt_valor_total)
    TextViewPlus txtValorTotal;

    @ViewById(R.id.txt_nr_lessons)
    TextViewPlus txtNrLessons;

    @ViewById(R.id.img_photo)
    ImageView imgPhoto;

    @ViewById(R.id.img_skype)
    ImageView imgSkype;

    @ViewById(R.id.img_hangout)
    ImageView imgHangout;

    @ViewById(R.id.txt_name)
    TextViewPlus txtName;

    @ViewById(R.id.txt_idioma)
    TextViewPlus txtIdioma;

    @ViewById(R.id.txt_nivel)
    TextViewPlus txtNivel;

    @ViewById(R.id.txt_avaliacao)
    TextViewPlus txtAvaliacao;

    @ViewById(R.id.bt_confirmar_pagar)
    Button btConfirmarPagar;

    @Extra(EXTRA_TEACHER)
    Teacher teacher;

    @Extra(EXTRA_RESERVES)
    ArrayList<Reserve> mReserves;

    private YfListAdapter<Reserve> mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private BroadcastReceiver broadcastReceiver;
    public static final String BROADCAST_FINISH_ORDER_ACTIVITY = "BROADCAST_FINISH_ORDER_ACTIVITY";

    @AfterViews
    void afterViews() {
        hideLogo();
        enableBackButton();
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new org.solovyev.android.views.llm.LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ReserveAdapter(this, mReserves);

        mRecyclerView.setAdapter(mAdapter);

        txtValorTotal.setText(getString(R.string.valor_total, String.format("%.2f", getValue())));

        if (mReserves.size() == 1) {
            txtNrLessons.setText(getString(R.string.uma_aula));
        } else {
            txtNrLessons.setText(getString(R.string.x_aulas, mReserves.size()));
        }

        configTeacherInfo();
    }

    private void configTeacherInfo() {
        Long languageId = mReserves.get(0).getLanguage().getMyId();

        Language language = mReserves.get(0).getLanguage();

        if (language != null) {
            txtNivel.setText(teacher.getLevelsStr(language.getMyId()));
            txtIdioma.setText(language.getName());
        } else {
            txtNivel.setText("");
        }

        txtName.setText(teacher.getName());
        txtAvaliacao.setText(getString(R.string.avaliacao, teacher.getRateStr()));
        ImageLoader.getInstance().displayImage(teacher.getAvatar(), imgPhoto);
        if (teacher.isSkypeAvaiable()) {
            imgSkype.setVisibility(View.VISIBLE);
        } else {
            imgSkype.setVisibility(View.GONE);
        }
        if (teacher.isHangoutAvaiable()) {
            imgHangout.setVisibility(View.VISIBLE);
        } else {
            imgHangout.setVisibility(View.GONE);
        }
    }

    @Click(R.id.bt_confirmar_pagar)
    void btConfirmarPagarClick() {
        showLoading(true);
        btConfirmarPagar.setVisibility(View.INVISIBLE);
        String reservesStr = "";
        if (mReserves != null && !mReserves.isEmpty()) {
            for (Reserve reserve : mReserves) {
                reservesStr += reserve.getId() + ",";
            }
            reservesStr = reservesStr.substring(0, reservesStr.length()-1);
        }
        /*
        RetrofitManager.getInstance().getOrderService().order(reservesStr, new Callback<Order>() {
            @Override
            public void success(Order order, Response response) {
                hideLoading();
                verifyAndRedirectPagSeguro(order);
            }

            @Override
            public void failure(RetrofitError error) {
                hideLoading();
                showError(error, null);
                btConfirmarPagar.setVisibility(View.VISIBLE);
            }
        });
        */

        final User user = SharedPrefManager.getInstance().getUser();

        RetrofitManager.getInstance().getOrderService().order(reservesStr, new Callback<PayResult>() {
            @Override
            public void success(final PayResult payResult, Response response) {
                hideLoading();
                View.OnClickListener clickPagarAgora = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        redirectToPagSeguro(payResult.getRedirectUrl());
                    }
                };
                View.OnClickListener clickPagarDepois = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        WeeDialogUtil.showDialog(OrderLessonsActivity.this, getString(R.string.atencao), getString(R.string.msg_email_pagamento_enviado, user.getEmail()), getString(R.string.ok), false, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finishAndShowHome();
                            }
                        }, false);
                    }
                };
                WeeDialogUtil.showConfirmDialog(OrderLessonsActivity.this, R.string.pagamento, R.string.realizar_pagamento, R.string.sim, R.string.nao, clickPagarAgora, clickPagarDepois);
            }

            @Override
            public void failure(RetrofitError error) {
                hideLoading();
                showError(error, null);
                btConfirmarPagar.setVisibility(View.VISIBLE);
            }
        });

    }

    private void verifyAndRedirectPagSeguro(final Order order) {
        final User user = SharedPrefManager.getInstance().getUser();
        showLoading(true);
        RetrofitManager.getInstance().getOrderService().pay(21, 21, new Callback<PayResult>() {
            @Override
            public void success(final PayResult payResult, Response response) {
                hideLoading();
                View.OnClickListener clickPagarAgora = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        redirectToPagSeguro(payResult.getRedirectUrl());
                    }
                };
                View.OnClickListener clickPagarDepois = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        WeeDialogUtil.showDialog(OrderLessonsActivity.this, getString(R.string.atencao), getString(R.string.msg_email_pagamento_enviado, user.getEmail()), getString(R.string.ok), false, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finishAndShowHome();
                            }
                        }, false);
                    }
                };
                WeeDialogUtil.showConfirmDialog(OrderLessonsActivity.this, R.string.pagamento, R.string.realizar_pagamento, R.string.sim, R.string.nao, clickPagarAgora, clickPagarDepois);

            }

            @Override
            public void failure(RetrofitError error) {
                hideLoading();
                showError(error, null);
                btConfirmarPagar.setVisibility(View.VISIBLE);
            }
        });
    }

    private void redirectToPagSeguro(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
        finishAndShowHome();
    }

    @Override
    protected void onResume() {
        super.onResume();
        unregisterReceiver();
    }

    private void unregisterReceiver() {
        if (broadcastReceiver != null) {
            try {
                unregisterReceiver(broadcastReceiver);
            } catch (IllegalArgumentException e) {
            }
        }
    }

    private String getItemPagseguro() {
        Reserve reserve = mReserves.get(0);
        return getString(R.string.pagseguro_item, reserve.getLanguage().getName(), reserve.getLevel().getName());
    }

    private double getValue() {
        return Constants.LESSON_VALUE * mReserves.size();
    }

    public final static void showActivity(Context ctx, Teacher teacher, ArrayList<Reserve> reserves) {
        Intent it = new Intent(ctx, OrderLessonsActivity_.class);
        it.putExtra(EXTRA_TEACHER, teacher);
        it.putExtra(EXTRA_RESERVES, reserves);
        ctx.startActivity(it);
    }

    private void finishAndShowHome() {
        Intent intent = new Intent(TeacherActivity.BROADCAST_FINISH_TEACHER_ACTIVITY);
        sendBroadcast(intent);
        finish();
    }
}
