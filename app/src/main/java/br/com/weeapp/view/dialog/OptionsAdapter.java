package br.com.weeapp.view.dialog;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.rey.material.widget.RadioButton;

import java.util.ArrayList;
import java.util.List;

import br.com.weeapp.R;
import br.com.weeapp.view.custom.ViewHolderClick;

/**
 * Created by ranipieper on 8/4/15.
 */
public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.ViewHolder> {
    private String[] mDataset;
    private ViewHolderClick mViewHolderClick;
    private boolean mMultipleChoise;
    private List<String> mSelectedValues = new ArrayList();

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RadioButton ckOption;
        public ViewHolder(View v) {
            super(v);
            ckOption = (RadioButton)v.findViewById(R.id.ck_option);
            ckOption.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        mViewHolderClick.onItemSelected(getAdapterPosition());
                    } else {
                        mViewHolderClick.onItemUnselected(getAdapterPosition());
                    }
                }
            });
        }
    }

    public String getItem(int position) {
        return mDataset[position];
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public OptionsAdapter(String[] myDataset, boolean multipleChoise, ViewHolderClick viewHolderClick, List<String> selectedValues ) {
        this.mDataset = myDataset;
        this.mMultipleChoise = multipleChoise;
        this.mViewHolderClick = viewHolderClick;
        this.mSelectedValues = selectedValues;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OptionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.options_dialog_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.ckOption.setText(mDataset[position]);
        if (mSelectedValues.contains(mDataset[position])) {
            holder.ckOption.setCheckedImmediately(true);
        } else {
            holder.ckOption.setCheckedImmediately(false);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
