package br.com.weeapp.view.login;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import br.com.weeapp.R;
import br.com.weeapp.preferences.SharedPrefManager;
import br.com.weeapp.view.base.BaseActivity;
import br.com.weeapp.view.custom.Button;
import br.com.weeapp.view.home.HomeActivity_;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;

/**
 * Created by ranipieper on 7/30/15.
 */
@EActivity(R.layout.termos_de_uso_activity)
public class TermosUsoActivity extends BaseActivity {

    public static final String PARAM_SHOW_BUTTONS = "PARAM_SHOW_BUTTONS";

    @Extra(PARAM_SHOW_BUTTONS)
    boolean mShowButtons;

    @ViewById(R.id.bt_accepted)
    Button btAccepted;

    @ViewById(R.id.bt_not_accepted)
    Button btNotAccepted;

    @ViewById(R.id.txt_termos)
    TextViewPlus txtTermos;

    @AfterViews
    void afterViews() {
        txtTermos.setText(Html.fromHtml(getString(R.string.texto_termos_de_uso)));
        if (mShowButtons) {
            btAccepted.setVisibility(View.VISIBLE);
            btNotAccepted.setVisibility(View.VISIBLE);
        } else {
            btAccepted.setVisibility(View.GONE);
            btNotAccepted.setVisibility(View.GONE);
            hideLogo();
            enableBackButton();
        }
    }

    @Click(R.id.bt_accepted)
    void btAcceptedClick() {
        SharedPrefManager.getInstance().setTermosAceito(true);
        Intent intent = new Intent(this, HomeActivity_.class);
        startActivity(intent);
        hideLoading();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        } else {
            finish();
        }
    }

    @Click(R.id.bt_not_accepted)
    void btNotAcceptedClick() {
        finish();
    }

    public static void showActivity(Context ctx, boolean showButtons) {
        Intent it = new Intent(ctx, TermosUsoActivity_.class);
        it.putExtra(PARAM_SHOW_BUTTONS, showButtons);
        ctx.startActivity(it);
    }
}
