package br.com.weeapp.view.dialog;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.weeapp.R;
import br.com.weeapp.model.Lesson;
import br.com.weeapp.view.custom.ViewHolderClick;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;

/**
 * Created by ranipieper on 8/4/15.
 */
public class LessonsAdapter extends RecyclerView.Adapter<LessonsAdapter.ViewHolder> {
    private List<Lesson> mDataset;
    private ViewHolderClick mViewHolderClick;
    private Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextViewPlus txtOption;
        public ViewHolder(View v) {
            super(v);
            txtOption = (TextViewPlus)v.findViewById(R.id.txt_option);
            txtOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mViewHolderClick.onItemSelected(getAdapterPosition());
                }
            });
        }
    }

    public Lesson getItem(int position) {
        return mDataset.get(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public LessonsAdapter(Context context, List<Lesson> myDataset, ViewHolderClick viewHolderClick) {
        this.mContext = context;
        this.mDataset = myDataset;
        this.mViewHolderClick = viewHolderClick;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public LessonsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lessons_dialog_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.txtOption.setText(mContext.getString(R.string.aula_das, getItem(position).getHour()));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
