package br.com.weeapp.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.solovyev.android.views.llm.DividerItemDecoration;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import br.com.weeapp.R;
import br.com.weeapp.view.custom.Button;
import br.com.weeapp.view.custom.ViewHolderClick;

/**
 * Created by ranipieper on 8/4/15.
 */
public class OptionsDialogFragment extends DialogFragment {

    private RecyclerView mRecyclerView;
    private OptionsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    OptionsDialogListener mListener;

    private String mTitle;
    private String mButtonText;
    private String[] mOptions;
    private boolean mMultipleChoise = true;

    private LinkedHashSet<String> mValuesSelected = new LinkedHashSet();

    public OptionsDialogFragment() {
    }

    public OptionsDialogFragment(String title, String[] options, OptionsDialogListener listener) {
        super();
        this.mTitle = title;
        this.mOptions = options;
        this.mListener = listener;
    }

    public OptionsDialogFragment(String title, String[] options, OptionsDialogListener listener, LinkedHashSet<String> valuesSelected) {
        super();
        this.mTitle = title;
        this.mOptions = options;
        this.mListener = listener;
        this.mValuesSelected = valuesSelected;
    }

    public OptionsDialogFragment(String title, String[] options, OptionsDialogListener listener, LinkedHashSet<String> valuesSelected, boolean multipleChoise) {
        super();
        this.mTitle = title;
        this.mOptions = options;
        this.mListener = listener;
        this.mValuesSelected = valuesSelected;
        this.mMultipleChoise = multipleChoise;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.WeeAppTheme_OptionsDialog);
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.wee_options_dialog, null);
        builder.setView(v);

        TextView txtTitle = (TextView) v.findViewById(R.id.txt_title);
        Button btOk = (Button) v.findViewById(R.id.bt_ok);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.lst_options);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new org.solovyev.android.views.llm.LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getResources().getDrawable(R.drawable.line), true, false);

        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        ViewHolderClick viewHolderClick = new ViewHolderClick() {
            @Override
            public void onItemSelected(int position) {
                if (!mMultipleChoise) {
                    mValuesSelected = new LinkedHashSet();
                }
                mValuesSelected.add(mAdapter.getItem(position));

                if (!mMultipleChoise) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mListener.onSelectedValues(OptionsDialogFragment.this, mValuesSelected);
                            dismiss();
                        }
                    }, 200);
                }
            }

            @Override
            public void onItemUnselected(int position) {
                mValuesSelected.remove(mAdapter.getItem(position));
            }
        };

        if (mValuesSelected == null) {
            mValuesSelected = new LinkedHashSet();
        }

        mAdapter = new OptionsAdapter(this.mOptions, mMultipleChoise, viewHolderClick, new ArrayList(mValuesSelected));
        mRecyclerView.setAdapter(mAdapter);

        txtTitle.setText(mTitle);

        if (mMultipleChoise) {
            btOk.setVisibility(View.VISIBLE);
        } else {
            btOk.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(mButtonText)) {
            btOk.setText(this.mButtonText);
        }

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(true);


        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mListener.onSelectedValues(OptionsDialogFragment.this, mValuesSelected);
            }
        });

        return dialog;
    }

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface OptionsDialogListener {
        public void onSelectedValues(DialogFragment dialog, LinkedHashSet<String> values);
    }

}
