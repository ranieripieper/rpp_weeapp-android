package br.com.weeapp.view.settings;

import android.content.Context;
import android.content.Intent;
import android.text.Html;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.com.weeapp.R;
import br.com.weeapp.view.base.BaseActivity;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;

/**
 * Created by ranipieper on 8/5/15.
 */
@EActivity(R.layout.sobre_activity)
public class SobreActivity extends BaseActivity {

    @ViewById(R.id.texto_sobre_o_wee)
    TextViewPlus txtSobre;

    @AfterViews
    void afterViews() {
        hideLogo();
        txtSobre.setText(Html.fromHtml(getString(R.string.texto_sobre_o_wee)));
        enableBackButton();
    }

    public static void showActivity(Context ctx) {
        Intent it = new Intent(ctx, SobreActivity_.class);
        ctx.startActivity(it);
    }
}
