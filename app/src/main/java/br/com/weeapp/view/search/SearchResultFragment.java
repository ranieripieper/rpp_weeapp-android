package br.com.weeapp.view.search;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.freedom.yefeng.yfrecyclerview.YfListAdapter;
import com.freedom.yefeng.yfrecyclerview.YfListInterface;
import com.freedom.yefeng.yfrecyclerview.YfListRecyclerView;
import com.freedom.yefeng.yfrecyclerview.YfLoadMoreListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.weeapp.R;
import br.com.weeapp.enumeration.PeriodEnun;
import br.com.weeapp.enumeration.PlatformEnun;
import br.com.weeapp.model.Teacher;
import br.com.weeapp.model.dao.LanguageDao;
import br.com.weeapp.model.result.SearchResult;
import br.com.weeapp.service.RetrofitManager;
import br.com.weeapp.util.Constants;
import br.com.weeapp.view.base.BaseFragment;
import br.com.weeapp.view.base.error.ErrorListener;
import br.com.weeapp.view.teacher.TeacherActivity;
import me.ranieripieper.androidutils.viewlib.widget.TextViewPlus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 8/6/15.
 */
@EFragment(R.layout.search_result_fragment)
public class SearchResultFragment extends BaseFragment implements YfLoadMoreListener {

    private boolean mLoadingLock = false;
    private int page = 1;
    private boolean loadMore = true;

    @ViewById(R.id.recycler_view)
    YfListRecyclerView mRecyclerView;

    @ViewById(R.id.txt_msg_empty)
    TextViewPlus txtMsgEmpty;

    private YfListAdapter<Teacher> mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public static final String EXTRA_LEVEL = "EXTRA_LEVEL";
    public static final String EXTRA_PLATFORM = "EXTRA_PLATFORM";
    public static final String EXTRA_PERIOD = "EXTRA_PERIOD";
    public static final String EXTRA_LANGUAGE = "EXTRA_LANGUAGE";

    @FragmentArg(EXTRA_LEVEL)
    Integer levelId;

    @FragmentArg(EXTRA_PLATFORM)
    ArrayList<String> platforms;

    @FragmentArg(EXTRA_PERIOD)
    ArrayList<String> schedules;

    @FragmentArg(EXTRA_LANGUAGE)
    Long mLanguageId;

    @Bean
    LanguageDao languageDao;

    @AfterViews
    void afterViews() {
        callService();
    }

    private void populateView(SearchResult searchResult) {
        ArrayList<Teacher> lstTeachers = null;
        if (searchResult != null) {
            lstTeachers = searchResult.getTeachers();
        }
        if (lstTeachers == null || lstTeachers.isEmpty()) {
            if (page == 1) {
                txtMsgEmpty.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        } else {
            if (page == 1) {
                mRecyclerView.setHasFixedSize(true);
                mLayoutManager = new org.solovyev.android.views.llm.LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

                mRecyclerView.setLayoutManager(mLayoutManager);

                mAdapter = new TeacherAdapter(getHomeActivity(), searchResult.getTeachers(), languageDao.getById(mLanguageId));

                mAdapter.setOnItemClickListener(new YfListInterface.OnItemClickListener<Teacher>() {
                    @Override
                    public void onItemClick(View view, Teacher item) {
                        TeacherActivity.showActivity(getActivity(), item, mLanguageId, levelId, null);
                    }
                });

                mRecyclerView.setAdapter(mAdapter);
                mRecyclerView.setVisibility(View.VISIBLE);
                txtMsgEmpty.setVisibility(View.GONE);

                mRecyclerView.enableAutoLoadMore(this);
            } else {
                mAdapter.addData(lstTeachers);
            }

        }
        if (lstTeachers.size() < Constants.PER_PAGE) {
            loadMore = false;
        } else {
            loadMore = true;
        }

        mLoadingLock = false;
    }

    @Override
    public void loadMore() {
        if (mLoadingLock) {
            return;
        }

        if (loadMore) {
            // has more
            page++;
            mLoadingLock = true;
            if (!mAdapter.getFooters().contains("loading...")) {
                mAdapter.addFooter("loading...");
            }
            callService();
        } else {
            // no more
            if (mAdapter.getFooters().contains("loading...")) {
                mAdapter.removeFooter("loading...");
            }
        }
    }

    private void callService() {
        if (page == 1) {
            mRecyclerView.setVisibility(View.GONE);
            txtMsgEmpty.setVisibility(View.GONE);
            showLoading(false);
        }

        Boolean skype = null;
        Boolean hangout = null;

        List<String> platforms = getPlatform();
        if (platforms.contains(String.valueOf(PlatformEnun.SKYPE.id))) {
            skype = true;
        }
        if (platforms.contains(String.valueOf(PlatformEnun.HANGOUT.id))) {
            hangout = true;
        }

        if (skype != null && skype && hangout != null && hangout) {
            skype = null;
            hangout = null;
        }

        RetrofitManager.getInstance().getSearchService().search(levelId, skype, hangout, getPeriod(), mLanguageId, page, Constants.PER_PAGE, new Callback<SearchResult>() {
            @Override
            public void success(SearchResult searchResult, Response response) {
                hideLoading();
                populateView(searchResult);
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, new ErrorListener() {
                    @Override
                    public void refresh() {
                        callService();
                    }

                    @Override
                    public void close() {
                        getFragmentManager().popBackStackImmediate();
                    }
                });
            }
        });
    }

    private List<String> getPlatform() {
        if (platforms != null && !platforms.isEmpty()) {
            List<String> result = new ArrayList();
            for (String platform : platforms) {
                PlatformEnun platformEnun = PlatformEnun.fromName(getActivity(), platform);
                if (platformEnun != null) {
                    result.add(String.valueOf(platformEnun.id));
                }
            }
        }
        return new ArrayList();
    }

    private String getPeriod() {
        if (schedules == null || schedules.isEmpty()) {
            return "";
        } else {
            String result = "";
            for (String period : schedules) {
                PeriodEnun periodEnun = PeriodEnun.fromName(getActivity(), period);
                if (periodEnun != null) {
                    result += String.valueOf(periodEnun.id) + ",";
                }
            }
            if (TextUtils.isEmpty(result)) {
                return null;
            } else {
                return result.substring(0, result.length()-1);
            }
        }
    }

}
