package br.com.weeapp.util;

import android.text.TextUtils;

import java.text.Normalizer;

/**
 * Created by ranipieper on 8/24/15.
 */
public class StringUtil {

    public static final String SEPARATOR = ", ";
    public static String adjustString(String value) {
        if (!TextUtils.isEmpty(value) && value.length() > SEPARATOR.length()) {
            value = value.substring(0, value.length() - SEPARATOR.length());
            int lastComma = value.lastIndexOf(SEPARATOR);
            if (lastComma > 0) {
                value = value.substring(0, lastComma) + " e " + value.substring(lastComma + SEPARATOR.length(), value.length());
            }
            return value;
        }
        return "";
    }

    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s.toLowerCase();
    }
}
