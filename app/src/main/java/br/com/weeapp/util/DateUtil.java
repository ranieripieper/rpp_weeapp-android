package br.com.weeapp.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by ranipieper on 8/24/15.
 */
public class DateUtil {

    public static final ThreadLocal<DateFormat> diaMesAno = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("dd-MM-yyyy");
        }
    };

    public static final ThreadLocal<DateFormat> horaMin = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("HH'h'mm");
        }
    };
}
