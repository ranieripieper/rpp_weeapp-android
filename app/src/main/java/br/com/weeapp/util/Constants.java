package br.com.weeapp.util;

/**
 * Created by ranipieper on 8/20/15.
 */
public class Constants {

    public static int PER_PAGE = 10;

    public static float LESSON_VALUE = 25.0F;

    public static String RESERVE_TYPE_NEW = "new";
    public static String RESERVE_TYPE_OLD = "old";


    public static final String PARAM_CODE = "{code}";
    public static final String URL_PAGSEGURO_REDIRECT = "https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code={code}";

    public static final String PARSE_APP_ID = "f1FD6Lca9LPeFI59iWzM5nOSLN5ltHn3c2tJFMKL";
    public static final String PARSE_CLIENT_KEY = "juoY06Q4XtJLelWI8hbTU8SYIuwHA5T3bM4D3hnk";

}
