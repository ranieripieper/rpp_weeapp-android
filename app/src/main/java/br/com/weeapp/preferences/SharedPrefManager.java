package br.com.weeapp.preferences;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;

import java.util.Date;

import br.com.weeapp.model.User;

/**
 * Created by jeffcailteux on 7/21/15.
 */
public class SharedPrefManager {

    //TODO setup simple encryption of saved strings

    private static SharedPrefManager sharedPrefManager;
    private Context mContext;
    private final static String PREFS_NAME = "weapp_shared_prefs";

    public final static String USER = "USER";
    public final static String DEVICE_TOKEN = "DEVICE_TOKEN";
    public final static String UPDATE_LANGUAGES = "UPDATE_LANGUAGES";
    public final static String TERMOS = "TERMOS";
    public final static String DATE_LAST_TIP = "DATE_LAST_TIP";

    private SharedPrefManager(Context context) {
        this.mContext = context;
    }

    public static SharedPrefManager getInstance() {
        return sharedPrefManager;
    }

    public static void init(Application application) {
        sharedPrefManager = new SharedPrefManager(application);
    }

    private void saveJsonProperty(String propertyName, Object object) {
        SharedPreferences.Editor editor = getEditor();
       // if(object != null) {
            editor.putString(propertyName, new Gson().toJson(object));
       // } else {
        //    editor.putString(propertyName, "");
        //}

        editor.apply();
    }

    private String getJsonProperty(String propertyName) {
        return getSharedPreferences().getString(propertyName, null);
    }

    public User getUser() {
        String json = getJsonProperty(USER);
        if (json != null) {
            return new Gson().fromJson(json, User.class);
        }
        return null;
    }

    public void setUser(User value) {
        saveJsonProperty(USER, value);
    }

    public String getDeviceToken() {
        return getSharedPreferences().getString(DEVICE_TOKEN, "");
    }

    public void setDeviceToken(String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(DEVICE_TOKEN, value);
        editor.apply();
    }

    public boolean isUpdateLanguages() {
        return getSharedPreferences().getBoolean(UPDATE_LANGUAGES, true);
    }

    public void setUpdateLanguages(boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(UPDATE_LANGUAGES, value);
        editor.apply();
    }

    public Date getDateLastTip() {
        long dt = getSharedPreferences().getLong(DATE_LAST_TIP, -1);
        if (dt > 0) {
            return new Date(dt);
        }
        return null;
    }

    public void setDateLastTip(Date value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putLong(DATE_LAST_TIP, value.getTime());
        editor.apply();
    }

    public boolean isTermosAceito() {
        return getSharedPreferences().getBoolean(TERMOS, false);
    }

    public void setTermosAceito(boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(TERMOS, value);
        editor.apply();
    }


    public boolean isLogged() {
        if (getUser() != null) {
            return true;
        }
        return false;
    }

    public void logout() {
        setUser(null);
        //logout facebook
        LoginManager.getInstance().logOut();
        setTermosAceito(false);
    }

    private SharedPreferences.Editor getEditor() {
        return getSharedPreferences().edit();
    }

    private SharedPreferences getSharedPreferences() {
        return mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

}


