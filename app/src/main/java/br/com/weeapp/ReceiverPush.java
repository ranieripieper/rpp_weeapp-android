package br.com.weeapp;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.weeapp.model.PushNotificationMessage;
import br.com.weeapp.service.RetrofitManager;
import br.com.weeapp.view.base.BaseActivity;
import br.com.weeapp.view.home.HomeActivity_;
import br.com.weeapp.view.splash.SplashLoginActivity;
import br.com.weeapp.view.splash.SplashLoginActivity_;

public class ReceiverPush extends ParsePushBroadcastReceiver {
	
	@Override
	protected void onPushReceive(Context context, Intent intent) {
		try {
            String data = intent.getStringExtra("com.parse.Data");
			PushNotificationMessage pushNotificationMsg = RetrofitManager.getGson().fromJson(data, PushNotificationMessage.class);

			createNotification(context, pushNotificationMsg);
            
        } catch (Exception e) {
            Log.e("ParsePushReceiver", "Unexpected JSONException when receiving push data: ", e);
        }
	}
	
    @Override
    protected void onPushOpen(Context context, Intent intent) {
    	
        ParseAnalytics.trackAppOpened(intent);

        String uriString = null;
        try {
            JSONObject pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));
            uriString = pushData.optString("uri");
        } catch (JSONException e) {
            Log.d("ParsePushReceiver", "Unexpected JSONException when receiving push data: ", e);
        }
        Class<? extends Activity> cls = getActivity(context, intent);
        Intent activityIntent;
        if (uriString != null && !uriString.isEmpty()) {
            activityIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriString));
        } else {
            activityIntent = new Intent(context, SplashLoginActivity.class);
        }

        activityIntent.putExtras(intent.getExtras());
        if (Build.VERSION.SDK_INT >= 16) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(cls);
            stackBuilder.addNextIntent(activityIntent);
            stackBuilder.startActivities();
        } else {
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(activityIntent);
        }
    }

    public static void createNotification(Context context, PushNotificationMessage message) {

    	NotificationCompat.Builder mBuilder =
    	        new NotificationCompat.Builder(context)
    	        .setSmallIcon(R.drawable.ic_wee_notification)
    	        .setContentTitle(context.getString(R.string.app_name))
    	        .setContentText(message.getMessage())
    	        .setStyle(new NotificationCompat.BigTextStyle().bigText(message.getMessage()))
    	        .setAutoCancel(true)
    	        .setCategory(NotificationCompat.CATEGORY_SOCIAL)
    	        .setGroupSummary(true)
    	        .setColor(context.getResources().getColor(R.color.accent));

    	Intent resultIntent = new Intent(context, HomeActivity_.class);
    	TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
    	stackBuilder.addParentStack(SplashLoginActivity_.class);
		resultIntent.putExtra(BaseActivity.EXTRA_SCREEN_NOTIFICATION, message.getScreen());
    	stackBuilder.addNextIntent(resultIntent);
    	
    	PendingIntent resultPendingIntent =
    	        stackBuilder.getPendingIntent(
    	            0,
    	            PendingIntent.FLAG_CANCEL_CURRENT
    	        );
    	mBuilder.setContentIntent(resultPendingIntent);
    	
    	NotificationManager mNotificationManager =
    	    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    	
    	mNotificationManager.notify(new Double(Math.random()*1000000).intValue(), mBuilder.build());
    }
    

    public static Bitmap getCircularBitmap(Bitmap bitmap) {
        Bitmap output;

        if (bitmap != null) {
        	if (bitmap.getWidth() > bitmap.getHeight()) {
                output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Config.ARGB_8888);
            } else {
                output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

            float r = 0;

            if (bitmap.getWidth() > bitmap.getHeight()) {
                r = bitmap.getHeight() / 2;
            } else {
                r = bitmap.getWidth() / 2;
            }

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawCircle(r, r, r, paint);
            paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            return output;
        }
        
        return null;
        
    }
    
    
} 